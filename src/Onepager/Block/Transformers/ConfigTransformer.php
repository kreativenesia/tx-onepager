<?php namespace ThemeXpert\Onepager\Block\Transformers;

class ConfigTransformer {
  public function __construct( $fieldsTransformer ) {
    $this->fieldsTransformer = $fieldsTransformer;
  }

  public function transform( $config, $file, $url ) {
    //default file names
    //users can set different names though
    //this keeps boilerplate away
    $default_image = "block.jpg";
    $default_view  = "view.php";
    $default_style = "style.php";
    $config_file   = $file;

    //directory path of this block
    $dir = dirname( $file ) . DIRECTORY_SEPARATOR;

    //configurations we want
    //unique slug that is used to identify a block
    //boys make it unique
    $slug = $config['slug'];

    //used to categorize blocks
    $groups = $this->get( $config, 'groups', array() );

    //name could be anything defaults to slug
    $name = $this->get( $config, 'name', ucfirst( $config['slug'] ) );

    //this is quite a complex thing
    //as you see on live editing mode, the right side refreshes alot.
    //wow and other animated stuffs looks wired. so we remove them on live mode
    //so if you want a specific class on live mode you want to remove its here you do
    //for example you want to remove wow class from article  tag you do it this way
    //$removables = array("article" => [ 'wow' ] );
    $removables = array(
      "[data-animated], .animated, .wow" => array( 'animated', 'wow' ),
    );
    $livemode   = $this->get( $config, 'livemode', $removables );


    //get the view file path
    $view_file = $dir . $this->get( $config, 'view', $default_view );

    //get the internal stylesheet file
    $style_file = $dir . $this->get( $config, 'style', $default_style );

    //get the block image path
    if(file_exists(ONEPAGER_THEME_PATH."/overrides/".$slug."/block.jpg")){
      $image = trailingslashit( ONEPAGER_THEME_URL ) ."/overrides/".$slug."/block.jpg";
    } else {
      switch ( $config['slug'] ) {
        case 'multipress-blog-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/blog/blog1.jpg';
          break;
        case 'multipress-blog-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/blog/blog2.jpg';
          break;
        case 'multipress-blog-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/blog/blog3.jpg';
          break;
        case 'multipress-blog-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/blog/blog4.jpg';
          break;
        case 'multipress-blog-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/blog/blog5.jpg';
          break;
        case 'multipress-blog-6':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/blog/blog6.jpg';
          break;
          case 'multipress-blog-7':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/blog/blog7.jpg';
          break;
          case 'multipress-contact-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/contacts/contact1.jpg';
          break;
          case 'multipress-contact-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/contacts/contact2.jpg';
          break;
          case 'multipress-contact-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/contacts/contact3.jpg';
          break;
          case 'multipress-contact-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/contacts/contact4.jpg';
          break;
          case 'multipress-contact-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/contacts/contact5.jpg';
          break;
          case 'multipress-content-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content1.jpg';
          break;
          case 'multipress-content-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content2.jpg';
          break;
          case 'multipress-content-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content3.jpg';
          break;
          case 'multipress-content-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content4.jpg';
          break;
          case 'multipress-content-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content5.jpg';
          break;
          case 'multipress-content-6':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content6.jpg';
          break;
          case 'multipress-content-7':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content7.jpg';
          break;
          case 'multipress-content-8':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content8.jpg';
          break;
          case 'multipress-content-9':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content9.jpg';
          break;
          case 'multipress-content-10':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content10.jpg';
          break;
          case 'multipress-content-11':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content11.jpg';
          break;
          case 'multipress-content-12':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content12.jpg';
          break;
          case 'multipress-content-13':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content13.jpg';
          break;
          case 'multipress-content-14':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content14.jpg';
          break;
          case 'multipress-content-15':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content15.jpg';
          break;
          case 'multipress-content-16':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content16.jpg';
          break;
          case 'multipress-content-17':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content17.jpg';
          break;
          case 'multipress-content-18':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content18.jpg';
          break;
          case 'multipress-content-19':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content19.jpg';
          break;
          case 'multipress-content-20':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/content/content20.jpg';
          break;
          case 'multipress-countdown-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/countdown/countdown1.jpg';
          break;
          case 'multipress-countdown-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/countdown/countdown2.jpg';
          break;
          case 'multipress-countdown-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/countdown/countdown3.jpg';
          break;
          case 'multipress-countdown-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/countdown/countdown4.jpg';
          break;
          case 'multipress-countdown-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/countdown/countdown5.jpg';
          break;
          case 'multipress-features-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/feature/feature1.jpg';
          break;
          case 'multipress-features-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/feature/feature2.jpg';
          break;
          case 'multipress-features-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/feature/feature3.jpg';
          break;
          case 'multipress-features-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/feature/feature4.jpg';
          break;
          case 'multipress-features-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/feature/feature5.jpg';
          break;
          case 'multipress-features-6':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/feature/feature6.jpg';
          break;
          case 'multipress-footer-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/footer/footer1.jpg';
          break;
          case 'multipress-footer-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/footer/footer2.jpg';
          break;
          case 'multipress-footer-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/footer/footer3.jpg';
          break;
          case 'multipress-footer-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/footer/footer4.jpg';
          break;
          case 'multipress-footer-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/footer/footer5.jpg';
          break;
          case 'multipress-form-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/form/form1.jpg';
          break;
          case 'multipress-form-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/form/form2.jpg';
          break;
          case 'multipress-form-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/form/form3.jpg';
          break;
          case 'multipress-form-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/form/form4.jpg';
          break;
          case 'multipress-form-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/form/form5.jpg';
          break;
          case 'multipress-landing-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze1.jpg';
          break;
          case 'multipress-landing-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze2.jpg';
          break;
          case 'multipress-landing-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze3.jpg';
          break;
          case 'multipress-landing-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze4.jpg';
          break;
          case 'multipress-landing-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze5.jpg';
          break;
          case 'multipress-landing-6':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze6.jpg';
          break;
          case 'multipress-landing-7':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze7.jpg';
          break;
          case 'multipress-landing-8':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze8.jpg';
          break;
          case 'multipress-landing-9':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze9.jpg';
          break;
          case 'multipress-landing-10':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze10.jpg';
          break;
          case 'multipress-landing-11':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze11.jpg';
          break;
          case 'multipress-landing-12':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze12.jpg';
          break;
          case 'multipress-landing-13':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze13.jpg';
          break;
          case 'multipress-landing-14':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze14.jpg';
          break;
          case 'multipress-landing-15':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze15.jpg';
          break;
          case 'multipress-landing-16':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze16.jpg';
          break;
          case 'multipress-landing-17':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze17.jpg';
          break;
          case 'multipress-landing-18':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze18.jpg';
          break;
          case 'multipress-landing-19':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze19.jpg';
          break;
          case 'multipress-landing-20':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze20.jpg';
          break;
          case 'multipress-landing-21':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze21.jpg';
          break;
          case 'multipress-landing-22':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze22.jpg';
          break;
          case 'multipress-landing-23':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/squeze/squeze23.jpg';
          break;
          case 'multipress-nav-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation1.jpg';
          break;
          case 'multipress-nav-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation2.jpg';
          break;
          case 'multipress-nav-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation3.jpg';
          break;
          case 'multipress-nav-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation4.jpg';
          break;
          case 'multipress-nav-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation5.jpg';
          break;
          case 'multipress-nav-6':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation6.jpg';
          break;
          case 'multipress-nav-7':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation7.jpg';
          break;
          case 'multipress-nav-8':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/navigations/navigation8.jpg';
          break;
          case 'multipress-portfolios-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/portfolio/portfolio1.jpg';
          break;
          case 'multipress-portfolios-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/portfolio/portfolio2.jpg';
          break;
          case 'multipress-portfolios-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/portfolio/portfolio3.jpg';
          break;
          case 'multipress-portfolios-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/portfolio/portfolio4.jpg';
          break;
          case 'multipress-portfolios-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/portfolio/portfolio5.jpg';
          break;
          case 'multipress-portfolios-6':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/portfolio/portfolio6.jpg';
          break;
          case 'multipress-pricing-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/pricing/pricing1.jpg';
          break;
          case 'multipress-pricing-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/pricing/pricing2.jpg';
          break;
          case 'multipress-pricing-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/pricing/pricing3.jpg';
          break;
          case 'multipress-pricing-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/pricing/pricing4.jpg';
          break;
          case 'multipress-pricing-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/pricing/pricing5.jpg';
          break;
          case 'multipress-pricing-6':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/pricing/pricing6.jpg';
          break;
          case 'multipress-shop-banner-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/banner/banner1.jpg';
          break;
          case 'multipress-shop-banner-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/banner/banner2.jpg';
          break;
          case 'multipress-shop-banner-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/banner/banner3.jpg';
          break;
          case 'multipress-shop-banner-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/banner/banner4.jpg';
          break;
          case 'multipress-single-image-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/singleimage/image1.jpg';
          break;
          case 'multipress-slider-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/slider/slider1.jpg';
          break;
          case 'multipress-slider-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/slider/slider2.jpg';
          break;
          case 'multipress-slider-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/slider/slider3.jpg';
          break;
          case 'multipress-slider-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/slider/slider4.jpg';
          break;
          case 'multipress-slider-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/slider/slider5.jpg';
          break;
          case 'multipress-status-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/status/status1.jpg';
          break;
          case 'multipress-status-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/status/status2.jpg';
          break;
          case 'multipress-status-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/status/status3.jpg';
          break;
          case 'multipress-subscribe-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/subscribe/subscribe1.jpg';
          break;
          case 'multipress-subscribe-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/subscribe/subscribe2.jpg';
          break;
          case 'multipress-subscribe-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/subscribe/subscribe3.jpg';
          break;
          case 'multipress-subscribe-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/subscribe/subscribe4.jpg';
          break;
          case 'multipress-subscribe-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/subscribe/subscribe5.jpg';
          break;
          case 'multipress-team-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/teams/team1.jpg';
          break;
          case 'multipress-team-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/teams/team2.jpg';
          break;
          case 'multipress-team-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/teams/team3.jpg';
          break;
          case 'multipress-team-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/teams/team4.jpg';
          break;
          case 'multipress-team-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/teams/team5.jpg';
          break;
          case 'multipress-testimonials-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/testimonials/testimonial1.jpg';
          break;
          case 'multipress-testimonials-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/testimonials/testimonial2.jpg';
          break;
          case 'multipress-testimonials-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/testimonials/testimonial3.jpg';
          break;
          case 'multipress-testimonials-4':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/testimonials/testimonial4.jpg';
          break;
          case 'multipress-testimonials-5':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/testimonials/testimonial5.jpg';
          break;
          case 'multipress-video-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/videos/video1.jpg';
          break;
          case 'multipress-video-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/videos/video2.jpg';
          break;
          case 'multipress-video-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/videos/video3.jpg';
          break;
          case 'multipress-product-1':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/woocommerce/product1.jpg';
          break;
          case 'multipress-product-2':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/woocommerce/product2.jpg';
          break;
          case 'multipress-product-3':
          $image = 'https://s3-ap-southeast-1.amazonaws.com/kreativenesia.com/txonepager-blockimage/woocommerce/product3.jpg';
          break;
        default:
          $image = trailingslashit( $url ) . $this->get( $config, 'image', $default_image );
          break;
      }
    }


    //assets is a function
    //we enqueue our stylesheets and scripts here
    $enqueue = $this->get( $config, 'assets', false );

    //three different tabs.
    $contents = $this->fieldsTransformer->transform( $this->get( $config, 'contents', array() ) );
    $settings = $this->fieldsTransformer->transform( $this->get( $config, 'settings', array() ) );
    $styles   = $this->fieldsTransformer->transform( $this->get( $config, 'styles', array() ) );

    return compact(
      'url',
      'dir',
      'slug',
      'livemode',
      'enqueue',
      'name',
      'groups',

      'image',
      'style_file',
      'view_file',
      'config_file',

      'settings',
      'contents',
      'styles'
    );
  }

  public function get( $arr, $key, $default ) {
    return is_array( $arr ) && array_key_exists( $key, $arr ) ? $arr[ $key ] : $default;
  }
}
