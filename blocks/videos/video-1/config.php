<?php

return array(
	'slug'    	=> 'multipress-video-1',
	'name' 		=> esc_html__( 'Multipress Video 1', 'multipress' ),
	'groups'    => array( 'video' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'This is Video Element'
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet et ratione, ullam illum repellat iste totam quas. Quibusdam ratione laboriosam, molestias sequi reprehenderit id, quasi quod, cumque odio animi voluptas?'
			),	
			array(
				'name' 		=> 'button_label',
				'label' 	=> esc_html__( 'Button Label', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Read More'
			),
			array(
				'name' 		=> 'button_url',
				'label' 	=> esc_html__( 'Button URL', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),
			array(
				'name' 		=> 'button_target',
				'type'		=> 'switch',
				'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
				'value' 	=> true
			),	
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name' 		=> 'youtube_embed_code',
				'label' 	=> esc_html__( 'Youtube Embed Code', 'multipress' ),
				'type' 		=> 'textarea',
				'value' 	=> '<iframe width="1280" height="720" src="https://www.youtube.com/embed/yY0L1xRtDSU" frameborder="0" allowfullscreen></iframe>'
			),
			
	),

	'settings' 	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'video_position',
			'label' 	=> esc_html__( 'Video Position', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'left',
			'options' 	=> array(
				'left' 	=> 'Left',
				'right' => 'Right',
			)
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_video',
				'label' 	=> esc_html__( 'Youtube Video Background', 'multipress' ),
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'section_subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'button_background',
				'label' 	=> esc_html__( 'Button Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'button_text_color',
				'label' 	=> esc_html__( 'Button Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'button_text_font_size',
				'label' 	=> esc_html__( 'Buttton Text Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
	),

);
