<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	$video_width 		= ! empty( $contents['video_width'] ) ? $contents['video_width'] : '640px';
?>

<!-- Multipress Video 1 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-video-block type-2 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="text-center">
			<header class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<p class="section-subtitle"><?php echo ''.$contents['section_subtitle']; ?></p>
				<?php endif; ?>
			</header>

			<div class="video-container" style="width:<?php echo ''.$video_width; ?>">
				<div class="intrinsic-ratio" style="padding-bottom:56.25%;">
					<?php if ( ! empty( $contents['youtube_embed_code'] ) ) : ?>
						<?php echo ''.$contents['youtube_embed_code']; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>