<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-form multipress-form--type-1 <?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row">
			<div class="col-md-6">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<div class="section-header">
						<?php if ( ! empty( $contents['section_title'] ) ) : ?>
							<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
							<small class="section-subtitle"><?php echo ''.$contents['section_subtitle']; ?></small>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
			
			<div class="col-md-5 col-md-offset-1">
				<div class="mp-form">
					<?php if ( ! empty( $contens['form_title'] ) ) : ?>
						<h2><?php echo esc_attr( $contens['form_title'] ); ?></h2>
					<?php endif; ?>

					<?php if ( ! empty( $contents['form_id'] ) ) : ?>
						<?php echo do_shortcode( $contents['form_id'] ); ?>
					<?php else : ?>
						<form action="">
							<p><input type="text" placeholder="Your name"></p>
							<p><input type="text" placeholder="Your company"></p>
							<p><input type="text" placeholder="Your email"></p>
							<p><input type="text" placeholder="Your phone"></p>
							<p><input type="submit" value="Try 30 Days free trial"></p>
						</form>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
</div>