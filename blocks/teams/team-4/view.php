<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	
	$team_args = array(
		'post_type'				=> 'multipress-team',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 4,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$custom_query 	= get_posts( $team_args );  
	$chunked_posts 	= array_chunk( $custom_query , 2 );
 ?>

<!-- Team Block Style 4 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-team multipress-team--style-4 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle wow fadeInUp'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="team-grid">

			<?php foreach ( $chunked_posts as $custom_post ) : ?>
			
				<div class="row">
					<?php $delay = 150; ?>
					<?php foreach( $custom_post as $post ) : setup_postdata( $post ); ?>

						<div class="col-md-6">
							<div class="team wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
								<div class="team__image team__image--circle">
									<?php 
										$thumbnail_id 	= get_post_thumbnail_id( $post->ID ); 
										$img_url 		= wp_get_attachment_image_src( $thumbnail_id, 'full' );
										if ( ! empty( $thumbnail_id ) ) : ?>
											<img src="<?php echo _multipress_resize( $img_url[0], 100, 100 ); ?>" alt="<?php echo esc_attr( $post->post_title ); ?>">
										<?php else : ?>
											<img src="http://unsplash.it/100/100" alt="<?php echo esc_attr( $post->post_title ); ?>">
										<?php endif; ?>
								</div>

								<?php $team_meta = get_post_meta( $post->ID, '_team_details', true ); ?>
								<div class="team__detail">
									<h3 class="team__title"><a href="<?php echo get_permalink( $post->ID ); ?>"><?php echo esc_attr( $post->post_title ); ?></a></h3>

									<?php if ( ! empty( $team_meta['role'] ) ) : ?>
										<span class="team__position"><?php echo esc_attr( $team_meta['role'] ); ?></span>
									<?php endif; ?>

									<?php if ( ! empty( $team_meta['biography'] ) ) : ?>
										<div class="team__desc">
											<p><?php echo wp_trim_words( $team_meta['biography'], 20 ); ?></p>
										</div>
									<?php endif; ?>

									<div class="team__social-link">
										<?php if ( ! empty( $team_meta['gplus'] ) ) : ?>
											<a href="<?php echo esc_url( $team_meta['gplus'] ); ?>"><i class="fa fa-google-plus"></i></a>
										<?php endif; 

										if ( $team_meta['facebook'] ) : ?>
											<a href="http://facebook.com/<?php echo esc_attr( $team_meta['facebook'] ); ?>"><i class="fa fa-facebook"></i></a>
										<?php endif; 

										if ( $team_meta['twitter'] ) : ?>
											<a href="http://twitter.com/<?php echo esc_attr( $team_meta['twitter'] ); ?>"><i class="fa fa-twitter"></i></a>
										<?php endif;

										if ( $team_meta['email'] ) : ?>
											<a href="mailto:<?php echo esc_attr( $team_meta['email'] ); ?>"><i class="fa fa-envelope"></i></a>
										<?php endif;

										if ( $team_meta['linkedin'] ) : ?>
											<a href="<?php echo esc_url( $team_meta['linkedin'] ); ?>"><i class="fa fa-linkedin"></i></a>
										<?php endif; 

										if ( $team_meta['dribbble'] ) : ?>
											<a href="https://dribbble.com/<?php echo esc_url( $team_meta['dribbble'] ); ?>"><i class="fa fa-dribbble"></i></a>
										<?php endif; 

										if ( $team_meta['youtube'] ) : ?>
											<a href="http://youtube.com/<?php echo esc_attr( $team_meta['youtube'] ); ?>"><i class="fa fa-youtube"></i></a>
										<?php endif; 

										if ( $team_meta['skype'] ) : ?>
											<a href="skype:<?php echo esc_attr( $team_meta['skype'] ); ?>?chat"><i class="fa fa-skype"></i></a>
										<?php endif; ?>

										<?php 
											if ( ! empty( $team_meta['additional_social_account'] ) ) : ?>
										 		<?php foreach ( $team_meta['additional_social_account'] as $account ) : ?>
										 			<a href="<?php echo esc_url( $account['url'] ); ?>"><i class="<?php echo esc_attr( $account['icon'] ); ?>"></i></a>
										 		<?php endforeach; ?>	
										 <?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						<?php $delay +=175; ?>
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>

				</div>

			<?php endforeach; ?>
				
		</div>
	</div>
</div>