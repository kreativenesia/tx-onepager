<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

	$args = array(
		'post_type'				=> 'multipress-team',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 8,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$teams = new WP_Query( $args );
 ?>

<!-- Team Block Style 2 -->
<div id="multipress-element-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-team multipress-team--style-2 multipress-element-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="team-slider">
			<ul class="slides">

				<?php if ( $teams->have_posts() ) : ?>
					<?php while ( $teams->have_posts() ) : $teams->the_post(); ?>
						
						<?php 
							$thumbnail = has_post_thumbnail() ? _multipress_resize( _multipress_get_featured_image_url(), 40, 40 ) : 'http://unsplash.it/40/40';
						 ?>
						<li data-thumb="<?php echo esc_url( $thumbnail ); ?>">
							<div class="team">
								
								<?php if ( has_post_thumbnail() ) : ?>
									<div class="team__image">
										<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 500, 600 ); ?>" alt="<?php the_title(); ?>">
									</div>
								<?php endif; ?>

								<div class="team__details">
									<h2 class="team__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<small class="team__position"><?php echo _multipress_team_get_role(); ?></small>
									<div class="team__desc">
										<?php echo _multipress_team_get_biography(); ?>
									</div>

									<?php $team_meta = _multipress_get_meta( '_team_details' ); ?>

									<div class="team__social-link">
										<?php if ( ! empty( $team_meta['gplus'] ) ) : ?>
											<a href="<?php echo esc_url( $team_meta['gplus'] ); ?>"><i class="fa fa-google-plus"></i></a>
										<?php endif; 

										if ( ! empty( $team_meta['facebook'] ) ) : ?>
											<a href="http://facebook.com/<?php echo esc_attr( $team_meta['facebook'] ); ?>"><i class="fa fa-facebook"></i></a>
										<?php endif; 

										if ( ! empty( $team_meta['twitter'] ) ) : ?>
											<a href="http://twitter.com/<?php echo esc_attr( $team_meta['twitter'] ); ?>"><i class="fa fa-twitter"></i></a>
										<?php endif;

										if ( ! empty( $team_meta['email'] ) ) : ?>
											<a href="mailto:<?php echo esc_attr( $team_meta['email'] ); ?>"><i class="fa fa-envelope"></i></a>
										<?php endif;

										if ( ! empty( $team_meta['linkedin'] ) ) : ?>
											<a href="<?php echo esc_url( $team_meta['linkedin'] ); ?>"><i class="fa fa-linkedin"></i></a>
										<?php endif; 

										if ( ! empty( $team_meta['dribbble'] ) ) : ?>
											<a href="https://dribbble.com/<?php echo esc_url( $team_meta['dribbble'] ); ?>"><i class="fa fa-dribbble"></i></a>
										<?php endif; 

										if ( ! empty( $team_meta['youtube'] ) ) : ?>
											<a href="http://youtube.com/<?php echo esc_attr( $team_meta['youtube'] ); ?>"><i class="fa fa-youtube"></i></a>
										<?php endif; 

										if ( ! empty( $team_meta['skype'] ) ) : ?>
											<a href="skype:<?php echo esc_attr( $team_meta['skype'] ); ?>?chat"><i class="fa fa-skype"></i></a>
										<?php endif; ?>

										<?php 
											if ( ! empty( $team_meta['additional_social_account'] ) ) : ?>
												<?php foreach ( $team_meta['additional_social_account'] as $account ) : ?>
													<a href="<?php echo esc_url( $account['url'] ); ?>"><i class="<?php echo esc_attr( $account['icon'] ); ?>"></i></a>
												<?php endforeach; ?>	
										 <?php endif; ?>
									</div>
								</div>
							</div>
						</li>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>

				<?php else: ?>
					
					<?php $counter = 1; ?>
					<?php while ( 4 >= $counter ) : ?>

						<li data-thumb="http://unsplash.it/40/40">
							<div class="team">
								<div class="team__image"><img src="http://unsplash.it/500/600" alt=""></div>
								<div class="team__details">
									<h2 class="team__title">Jane Rachel Doe</h2>
									<small class="team__position">Project Manager</small>
									<div class="team__desc">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi dolorum officiis, ab odio sit, laudantium eaque dolores aut aliquam amet mollitia quam consectetur architecto magnam autem accusantium sint doloremque et?</p>
									</div>
									<div class="team__social-link">
										<a href="#"><i class="fa fa-facebook"></i></a>
										<a href="#"><i class="fa fa-twitter"></i></a>
										<a href="#"><i class="fa fa-linkedin"></i></a>
									</div>
								</div>
							</div>
						</li>

					<?php $counter ++; endwhile; ?>
					
				<?php endif; ?>

			</ul>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-team .team-slider").flexslider({
			controlNav    : "thumbnails",
			directionNav : false
		});
	</script>
<?php } ?>