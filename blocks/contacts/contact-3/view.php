<?php
	$container_size	 = ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 = ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height = ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg = ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

 ?>

<!-- Contact Style 3 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-contact--style-3 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<p class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></p>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="contact-form">
					<?php if ( ! empty( $contents['contact_form'] ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="'.$contents['contact_form'].'"]' ); ?>
					<?php else : ?>
						<div class="row">
							<div class="col-md-6">
								<div class="control">
									<input type="text" placeholder="Your Name" tabindex="1">
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="control">
											<select name="" id="" tabindex="3">
												<option value="">Budget Service</option>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="control">
											<select name="" id="" tabindex="4">
												<option value="">Service</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="control">
									<input type="text" placeholder="Your Email" tabindex="2">
								</div>
								<div class="control">
									<input type="text" placeholder="Your Phone" tabindex="5">
								</div>
							</div>
						</div>
						<textarea placeholder="Your Message"></textarea>
						<div class="align-center">
							<input type="submit" value="Send Message">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-map").each(function() {

			var mapEl       = jQuery(this),
				lat         = parseFloat(mapEl.data("lat")),
				lon         = parseFloat(mapEl.data("lon")),
				height      = parseInt(mapEl.data("height")),
				style       = mapEl.siblings(".map-style").length ? JSON.parse( mapEl.siblings(".map-style").html() ) : [],
				markerImage = mapEl.data("marker-image"),
				title       = mapEl.data("title"),
				content     = mapEl.data("content"),
				zoom        = mapEl.data("zoom") ? mapEl.data("zoom") : 13 ;

			mapEl.height( height );
			mapEl.gmap3({
				map: {
					options: {
						center            : [lat, lon],
						zoom              : zoom,
						mapTypeId         : google.maps.MapTypeId.ROADMAP,
						mapTypeControl    : false,
						navigationControl : true,
						scrollwheel       : false,
						streetViewControl : false,
						styles            : style
					}
				},
				marker: {
					latLng  : [lat, lon],
					data    : "<div class='tokoo-info-window'><h2>" + title + "</h2>" + content+"</div>",
					options : {
						icon: markerImage
					},
					events: {
						click: function(marker, event, context) {

							var thisMap    = jQuery(this).gmap3("get"),
								infowindow = jQuery(this).gmap3({
									get: {
										name: "infowindow"
									}
								});
							
							if (infowindow) {

								infowindow.open(thisMap, marker);
								infowindow.setContent(context.data);

							} else {

								jQuery(this).gmap3({
									infowindow: {
										anchor  : marker,
										options : {
											content: context.data
										}
									}
								});

							}

						},
					}
				},
			});

		});
	</script>
<?php } ?>
