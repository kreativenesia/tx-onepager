<?php
	$container_size	 = ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
?>

<!-- Contact Style 1 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-contact--style-1 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="contact-section">
			<div class="contact-map multipress-map" 
				data-lat="<?php echo ''.$contents['latitude']; ?>" 
				data-lon="<?php echo ''.$contents['longitude']; ?>" 
				data-zoom="<?php echo ''.$contents['zoom']; ?>" 
				data-title="<?php echo ''.$contents['title']; ?>" 
				data-content="<?php echo ''.$contents['content']; ?>">
			</div>
			<script class="map-style" type="application/json">
				<?php echo ''.$contents['style']; ?>
			</script>
			<div class="contact-form">
				<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
					<?php if ( ! empty( $contents['section_title'] ) ) : ?>
						<h2 class='section_title'><?php echo $contents['section_title']; ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
						<h3 class='section-title'><?php echo ''.$contents['section_subtitle']; ?></h3>
					<?php endif; ?>
				</header>

				<?php if ( ! empty( $contents['contact_form'] ) ) : ?>
					<?php echo do_shortcode( '[contact-form-7 id="'.$contents['contact_form'].'"]' ); ?>
				<?php else : ?>
					<form action="#">
						<div class="control">
							<input type="text" placeholder="Your name">
						</div>
						<div class="control">
							<input type="text" placeholder="Your email">
						</div>
						<div class="control">
							<textarea placeholder="Your message"></textarea>
						</div>
						<input type="submit" value="Send message">
					</form>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(document).ready(function(){
			jQuery(".multipress-map").each(function() {

				var mapEl       = jQuery(this),
				lat         = parseFloat(mapEl.data("lat")),
				lon         = parseFloat(mapEl.data("lon")),
				height      = parseInt(mapEl.data("height")),
				style       = mapEl.siblings(".map-style").length ? JSON.parse( mapEl.siblings(".map-style").html() ) : [],
				markerImage = mapEl.data("marker-image"),
				title       = mapEl.data("title"),
				content     = mapEl.data("content"),
				zoom        = mapEl.data("zoom") ? mapEl.data("zoom") : 13 ;

				mapEl.height( height );
				mapEl.gmap3({
					map: {
						options: {
							center            : [lat, lon],
							zoom              : zoom,
							mapTypeId         : google.maps.MapTypeId.ROADMAP,
							mapTypeControl    : false,
							navigationControl : true,
							scrollwheel       : false,
							streetViewControl : false,
							styles            : style
						}
					},
					marker: {
						latLng  : [lat, lon],
						data    : "<div class='tokoo-info-window'><h2>" + title + "</h2>" + content+"</div>",
						options : {
							icon: markerImage
						},
						events: {
							click: function(marker, event, context) {

								var thisMap    = jQuery(this).gmap3("get"),
								infowindow = jQuery(this).gmap3({
									get: {
										name: "infowindow"
									}
								});
								
								if (infowindow) {

									infowindow.open(thisMap, marker);
									infowindow.setContent(context.data);

								} else {

									jQuery(this).gmap3({
										infowindow: {
											anchor  : marker,
											options : {
												content: context.data
											}
										}
									});

								}

							},
						}
					},
				});

			});
		});
	</script>
	<?php } ?>

