<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
 ?>

<!-- Contact Style 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-contact--style-2 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) ) : ?>
			<h2 class="section-title"><?php echo ''.$contents['section_title']; ?></h2>
		<?php endif; ?>

		<div class="row">
			<div class="col-md-6">
				<?php if ( ! empty( $contents['contact_text'] ) ) : ?>
					<div class="text-wrapper">
						<?php echo wpautop( $contents['contact_text'] ); ?>
					</div>
				<?php endif; ?>

				<ul class="contact-info">
					<?php if ( ! empty( $contents['phone_number'] ) ) : ?>
						<li><i class="fa fa-phone"></i> <?php echo ''.$contents['phone_number']; ?></li>
					<?php endif; ?>
					<?php if ( ! empty( $contents['email_address'] ) ) : ?>
						<li><i class="fa fa-envelope-o"></i> <?php echo ''.$contents['email_address']; ?></li>
					<?php endif; ?>
				</ul>

				<div class="row">
					<div class="col-md-4">
						<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
							<h3><?php echo ''.$contents['section_subtitle']; ?></h3>
						<?php endif; ?>
					</div>
					<div class="col-md-8">
						<div class="contact-map multipress-map" 
							 data-lat="<?php echo ''.$contents['latitude']; ?>" 
							 data-lon="<?php echo ''.$contents['longitude']; ?>" 
							 data-zoom="<?php echo ''.$contents['zoom']; ?>" 
							 data-title="<?php echo ''.$contents['title']; ?>" 
							 data-content="<?php echo ''.$contents['content']; ?>">
						</div>
						<script class="map-style" type="application/json">
							<?php echo ''.$contents['style']; ?>
						</script>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">
				<div class="contact-form">
					<?php if ( ! empty( $contents['contact_form'] ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="'.$contents['contact_form'].'"]' ); ?>
					<?php else : ?>
						<form action="#">
							<div class="control">
								<input type="text" placeholder="Your name">
							</div>
							<div class="control">
								<input type="text" placeholder="Your email">
							</div>
							<div class="control">
								<textarea placeholder="Your message"></textarea>
							</div>
							<input type="submit" value="Send message">
						</form>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-map").each(function() {

			var mapEl       = jQuery(this),
				lat         = parseFloat(mapEl.data("lat")),
				lon         = parseFloat(mapEl.data("lon")),
				height      = parseInt(mapEl.data("height")),
				style       = mapEl.siblings(".map-style").length ? JSON.parse( mapEl.siblings(".map-style").html() ) : [],
				markerImage = mapEl.data("marker-image"),
				title       = mapEl.data("title"),
				content     = mapEl.data("content"),
				zoom        = mapEl.data("zoom") ? mapEl.data("zoom") : 13 ;

			mapEl.height( height );
			mapEl.gmap3({
				map: {
					options: {
						center            : [lat, lon],
						zoom              : zoom,
						mapTypeId         : google.maps.MapTypeId.ROADMAP,
						mapTypeControl    : false,
						navigationControl : true,
						scrollwheel       : false,
						streetViewControl : false,
						styles            : style
					}
				},
				marker: {
					latLng  : [lat, lon],
					data    : "<div class='tokoo-info-window'><h2>" + title + "</h2>" + content+"</div>",
					options : {
						icon: markerImage
					},
					events: {
						click: function(marker, event, context) {

							var thisMap    = jQuery(this).gmap3("get"),
								infowindow = jQuery(this).gmap3({
									get: {
										name: "infowindow"
									}
								});
							
							if (infowindow) {

								infowindow.open(thisMap, marker);
								infowindow.setContent(context.data);

							} else {

								jQuery(this).gmap3({
									infowindow: {
										anchor  : marker,
										options : {
											content: context.data
										}
									}
								});

							}

						},
					}
				},
			});

		});
	</script>
<?php } ?>

