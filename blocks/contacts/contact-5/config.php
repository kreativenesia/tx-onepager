<?php

return array(
	'slug' 		=> 'multipress-contact-5', 
	'name' 		=> esc_html__( 'Multipress  Contact 5', 'multipress' ),
	'groups' 	=> array( 'contact' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Contact Us'
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae voluptatem placeat labore dolores quasi'
			),
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name' 		=> 'phone',
				'label' 	=> esc_html__( 'Phone Number', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '0222503530'
			),
			array(
				'name' 		=> 'fax',
				'label' 	=> esc_html__( 'Fax Number', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '0222503530'
			),
			array(
				'name' 		=> 'email',
				'label' 	=> esc_html__( 'Email Address', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '0222503530'
			),
			array(
				'name'		=> 'contact_form',
				'label' 	=> esc_html__( 'Contact Form', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> '',
				'options' 	=> _multipress_get_cf7_list_form(),
			),
	),
	
	'settings'	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),

	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#fff'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
				'value'		=> 'http://unsplash.it/1600/991',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'cover',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'section_subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'item_heading_color',
				'label' 	=> esc_html__( 'Item Heading Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_heading_font_size',
				'label' 	=> esc_html__( 'Item Heading Font Size', 'multipress' ),
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'item_content_color',
				'label' 	=> esc_html__( 'Item Content Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_content_font_size',
				'label' 	=> esc_html__( 'Item Content Font Size', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_form_background_color',
				'label' 	=> esc_html__( 'Item Form Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
	)
);