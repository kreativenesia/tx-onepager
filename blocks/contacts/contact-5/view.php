<?php
	$container_size	 = ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
 ?>

<!-- Contact Style 5 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-contact--style-5 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="contact-detail-section">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
				<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
					<?php if ( ! empty( $contents['section_title'] ) ) : ?>
						<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
						<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
					<?php endif; ?>
				</header>
			<?php endif; ?>

			<div class="contact-detail">
				<div class="contact-info">
					<?php if ( ! empty( $contents['phone'] ) ) : ?>
						<i class="fa fa-phone"></i>
						<a href="tel:<?php echo esc_attr( $contents['phone'] ); ?>"><?php echo esc_attr( $contents['phone'] ); ?></a>
					<?php endif; ?>
				</div>
				<div class="contact-info">
					<?php if ( ! empty( $contents['fax'] ) ) : ?>
						<i class="fa fa-fax"></i>
						<a href="fax:<?php echo esc_attr( $contents['fax'] ); ?>"><?php echo esc_attr( $contents['fax'] ); ?></a>
					<?php endif; ?>
				</div>
				<div class="contact-info">
					<?php if ( ! empty( $contents['email'] ) ) : ?>
						<i class="fa fa-envelope-o"></i>
						<a href="mailto:<?php echo esc_attr( $contents['email'] ); ?>"><?php echo esc_attr( $contents['email'] ); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="contact-form-section">
		<div class="container">
			<div class="row">
				<?php if ( ! empty( $contents['contact_form'] ) ) : ?>
					<?php echo do_shortcode( '[contact-form-7 id="'.$contents['contact_form'].'"]' ); ?>
				<?php else : ?>
					<div class="col-md-4 col-md-offset-2">
						<div class="control">
							<textarea placeholder="Your message"></textarea>
						</div>
					</div>
					<div class="col-md-4">
						<div class="control">
							<input type="text" placeholder="Your Name">
						</div>
						<div class="control">
							<input type="text" placeholder="Your Email">
						</div>
						<div class="control align-right">
							<input type="submit" value="Send Message">
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-map").each(function() {

			var mapEl       = jQuery(this),
				lat         = parseFloat(mapEl.data("lat")),
				lon         = parseFloat(mapEl.data("lon")),
				height      = parseInt(mapEl.data("height")),
				style       = mapEl.siblings(".map-style").length ? JSON.parse( mapEl.siblings(".map-style").html() ) : [],
				markerImage = mapEl.data("marker-image"),
				title       = mapEl.data("title"),
				content     = mapEl.data("content"),
				zoom        = mapEl.data("zoom") ? mapEl.data("zoom") : 13 ;

			mapEl.height( height );
			mapEl.gmap3({
				map: {
					options: {
						center            : [lat, lon],
						zoom              : zoom,
						mapTypeId         : google.maps.MapTypeId.ROADMAP,
						mapTypeControl    : false,
						navigationControl : true,
						scrollwheel       : false,
						streetViewControl : false,
						styles            : style
					}
				},
				marker: {
					latLng  : [lat, lon],
					data    : "<div class='tokoo-info-window'><h2>" + title + "</h2>" + content+"</div>",
					options : {
						icon: markerImage
					},
					events: {
						click: function(marker, event, context) {

							var thisMap    = jQuery(this).gmap3("get"),
								infowindow = jQuery(this).gmap3({
									get: {
										name: "infowindow"
									}
								});
							
							if (infowindow) {

								infowindow.open(thisMap, marker);
								infowindow.setContent(context.data);

							} else {

								jQuery(this).gmap3({
									infowindow: {
										anchor  : marker,
										options : {
											content: context.data
										}
									}
								});

							}

						},
					}
				},
			});

		});
	</script>
<?php } ?>
