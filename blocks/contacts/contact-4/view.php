<?php
	$container_size	 			= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
 ?>

<!-- Contact Style 4 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-contact--style-4 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row">
			<div class="col-md-8">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2><?php echo ''.$contents['section_title']; ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['contact_image'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['contact_image'] ); ?>" alt="<?php esc_html_e( 'contact Image', 'multipress' ); ?>">
				<?php endif; ?>
			</div>

			<div class="col-md-4">
				<h2><?php esc_html_e( 'Write Us', 'multipress' ); ?></h2>
				<div class="contact-form">
					<?php if ( ! empty( $contents['contact_form'] ) ) : ?>
						<?php echo do_shortcode( '[contact-form-7 id="'.$contents['contact_form'].'"]' ); ?>
					<?php else : ?>
						<form action="#">
							<div class="control">
								<input type="text" placeholder="Your name">
							</div>
							<div class="control">
								<input type="text" placeholder="Your email">
							</div>
							<div class="control">
								<textarea placeholder="Your message"></textarea>
							</div>
							<input type="submit" value="Send message">
						</form>
					<?php endif; ?>
				</div>
			</div>
		</div>
		
		<div class="contact-detail">
			<div class="row">
				<div class="col-md-6">
					<div class="contact-info">
						<h3><?php esc_html_e( 'Address', 'multipress' ); ?></h3>
						<?php if ( ! empty( $contents['address'] ) ) : ?>
							<?php echo wpautop( $contents['address'] ); ?>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="contact-info">
						<?php if ( ! empty( $contents['phone'] ) ) : ?>
							<h3><?php esc_html_e( 'Phone', 'multipress' ); ?></h3>
							<a href="tel:<?php echo esc_attr( $contents['phone'] ); ?>"><?php echo esc_attr( $contents['phone'] ); ?></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="contact-info">
						<?php if ( ! empty( $contents['fax'] ) ) : ?>
							<h3><?php esc_html_e( 'Fax', 'multipress' ); ?></h3>
							<a href="fax:<?php echo esc_attr( $contents['fax'] ); ?>"><?php echo esc_attr( $contents['fax'] ); ?></a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-md-2">
					<div class="contact-info">
						<?php if ( ! empty( $contents['email'] ) ) : ?>
							<h3><?php esc_html_e( 'Email', 'multipress' ); ?></h3>
							<a href="mailto:<?php echo esc_attr( $contents['email'] ); ?>"><?php echo esc_attr( $contents['email'] ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-map").each(function() {

			var mapEl       = jQuery(this),
				lat         = parseFloat(mapEl.data("lat")),
				lon         = parseFloat(mapEl.data("lon")),
				height      = parseInt(mapEl.data("height")),
				style       = mapEl.siblings(".map-style").length ? JSON.parse( mapEl.siblings(".map-style").html() ) : [],
				markerImage = mapEl.data("marker-image"),
				title       = mapEl.data("title"),
				content     = mapEl.data("content"),
				zoom        = mapEl.data("zoom") ? mapEl.data("zoom") : 13 ;

			mapEl.height( height );
			mapEl.gmap3({
				map: {
					options: {
						center            : [lat, lon],
						zoom              : zoom,
						mapTypeId         : google.maps.MapTypeId.ROADMAP,
						mapTypeControl    : false,
						navigationControl : true,
						scrollwheel       : false,
						streetViewControl : false,
						styles            : style
					}
				},
				marker: {
					latLng  : [lat, lon],
					data    : "<div class='tokoo-info-window'><h2>" + title + "</h2>" + content+"</div>",
					options : {
						icon: markerImage
					},
					events: {
						click: function(marker, event, context) {

							var thisMap    = jQuery(this).gmap3("get"),
								infowindow = jQuery(this).gmap3({
									get: {
										name: "infowindow"
									}
								});
							
							if (infowindow) {

								infowindow.open(thisMap, marker);
								infowindow.setContent(context.data);

							} else {

								jQuery(this).gmap3({
									infowindow: {
										anchor  : marker,
										options : {
											content: context.data
										}
									}
								});

							}

						},
					}
				},
			});

		});
	</script>
<?php } ?>

