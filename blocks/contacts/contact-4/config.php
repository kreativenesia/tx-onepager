<?php

return array(
	'slug' 		=> 'multipress-contact-4', 
	'name' 		=> esc_html__( 'Multipress  Contact 4', 'multipress' ),
	'groups' 	=> array( 'contact' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'We Are Everywhere'
			),
			array(
				'name' 		=> 'contact_image',
				'label' 	=> esc_html__( 'Contact Image', 'multipress' ),
				'type' 		=> 'image',
				'value'		=> ONEPAGER_URL . '/assets/images/map.png',
			),
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name' 		=> 'address',
				'label' 	=> esc_html__( 'Address', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos id corporis magni error unde accusantium possimus.'
			),
			array(
				'name' 		=> 'phone',
				'label' 	=> esc_html__( 'Phone Number', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '0222503530'
			),
			array(
				'name' 		=> 'fax',
				'label' 	=> esc_html__( 'Fax Number', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '0222503530'
			),
			array(
				'name' 		=> 'email',
				'label' 	=> esc_html__( 'Email Address', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '0222503530'
			),
			array(
				'name'		=> 'contact_form',
				'label' 	=> esc_html__( 'Contact Form', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> '',
				'options' 	=> _multipress_get_cf7_list_form(),
			),
	),
	
	'settings'	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
	),

	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#fff'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'cover',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'item_heading_color',
				'label' 	=> esc_html__( 'Item Heading Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_heading_font_size',
				'label' 	=> esc_html__( 'Item Heading Font Size', 'multipress' ),
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'item_content_color',
				'label' 	=> esc_html__( 'Item Content Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_content_font_size',
				'label' 	=> esc_html__( 'Item Content Font Size', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
	)
);