<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 10 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-image multipress-image-type-1 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="single-image-section align-<?php echo ''.$settings['image_alignment']; ?>" style="background-color:<?php echo ''.$contents['bg_content']; ?>">
			<?php if ( ! empty( $contents['image'] ) ) : ?>
				<?php if ( ! empty( $contents['image_link'] ) ) : ?>
					<a href="<?php echo esc_url( $contents['image_link'] ); ?>">
				<?php endif; ?>
					<img src="<?php echo esc_url( $contents['image'] ); ?>">
				<?php if ( ! empty( $contents['image_link'] ) ) : ?>
					</a>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
