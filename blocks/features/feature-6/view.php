<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$chunked_contents 	= array_chunk( $contents['items'], $settings['columns'] );
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section features feature-section-style-6 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="feature-list style-6 columns-<?php echo esc_attr( $settings['columns'] ); ?>">
			<?php foreach( $chunked_contents as $features ) : ?>

				<div class="feature-row">
					<?php $delay = 100; ?>
					<?php foreach( $features as $feature ) : ?>
						
						<?php $icon_class 	= ! empty( $feature['media'] ) ? $feature['media'] : ''; ?>
						<?php $icon_color 	= ! empty( $feature['icon_color'] ) ? 'style="color:'.$feature['icon_color'].'"' : '' ; ?>

						<div class="feature wow fadeIn" data-wow-delay="<?php echo ''.$delay; ?>ms" >
							<div class="feature__image">
								<div class="fa <?php echo esc_attr( $icon_class ); ?>" <?php echo ''.$icon_color; ?>></div>
							</div>
							<div class="feature__content">
								<h2 class="feature__title">
									<?php if ( ! empty( $feature['link'] ) ) : ?>
										<a href="<?php echo esc_url( $feature['link'] ); ?>"><?php echo esc_attr( $feature['title'] ); ?></a>
									<?php else : ?>
										<?php echo esc_attr( $feature['title'] ); ?>
									<?php endif; ?>
								</h2>
								<?php if ( ! empty( $feature['description'] ) ) : ?>
									<div class="feature__desc">
										<?php echo ''.$feature['description']; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
						<?php $delay +=175; ?>
					<?php endforeach; ?>
				
				</div>

			<?php endforeach; ?>
		</div>
	</div>
</div>