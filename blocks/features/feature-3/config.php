<?php

/*
	
	content : Ini mirip kaya feature yang sebelumnya dibuat

	di repeater, nambah icon color > inline style color di .fa/.feature__imagenya

	settings:
		Column : [select: 2-5] liat block sebelumnya
		
		content alignment : [select: left, center, right]
							nambah class di feature-list .align-center/.align-right

		Icon Position : [Select: Top, Left]
						Nambah class di feature-list .icon-left



 */

return array(
	'slug' 		=> 'multipress-features-3',
	'name' 		=> esc_html__( 'Features Style 3', 'multipress' ),
	'groups' 	=> array( 'features' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> ''
			),
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=>'items',
				'type'		=>'repeater',
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Prefessional Works'
						),
						array(
							'name' 		=> 'description', 
							'type'		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, error ab non! Sequi, maxime, et unde enim totam ad fugiat debitis repellat ea nulla earum odit eum perferendis libero explicabo.'
						),
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> 'fa-5x', 
							'value' 	=> 'fa-laptop' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#5db6de'
						),
						array(
							'name' 			=> 'link', 
							'placeholder'	=> get_home_url()
						),
						array(
							'name' 		=> 'target', 
							'label' 	=> 'open in new window', 
							'type' 		=> 'switch' 
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Fast Delivery'
						),
						array(
							'name' 		=> 'description', 
							'type'		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, error ab non! Sequi, maxime, et unde enim totam ad fugiat debitis repellat ea nulla earum odit eum perferendis libero explicabo.'
						),
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> 'fa-5x', 
							'value' 	=> 'fa-heart-o'
						),
						array(
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#6656a1'
						),
						array(
							'name' 			=> 'link', 
							'placeholder' 	=> get_home_url()
						),
						array( 
							'name' 		=> 'target', 
							'label' 	=> 'open in new window', 
							'type' 		=> 'switch'
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value'  	=> 'Top-notch Support'
						),
						array(
							'name' 		=> 'description', 
							'type'		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, error ab non! Sequi, maxime, et unde enim totam ad fugiat debitis repellat ea nulla earum odit eum perferendis libero explicabo.'
						),
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> 'fa-5x', 
							'value' 	=> 'fa-bookmark-o'
						),
						array(
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#d352c6'
						),
						array(
							'name' 			=> 'link', 
							'placeholder'	=> get_home_url()
						),
						array(
							'name' 		=> 'target', 
							'label' 	=> 'open in new window', 
							'type' 		=> 'switch'
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Wonderful artwork'
						),
						array(
							'name' 		=> 'description', 
							'type'		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti, error ab non! Sequi, maxime, et unde enim totam ad fugiat debitis repellat ea nulla earum odit eum perferendis libero explicabo.'
						),
						array(
							'name' 		=> 'media', 
							'type'		=> 'icon', 
							'size' 		=> 'fa-5x', 
							'value' 	=> 'fa-lightbulb-o'
						),
						array(
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value'		=> '#da525e'
						),
						array(
							'name' 			=> 'link', 
							'placeholder'	=> get_home_url()
						),
						array(
							'name'		=> 'target', 
							'label'		=> 'open in new window', 
							'type'		=> 'switch'
						),
					),
				)
			)
	),
	
	'settings' 	=> array(
		array(
			'name' 		=> 'section_alignment',
			'label' 	=> esc_html__( 'Section Heading Alignment', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'center',
			'options' 	=> array(
				'left' 		=> 'Left',
				'center' 	=> 'Center',
				'right' 	=> 'Right',
			)
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Section Container', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'normal',
				'container-fluid' 	=> 'fullwidth'
			)
		),
		array(
			'name'		=> 'content_alignment',
			'label' 	=> esc_html__( 'Content Alignment', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'align-left' 	=> esc_html__( 'Left', 'multipress' ),
				'align-center' 	=> esc_html__( 'Center', 'multipress' ),
				'align-right' 	=> esc_html__( 'Right', 'multipress' ),
			)
		),
		array(
			'name' 		=> 'icon_position',
			'label' 	=> esc_html__( 'Icon Position', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'top',
			'options' 	=> array(
				'top' 		=> 'top',
				'icon_left' => 'left',
				'icon_right' => 'right',
			)
		),
		array(
			'name'     => 'columns',
			'label'    => esc_html__( 'Columns', 'multipress' ),
			'type'     => 'select',
			'value'    => '4',
			'options'  => array(
				'2'   => '2',
				'3'   => '3',
				'4'   => '4',
				'5'   => '5'
			),
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#fff'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'section_subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'item_title_color',
				'label' 	=> esc_html__( 'Item Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_title_font_size',
				'label' 	=> esc_html__( 'Item Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'item_content_color',
				'label' 	=> esc_html__( 'Item Content Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_content_font_size',
				'label' 	=> esc_html__( 'Item Content Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
 			
	),
);
