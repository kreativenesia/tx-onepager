<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 3 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-3 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="col-md-8">
			<?php if ( ! empty( $contents['title'] ) ) : ?>
				<h2 class="h2 wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
			<?php endif; ?>
			<?php if ( ! empty( $contents['content'] ) ) : ?>
				<p class="text14 wow fadeInUp"><?php echo ''.$contents['content']; ?></p>
			<?php endif; ?>
			<?php if ( ! empty( $contents['button_text_1'] ) ) : ?>
				<?php $target ='';
					if ( ! empty( $contents['button_target1'])){
						$target = 'target="_blank"';
					}				
				?>
				<a href="<?php echo esc_url( $contents['button_link_1'] ); ?>" class="button wow fadeInUp" <?php echo '' . $target; ?>><?php echo esc_attr( $contents['button_text_1'] ); ?></a> 
			<?php endif; ?>
			<?php if ( ! empty( $contents['button_text_2'] ) ) : ?>
				<?php $target2 ='';
					if ( ! empty( $contents['button_target2'])){
						$target2 = 'target="_blank"';
					}				
				?>
				<a href="<?php echo esc_url( $contents['button_link_2'] ); ?>" class="button link wow fadeInUp" <?php echo '' . $target2; ?>><?php echo esc_attr( $contents['button_text_2'] ); ?></a>
			<?php endif; ?>
		</div>
		
		<div class="col-md-4">
			<a href="<?php echo esc_url( $contents['video_url'] ); ?>" class="popup-youtube">
				<?php if ( ! empty( $contents['image'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>" class="wow fadeInRight">
				<?php else : ?>
					<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/video-preview.jpg" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>" class="wow fadeInRight">
				<?php endif; ?>
			</a>
		</div>
	</div>
</div>