<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 6 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-6 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="section-header">
			<?php if ( ! empty( $contents['image'] ) ) : ?>
				<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>" class="mb100 wow fadeInUp">
			<?php else : ?>
				<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/sofa.png" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>" class="mb100 wow fadeInUp">
			<?php endif; ?>
			<?php if ( ! empty( $contents['title'] ) ) : ?>
				<h2 class="section-title wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
			<?php endif; ?>
		</div>
		
		<div class="row">
			<div class="col-md-8 col-md-offset-2 align-center">
				<?php if ( ! empty( $contents['content'] ) ) : ?>
					<p class="text14 wow fadeInUp"><?php echo ''.$contents['content']; ?></p>
				<?php endif; ?>
				<?php if ( ! empty( $contents['button_text'] ) ) : ?>
					<?php $target1 ='';
						if ( ! empty( $contents['button_target'])){
							$target1 = 'target="_blank"';
						}				
					?>
					<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button big wow fadeInUp" <?php echo '' . $target1 ?>><?php echo esc_attr( $contents['button_text'] ); ?></a> 
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>