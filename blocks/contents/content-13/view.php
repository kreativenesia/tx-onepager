<?php
	$container_size	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height = ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg = ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

?> 

<!-- Multipress Content 13 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-13 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="section-header">
			<?php if ( ! empty( $contents['title'] ) ) : ?>
				<h2 class="section-title wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
			<?php endif; ?>
		</div>

		<div class="row">
			<?php $delay = 150; ?>
			<?php foreach( $contents['top_items'] as $feature ) : ?>
					
				<?php if ( ! empty( $feature['content'] ) ) : ?>
					
					<div class="col-md-6 wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
						<p class="text14"><?php echo ''.$feature['content']; ?></p>
					</div>

				<?php endif; ?>	
				<?php $delay +=175; ?>
			<?php endforeach; ?>
		</div>

		<hr class="lighter mt50 mb50">
		
		<div class="image-block mb30">
			<?php if ( ! empty( $contents['image'] ) ) : ?>
				<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>" class="wow fadeInUp">
			<?php else : ?>
				<img src="http://unsplash.it/1600/480" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>" class="wow fadeInUp">
			<?php endif; ?>
		</div>
		
		<div class="row">
			
			<?php $delay = 150; $counter = 1; ?>
			<?php foreach( $contents['bottom_items'] as $feature ) : ?>
				
				<?php $offset_class = ( 1 !== $counter ) ? ' col-md-offset-1' : ''; ?>
				<div class="col-md-3<?php echo esc_attr( $offset_class ); ?> wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
					<?php if ( ! empty( $feature['title'] ) ) : ?>
						<h3 class="h4"><?php echo esc_attr( $feature['title'] ); ?></h3>
					<?php endif; ?>
					<?php if ( ! empty( $feature['content'] ) ) : ?>
					<p>
						<?php echo ''.$feature['content']; ?>
					</p>
				<?php endif; ?>	
				</div>
				
			<?php $delay +=175; $counter++; endforeach; ?>

		</div>
	</div>
</div>
