<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?> 

<!-- Multipress Content 12 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-12 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="section-header">
			<?php if ( ! empty( $contents['title'] ) ) : ?>
				<h2 class="section-title wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
			<?php endif; ?>
		</div>

		<div class="row">
			<div class="col-md-3">
				<?php $delay = 150; ?>
				<?php $left_counter = 1; ?>
				<?php foreach( $contents['left_items'] as $feature ) : ?>
					
					<?php $mt_class = ( 1 == $left_counter ) ? ' mt50' : ''; ?>
					<div class="feature-content <?php echo esc_attr( $mt_class ); ?> wow fadeInLeft" data-wow-delat="<?php echo ''.$delay; ?>ms">
						<?php if ( ! empty( $feature['title'] ) ) : ?>
							<h3 class="h4"><?php echo esc_attr( $feature['title'] ); ?></h3>
						<?php endif; ?>
						<?php if ( ! empty( $feature['content'] ) ) : ?>
							<p>
								<?php echo ''.$feature['content']; ?>
							</p>
						<?php endif; ?>	
					</div>
					
				<?php $delay +=175; $left_counter++; endforeach; ?>
			</div>

			<div class="col-md-4 col-md-offset-1">
				<?php if ( ! empty( $contents['image'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>" class="wow fadeInUp">
				<?php else : ?>
					<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/preview.png" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>" class="wow fadeInUp">
				<?php endif; ?>
			</div>
			
			<div class="col-md-3 col-md-offset-1">
				
				<?php 
					$delay = 150;
					$right_counter = 1; 
				?>
				<?php foreach( $contents['right_items'] as $feature ) : ?>
					
					<?php $mt_class = ( 1 == $right_counter ) ? ' mt50' : ''; ?>
					<div class="feature-content wow fadeInRight" data-wow-delay="<?php echo ''.$delay; ?>ms">
						<?php if ( ! empty( $feature['title'] ) ) : ?>
							<h3 class="h4 <?php echo esc_attr( $mt_class ); ?>"><?php echo esc_attr( $feature['title'] ); ?></h3>
						<?php endif; ?>
						<?php if ( ! empty( $feature['content'] ) ) : ?>
							<p>
								<?php echo ''.$feature['content']; ?>
							</p>
						<?php endif; ?>	
						
					</div>
					
				<?php $delay += 175; $right_counter++; endforeach; ?>
				
			</div>
		</div>
	</div>
</div>
