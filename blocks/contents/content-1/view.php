<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 1 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-1 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row">
			<div class="col-md-7">
				<?php if ( ! empty( $contents['title'] ) ) : ?>
					<h2 class="for-section h2 wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['content'] ) ) : ?>
					<div class="for-content text14 wow fadeInUp" data-wow-delay="150ms">
						<?php echo wpautop( $contents['content'] ); ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-4 col-md-offset-1">
				<?php if ( ! empty( $contents['image'] ) ) : ?>
			    	<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>" class="wow fadeInUp">
				<?php else : ?>
					<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/name-card.png" alt="" class="wow fadeInUp">
				<?php endif; ?>
			</div>
		</div>

		<hr class="mt30 mb30 lighter">

		<div class="row">
			
			<?php 
				$delay   = 150;
				$counter = 1; 
			?>
			<?php foreach( $contents['items'] as $feature ) : ?>
				
				<?php if ( 3 >= $counter ) : ?>
					
					<?php $offset_class 	= ( 1 !== $counter ) ? ' col-md-offset-1' : ''; ?>
					<?php $title_color 		= ! empty( $feature['title_color'] ) ? $feature['title_color'] : ''; ?>
					<?php $title_size 		= ! empty( $feature['title_font_size'] ) ? $feature['title_font_size'] : ''; ?>
					<?php $content_color 	= ! empty( $feature['content_color'] ) ? $feature['content_color'] : ''; ?>
					<?php $content_size 	= ! empty( $feature['content_font_size'] ) ? $feature['content_font_size'] : ''; ?>
					<?php $title_styles 	= ! empty( $feature['title_color'] ) || ! empty( $feature['title_font_size'] )  ? "style='color:{$title_color}; font-size:{$title_size}'" : ''; ?>
					<?php $content_styles 	= ! empty( $feature['content_color'] ) || ! empty( $feature['content_font_size'] )  ? "style='color:{$content_color}; font-size:{$content_size}'" : ''; ?>
				
					<div class="col-md-3<?php echo esc_attr( $offset_class ); ?> wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms" <?php echo ''.$content_styles; ?>>
						<h2 class="h4" <?php echo ''.$title_styles ?>><?php echo esc_attr( $feature['title'] ); ?></h2>
						<?php if ( ! empty( $feature['content'] ) ) : ?>
							<?php echo wpautop( $feature['content'] ); ?>
						<?php endif; ?>
					</div>
					
				<?php endif; ?>
				
			<?php $delay += 175; $counter++; endforeach; ?>
			
		</div>
	</div>
</div>