<?php

return array(
	'slug'    	=> 'multipress-content-1',
	'name' 		=> esc_html__( 'Multipress Content 1', 'multipress' ),
	'groups'    => array( 'contents' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Top'
		),
			array(
				'name' 		=> 'title',
				'label' 	=> esc_html__( 'Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Curabitur purus purus'
			),
			array(
				'name' 		=> 'content',
				'label' 	=> esc_html__( 'Content', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates ducimus natus labore animi corporis nesciunt fuga, voluptatibus neque quisquam quaerat eos vero aperiam ipsum maxime, quasi. Assumenda ratione laborum quam.'
			),
			array(
				'name' 		=> 'image',
				'label' 	=> esc_html__( 'Image', 'multipress' ),
				'type' 		=> 'image',
			),
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=>'items',
				'type'		=>'repeater',
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Pellentesque aliquam'
						),
						array(
							'name' 		=> 'title_color',
							'label' 	=> esc_html__( 'Title Color', 'multipress' ),
							'type' 		=> 'colorpicker',
						),
						array(
							'name' 		=> 'title_font_size',
							'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
							'type' 		=> 'text',
						),
						array(
							'name' 		=> 'content', 
							'type' 		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum sed ipsa, doloribus nihil, impedit suscipit quia voluptatum amet qui tempore optio perferendis atque ex recusandae, dolorem aliquid illum modi odit.'
						),
						array(
							'name' 		=> 'content_color',
							'label' 	=> esc_html__( 'Content Color', 'multipress' ),
							'type' 		=> 'colorpicker',
						),
						array(
							'name' 		=> 'content_font_size',
							'label' 	=> esc_html__( 'Content Font Size', 'multipress' ),
							'type' 		=> 'text',
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Donec feugiat ligula augue'
						),
						array(
							'name' 		=> 'title_color',
							'label' 	=> esc_html__( 'Title Color', 'multipress' ),
							'type' 		=> 'colorpicker',
						),
						array(
							'name' 		=> 'title_font_size',
							'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
							'type' 		=> 'text',
						),
						array(
							'name' 		=> 'content', 
							'type' 		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet fugiat velit ipsa ad, necessitatibus nulla a, maxime voluptatum, inventore voluptates pariatur optio dolor nostrum reprehenderit asperiores atque perspiciatis dignissimos non.'
						),
						array(
							'name' 		=> 'content_color',
							'label' 	=> esc_html__( 'Content Color', 'multipress' ),
							'type' 		=> 'colorpicker',
						),
						array(
							'name' 		=> 'content_font_size',
							'label' 	=> esc_html__( 'Content Font Size', 'multipress' ),
							'type' 		=> 'text',
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Nam felis ante'
						),
						array(
							'name' 		=> 'title_color',
							'label' 	=> esc_html__( 'Title Color', 'multipress' ),
							'type' 		=> 'colorpicker',
						),
						array(
							'name' 		=> 'title_font_size',
							'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
							'type' 		=> 'text',
						),
						array(
							'name' 		=> 'content', 
							'type' 		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur, quam harum corporis fugit et iure doloremque soluta ducimus sequi saepe, quos sed nam temporibus repellat praesentium! Repellat laboriosam, fuga molestiae.'
						),
						array(
							'name' 		=> 'content_color',
							'label' 	=> esc_html__( 'Content Color', 'multipress' ),
							'type' 		=> 'colorpicker',
						),
						array(
							'name' 		=> 'content_font_size',
							'label' 	=> esc_html__( 'Content Font Size', 'multipress' ),
							'type' 		=> 'text',
						),
					),
				)
			)
	),

	'settings' 	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#fff'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),

			array(
				'name' => 'bg_video',
				'label' => esc_html__( 'Youtube Video Background', 'multipress' ),
				'type' => 'text',
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Content Styling'
		),
			array(
				'name' 		=> 'title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'content_color',
				'label' 	=> esc_html__( 'Content Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'content_font_size',
				'label' 	=> esc_html__( 'Content Font Size', 'multipress' ),
				'type' 		=> 'text',
			),

	),

);
