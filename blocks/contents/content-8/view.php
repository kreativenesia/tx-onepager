<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 8 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-8 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<div class="col-md-6">
						<?php if ( ! empty( $contents['title'] ) ) : ?>
							<h2 class="h2 wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $contents['content'] ) ) : ?>
							<p class="text14 wow fadeInUp"><?php echo ''.$contents['content']; ?></p>
						<?php endif; ?>

						<div class="features left_icon">
							<div class="feature-list">
								<?php $delay = 150; ?>
								<?php foreach( $contents['items'] as $feature ) : ?>
			
									<?php $icon_color = ! empty( $feature['icon_color'] ) ? 'style="color:'.$feature['icon_color'].'"': ''; ?>
									<div class="feature wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
										<div class="feature_content">
											<i class="fa <?php echo esc_attr( $feature['media'] ); ?>" <?php echo ''.$icon_color; ?>></i>
											<?php if ( ! empty( $feature['title'] ) ) : ?>
												<h3 class="feature_title"><?php echo esc_attr( $feature['title'] ); ?></h3>
											<?php endif; ?>
											<?php if ( ! empty( $feature['description'] ) ) : ?>
												<p><?php echo ''.$feature['description']; ?></p>
											<?php endif; ?>
										</div>
									</div>
									<?php $delay +=175; ?>
								<?php endforeach; ?>
								
							</div>
						</div>
					</div>
					
					<div class="col-md-4 col-md-offset-2">
						<?php if ( ! empty( $contents['image'] ) ) : ?>
							<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
						<?php else : ?>
							<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/iphone.png" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
