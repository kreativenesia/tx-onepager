<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-2 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header">
				<?php if ( ! empty( $contents['section_image'] ) ) : ?>
					<img class="mb50" src="<?php echo esc_url( $contents['section_image'] ); ?>" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>" clss="wow fadeInUp">
				<?php else : ?> 
					<img class="mb50" src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/clipboard.png" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>" clss="wow fadeInUp">
				<?php endif; ?>

				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<p class='section-subtitle wow fadeInUp'><?php echo ''.$contents['section_subtitle']; ?></p>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="features left_icon">
			<div class="feature-list columns-<?php echo esc_attr( $settings['columns'] ); ?>">
				<?php $delay = 150; ?>
				<?php foreach( $contents['items'] as $feature ) : ?>
					
					<div class="feature wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
						<div class="feature_content">
							<div class="feature_image">
								<i class="fa <?php echo esc_attr( $feature['media'] ); ?>"></i>
							</div>
							<?php if ( ! empty( $feature['title'] ) ) : ?>
								<h3 class="feature_title"><?php echo esc_attr( $feature['title'] ); ?></h3>
							<?php endif; ?>
							<?php if ( ! empty( $feature['content'] ) ) : ?>
								<p class="feature_desc">
									<?php echo ''.$feature['content']; ?>
								</p>
							<?php endif; ?>
						</div>
					</div>
					
				<?php $delay +=175; endforeach; ?>

			</div>
		</div>
	</div>
</div>