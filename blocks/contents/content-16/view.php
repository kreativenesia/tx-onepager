<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>
 
<!-- Multipress Content 16 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-16 section-highlight multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<?php if ( ! empty( $contents['image'] ) ) : ?>
		<?php if ( ! empty( $settings['image_position'] ) && 'image-right' === $settings['image_position'] ): ?>
			<div class="col-md-6 col-md-push-6 edge-image wow fadeInRight">
		<?php else : ?>
			<div class="col-md-6 edge-image wow fadeInLeft">
		<?php endif; ?>
			<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="">
		</div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">

		<?php if ( empty( $contents['image'] ) ): ?>
			<div class="row use-bg">
		<?php else: ?>
			<div class="row">
		<?php endif; ?>
			<?php if ( ! empty( $settings['image_position'] ) && 'image-left' === $settings['image_position'] ) : ?>
				<div class="col-md-5 col-md-push-7">
			<?php else: ?>
				<div class="col-md-5">
			<?php endif; ?>
				
				<?php if ( ! empty( $contents['title'] ) ) : ?>
					<h2 class="h2 text-white wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['content'] ) ) : ?>
					<div class="text14 text-gray wow fadeInUp" data-wow-delay="300ms"><?php echo ''.$contents['content']; ?></div>
				<?php endif; ?>

				<?php if ( ! empty( $contents['button_text'] ) ) : ?>
					<?php $target1 ='';
						if ( ! empty( $contents['button_target'])){
							$target1 = 'target="_blank"';
						}				
					?>
					<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button big wow fadeInUp" data-wiw-delay="375ms" <?php echo '' . $target1 ?>><?php echo ''.$contents['button_text']; ?></a>
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>