<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 19 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-19 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<header class="section-header">
			<?php if ( ! empty( $contents['title'] ) ) : ?>
				<h2 class="section-title wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
			<?php endif; ?>
			<?php if ( ! empty( $contents['content'] ) ) : ?>
				<div class="section-subtitle wow fadeInUp" data-wow-delay="150ms"><?php echo ''.$contents['content']; ?></div>
			<?php endif; ?>
		</header>

		<div class="image-block mb50 wow fadeInUp">
			<a href="<?php echo esc_url( $contents['video_url'] ); ?>" class="popup-youtube">
				<?php if ( ! empty( $contents['image'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>">
				<?php else : ?>
					<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/video-preview-large.jpg" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
				<?php endif; ?>
			</a>
		</div>
		
		<div class="features left_icon">
			<div class="feature-list  columns-3">
				<?php $delay = 150; ?>
				<?php foreach( $contents['items'] as $feature ) : ?>
					
					<div class="feature wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
						<div class="feature_content">
							<i class="fa <?php echo esc_attr( $feature['media'] ); ?>"></i>
							<?php if ( ! empty( $feature['title'] ) ) : ?>
								<h3><?php echo esc_attr( $feature['title'] ); ?></h3>
							<?php endif; ?>
							<?php if ( ! empty( $feature['content'] ) ) : ?>
								<p><?php echo ''.$feature['content']; ?></p>
							<?php endif; ?>	
						</div>
					</div>
					
				<?php $delay +=175; endforeach; ?>
				
			</div>
		</div>
	</div>
</div>