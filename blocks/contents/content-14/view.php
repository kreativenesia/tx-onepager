<?php
	$container_size	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height = ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg = ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

?>  

<!-- Multipress Content 14 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-14 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="col-md-8 col-md-offset-2 align-<?php echo ''.$settings['text_alignment']; ?>">
			
			<?php if ( ! empty( $contents['image'] ) ) : ?>
				<img src="<?php echo esc_url( $contents['image'] ); ?>" alt="<?php esc_html_e( 'Section Image', 'multipress' ); ?>" class="mb30 wow fadeInUp">
			<?php else : ?>
				<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/twitter-badge.png" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>" class="mb30 wow fadeInUp">
			<?php endif; ?>
			
			<?php if ( ! empty( $contents['title'] ) ) : ?>
				<h2 class="h2 wow fadeInUp" data-wow-delat="150ms"><?php echo esc_attr( $contents['title'] ); ?></h2>
			<?php endif; ?>
			<?php if ( ! empty( $contents['content'] ) ) : ?>
				<div class="text14 text-gray wow fadeInUp" data-wow-delat="350ms"><?php echo ''.$contents['content']; ?></div>
			<?php endif; ?>
		</div>
	</div>
</div>