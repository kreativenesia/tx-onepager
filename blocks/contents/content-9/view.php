<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$content_color	 	= ! empty( $contents['content_color'] ) ? $contents['content_color'] : '';
	$content_size	 	= ! empty( $contents['content_font_size'] ) ? $contents['content_font_size'] : '';
	$content_styles	 	= ( ! empty( $contents['content_color'] ) || ! empty( $contents['content_font_size'] ) )  ? "style='color:{$content_color};font-size:{$content_size}'": '';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress Content 9 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-9 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="cta-section align-<?php echo ''.$settings['text_alignment']; ?>" style="background-color:<?php echo ''.$contents['bg_content']; ?>">
			<?php if ( ! empty( $contents['content'] ) ) : ?>
				<span class="h3" <?php echo ''.$content_styles; ?>><?php echo ''.$contents['content']; ?></span> 
			<?php endif; ?>
			<?php if ( ! empty( $contents['button_text'] ) ) : ?>
				<?php $target1 ='';
					if ( ! empty( $contents['button_target'])){
						$target1 = 'target="_blank"';
					}				
				?>
				<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button big" <?php echo '' . $target1 ?>><?php echo ''.$contents['button_text']; ?></a>
			<?php endif; ?>
		</div>
	</div>
</div>
