<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>
 
<!-- Multipress Content 20 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-content multipress-content-type-20 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_video'] ) && ! empty( $styles['bg_video'] ) ) : ?>
		<div class="background-video" data-video="<?php echo esc_url( $styles['bg_video'] ); ?>"></div>
	<?php endif; ?>
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row pt30 pb30">
			<div class="col-md-8 col-md-offset-2 align-center">
				<?php if ( ! empty( $contents['title'] ) ) : ?>
					<h2 class="text-white h2 wow fadeInUp"><?php echo esc_attr( $contents['title'] ); ?></h2>
				<?php endif; ?>
				
				<?php if ( ! empty( $contents['button_text'] ) ) : ?>
					<?php $target1 ='';
						if ( ! empty( $contents['button_target'])){
							$target1 = 'target="_blank"';
						}				
					?>
					<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button big mb50 wow fadeInUp" <?php echo '' . $target1 ?>><?php echo ''.$contents['button_text']; ?></a>
				<?php endif; ?>
				
				<?php if ( ! empty( $contents['content'] ) ) : ?>
					<div class="text14 text-gray wow fadeInUp">
						<?php echo ''.$contents['content']; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>