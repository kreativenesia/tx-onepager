<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	
	$args = array(
		'post_type'				=> 'post',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 3,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$blogs = new WP_Query( $args );
 ?>

<!-- Blog Style 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-blog--style-2 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section_title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle wow fadeInUp'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="row">

			<?php if ( $blogs->have_posts() ) : ?>
				<?php 
					$delay = 150;
					while ( $blogs->have_posts() ) : $blogs->the_post(); ?>
					
					<div class="col-md-4">
						<article class="blog wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">

							<figure class="blog__image">
								<a href="<?php the_permalink(); ?>">
									<?php if ( has_post_thumbnail() ) : ?>
										<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 400, 200 ) ?>" alt="<?php esc_html_e( 'Post thumbnail', 'multipress' ); ?>">	
									<?php else : ?>
										<img src="http://unsplash.it/400/200" alt="<?php esc_html_e( 'Post thumbnail', 'multipress' ); ?>">		
									<?php endif; ?>
								</a>
							</figure>

							<small class="blog__date"><?php echo _multipress_published_date( array( 'before' => '', 'after' => ' ' ) ); ?></small>
							<h3 class="blog__title"><?php the_title(); ?></h3>
							<div class="blog__excerpt">
								<?php echo wp_trim_words( get_the_content(), 25, '' ); ?>
							</div>
							<a href="<?php the_permalink(); ?>" class="button more"><?php esc_html_e( 'Read more', 'multipress' ); ?></a>
						</article>
					</div>

				<?php $delay+=175; endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php else: ?>
				
				<?php 
					$counter = 1; 
					$delay   = 150;
				?>
				<?php while ( 4 > $counter ) : ?>
				    
					<div class="col-md-4">
						<article class="blog wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
							<figure class="blog__image">
								<a href="#"><img src="http://unsplash.it/400/200" alt=""></a>
							</figure>
							<small class="blog__date">January 12, 2016</small>
							<h3 class="blog__title">Lorem ipsum dolor sit amet, consectetur adipisicing.</h3>
							<div class="blog__excerpt">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eius sit pariatur accusamus ullam tenetur. Veniam cum corrupti velit sint natus ut dolorum neque facere, ab iure fugit! Suscipit, nam.</p>
							</div>
							<a href="#" class="button more">Read more</a>
						</article>
					</div>

				<?php $delay+=175; $counter++; endwhile; ?>

			<?php endif; ?>
			
		</div>
	</div>
</div>