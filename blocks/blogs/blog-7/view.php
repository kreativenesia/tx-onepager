<?php
	$container_size	 			= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	
	$args = array(
		'post_type'				=> 'post',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 4,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$blogs = new WP_Query( $args );
 ?>

<!-- Blog Style 7 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-blog--style-7 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) ) : ?>
			<h2 class='section_title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
		<?php endif; ?>
		
		<?php $counter = 1; ?>
		<?php if ( $blogs->have_posts() ) : ?>
			<?php while ( $blogs->have_posts() ) : $blogs->the_post(); ?>
				
				<?php if ( 1 == $counter ) : ?>
					
					<div class="blog blog--featured">
						<small class="blog__date"><?php echo _multipress_published_date( array( 'before' => '', 'after' => ' ' ) ); ?></small>
						<h2 class="blog__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
						
						<figure class="blog__image">
							<a href="<?php the_permalink(); ?>">
								<?php if ( has_post_thumbnail() ) : ?>
									<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 1600, 600 ) ?>" alt="<?php esc_html_e( 'Post thumbnail', 'multipress' ); ?>">
								<?php else : ?>
									<img src="http://unsplash.it/1600/600" alt="<?php esc_html_e( 'Post thumbnail', 'multipress' ); ?>">
								<?php endif; ?>
							</a>
						</figure>

						<div class="blog__excerpt">
							<?php echo wp_trim_words( get_the_content(), 25, '' ); ?>
						</div>
					</div>

				<?php else : ?>

					<?php ob_start(); ?>
						
						<div class="col-md-4">
							<article class="blog">
								<small class="blog__date"><?php echo _multipress_published_date( array( 'before' => '', 'after' => ' ' ) ); ?></small>
								<h3 class="blog__title"><?php the_title(); ?></h3>
								<div class="blog__excerpt">
									<?php echo wp_trim_words( get_the_content(), 25 ); ?>
								</div>
								<a href="<?php the_permalink(); ?>" class="button more"><?php esc_html_e( 'Read more', 'multipress' ); ?></a>
							</article>
						</div>

					<?php $blog_contents[] = ob_get_contents(); ?>
					<?php ob_end_clean(); ?>

				<?php endif; ?>
			
			<?php $counter++; endwhile; ?>
			<?php wp_reset_postdata(); ?>

		<?php endif; ?>

		<div class="row">
			<?php if ( ! empty( $blog_contents ) ) : ?>
				<?php foreach ( $blog_contents as $content ) : ?>
					<?php echo ''.$content; ?>			
				<?php endforeach ?>			
			<?php endif; ?>	
		</div>

	</div>
</div>