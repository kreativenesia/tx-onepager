<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

	$args = array(
		'post_type'				=> 'post',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 5,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$blogs = new WP_Query( $args );
 ?>

<!-- Blog Style 6 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-blog--style-6 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row equal-height">

			<?php $counter = 1; ?>
			<?php if ( $blogs->have_posts() ) : ?>
				<?php while ( $blogs->have_posts() ) : $blogs->the_post(); ?>
					
					<?php if ( 1 == $counter ) : ?>
						
						<div class="col-md-6">
							<div class="blog blog--featured">
								<small class="blog__date"><?php echo _multipress_published_date( array( 'before' => '', 'after' => ' ' ) ); ?></small>
								<h2 class="blog__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="blog__excerpt">
									<?php echo wp_trim_words( get_the_content(), 25, '' ); ?>
								</div>
								<a href="<?php the_permalink(); ?>" class="button large"><?php esc_html_e( 'Read More', 'multipress' ); ?></a>
							</div>
						</div>

					<?php else : ?>

						<?php ob_start(); ?>

							<div class="col-sm-6">
								<article class="blog">
									<small class="blog__date"><?php echo _multipress_published_date( array( 'before' => '', 'after' => ' ' ) ); ?></small>
									<h3 class="blog__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<div class="blog__excerpt">
										<?php echo wp_trim_words( get_the_content(), 25 ); ?>
									</div>
								</article>
							</div>

						<?php $blog_contents[] = ob_get_contents(); ?>
						<?php ob_end_clean(); ?>

					<?php endif; ?>
				
				<?php $counter++; endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php endif; ?>

			<div class="col-md-6 dir-column">
				<?php if ( ! empty( $blog_contents ) ) : ?>
					<?php $the_cunks = array_chunk( $blog_contents, 2 ); ?>
					<?php foreach ( $the_cunks as $kacunks ) : ?>
						<div class="row equal-height">
							<?php foreach ( $kacunks as $content ) : ?>
								<?php echo ''.$content; ?>			
							<?php endforeach; ?>
						</div>			
					<?php endforeach; ?>			
				<?php endif; ?>	
			</div>
		</div>
	</div>
</div>