<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

	$args = array(
		'post_type'				=> 'post',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 6,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$custom_query 	= get_posts( $args ); 
	$chunked_posts 	= array_chunk( $custom_query , 3 ); 
 ?>

<!-- Blog Style 4 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-blog multipress-blog--style-4 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle wow fadeInUp'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
				<a href="<?php echo esc_url( $contents['post_link']); ?>" class="button"><?php esc_html_e( 'All Posts', 'multipress' ); ?></a>
			</header>
		<?php endif; ?>

		<?php foreach ( $chunked_posts as $custom_post ) : ?>
			
			<div class="row">
				<?php $delay = 150; ?>		
				<?php foreach( $custom_post as $post ) : setup_postdata( $post ); ?>
					
					<div class="col-md-4">
						<article class="blog wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
							<small class="blog__date"><?php echo get_the_date( 'F d, Y', $post->ID ); ?></small>
							<h3 class="blog__title"><?php echo get_the_title( $post->ID ); ?></h3>
							<?php if ( ! empty( $post->post_content ) ) : ?>
								<div class="blog__excerpt">
									<?php echo wp_trim_words( $post->post_content, 25, '' ); ?>
								</div>
							<?php endif; ?>
							<a href="<?php echo get_permalink( $post->ID ); ?>" class="button more"><?php esc_html_e( 'Read more', 'multipress' ); ?></a>
						</article>
					</div>

				<?php 
				$delay +=170;
				endforeach; ?>

			</div>

		<?php endforeach; ?>

	</div>
</div>