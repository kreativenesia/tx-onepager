<?php

return array(
	'slug'    	=> 'multipress-shop-banner-1',
	'name' 		=> esc_html__( 'Multipress Shop Banner 1', 'multipress' ),
	'groups'    => array( 'shop' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> ''
			),	
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=>'items',
				'type'		=>'repeater',
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Unique Chairs'
						),
						array(
							'name' 		=> 'description', 
							'type' 		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet.'
						),
						array(
							'name' 		=> 'text_prefix', 
							'type' 		=> 'text', 
							'value' 	=> 'Start from'
						),
						array(
							'name' 		=> 'price', 
							'type' 		=> 'text', 
							'value' 	=> '$10'
						),
						array(
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'multipress' ),
							'type' 		=> 'image',
							'value'		=> ONEPAGER_URL . '/assets/images/onepager/product-1.jpg',
						),
						array(
							'name' 		=> 'button_text', 
							'type' 		=> 'text', 
							'value' 	=> 'Shop Now'
						),
						array(
							'name' 		=> 'button_link', 
							'type' 		=> 'text', 
							'value' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Au de Perfume'
						),
						array(
							'name' 		=> 'description', 
							'type' 		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet.'
						),
						array(
							'name' 		=> 'text_prefix', 
							'type' 		=> 'text', 
							'value' 	=> 'Start from'
						),
						array(
							'name' 		=> 'price', 
							'type' 		=> 'text', 
							'value' 	=> '$10'
						),
						array(
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'multipress' ),
							'type' 		=> 'image',
							'value'		=> ONEPAGER_URL . '/assets/images/onepager/product-2.jpg',
						),
						array(
							'name' 		=> 'button_text', 
							'type' 		=> 'text', 
							'value' 	=> 'Shop Now'
						),
						array(
							'name' 		=> 'button_link', 
							'type' 		=> 'text', 
							'value' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'Bright Lamp'
						),
						array(
							'name' 		=> 'description', 
							'type' 		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet.'
						),
						array(
							'name' 		=> 'text_prefix', 
							'type' 		=> 'text', 
							'value' 	=> 'Start from'
						),
						array(
							'name' 		=> 'price', 
							'type' 		=> 'text', 
							'value' 	=> '$10'
						),
						array(
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'multipress' ),
							'type' 		=> 'image',
							'value'		=> ONEPAGER_URL . '/assets/images/onepager/product-3.jpg',
						),
						array(
							'name' 		=> 'button_text', 
							'type' 		=> 'text', 
							'value' 	=> 'Shop Now'
						),
						array(
							'name' 		=> 'button_link', 
							'type' 		=> 'text', 
							'value' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
					),
					
				)
			)
	),

	'settings' 	=> array(
		array(
			'name' 		=> 'section_alignment',
			'label' 	=> esc_html__( 'Section Heading Alignment', 'multipress' ),
			'type' 		=> 'select',
			'center' 	=> 'center',
			'options' 	=> array(
				'left' 		=> 'Left',
				'center' 	=> 'Center',
				'right' 	=> 'Right',
			)
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),

		array(
			'name' 		=> 'overlap_button', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Overlap Button?', 'multipress' ), 
			'value' 	=> true, 
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#e0e6e9'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'section_subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'item_title_color',
				'label' 	=> esc_html__( 'Item Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_title_font_size',
				'label' 	=> esc_html__( 'Item Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'item_content_color',
				'label' 	=> esc_html__( 'Item Content Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_content_font_size',
				'label' 	=> esc_html__( 'Item Content Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'item_before_price_color',
				'label' 	=> esc_html__( 'Item Before Price Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_before_price_font_size',
				'label' 	=> esc_html__( 'Item Before Price Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'item_price_color',
				'label' 	=> esc_html__( 'Item Price Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_price_font_size',
				'label' 	=> esc_html__( 'Item Price Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
	),

);
