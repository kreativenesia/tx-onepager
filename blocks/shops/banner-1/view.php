<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Multipress ecommerce 1 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-ecommerce multipress-ecommerce-1 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="row">
			<?php $delay = 150; ?>
			<?php foreach( $contents['items'] as $feature ) : ?>
			
				<div class="col-md-4">
					<div class="product-card wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
						<?php if ( ! empty( $feature['title'] ) ) : ?>
							<h2 class="product-card__title"><?php echo esc_attr( $feature['title'] ); ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $feature['description'] ) ) : ?>
							<small class="product-card__desc"><?php echo ''.$feature['description']; ?></small>
						<?php endif; ?>
						<?php if ( ! empty( $feature['text_prefix'] ) ) : ?>
							<em class="pricing-start"><?php echo ''.$feature['text_prefix']; ?> <strong><?php echo ''.$feature['price']; ?></strong></em>
						<?php endif; ?>

						<figure class="product-card__image">
							<?php if ( ! empty( $feature['image'] ) ) : ?>
								<img src="<?php echo esc_url( $feature['image'] ); ?>" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
							<?php else : ?>
								<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/product-1.jpg" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
							<?php endif; ?>
						</figure>

						<?php if ( ! empty( $feature['button_text'] ) ) : ?>
						<?php  $overlap = (!empty( $settings['overlap_button'] ) && $settings['overlap_button'] === true ) ? "overlap": ''; ?>
						<div class="product-card__action <?php echo ''.$overlap; ?>">
							<?php $target1 ='';
								if ( ! empty( $feature['button_target'])){
									$target1 = 'target="_blank"';
								}				
							?>
							<a href="<?php echo esc_url( $feature['button_link'] ); ?>" class="button product-card__button" <?php echo '' . $target1 ?>><?php echo esc_attr( $feature['button_text'] ); ?></a>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php $delay +=175; ?>
			<?php endforeach; ?>

		</div>
	</div>
</div>