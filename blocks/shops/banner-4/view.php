<?php
	$container_size	 = ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
?>

<!-- Multipress ecommerce 4 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-ecommerce multipress-ecommerce-4 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?> align-center">
		<?php if ( ! empty( $contents['title'] ) ) : ?>
			<h2><?php echo ''.$contents['title']; ?></h2>
		<?php endif; ?>
		<?php if ( ! empty( $contents['button_text'] ) ) : ?>
			<?php $target1 ='';
				if ( ! empty( $contents['button_target'])){
					$target1 = 'target="_blank"';
				}				
			?>
			<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button big" <?php echo '' . $target1 ?>><?php echo esc_attr( $contents['button_text'] ); ?></a>
		<?php endif; ?>
	</div>
</div>