<?php

return array(
	'slug'    	=> 'multipress-shop-banner-2',
	'name' 		=> esc_html__( 'Multipress Shop Banner 2', 'multipress' ),
	'groups'    => array( 'shop' ),
	'contents' 	=> array(
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=>'items',
				'type'		=>'repeater',
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'THE BEST PERFUME IN THE WORLD'
						),
						array(
							'name' 		=> 'description', 
							'type' 		=> 'editor', 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur'
						),
						array(
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'multipress' ),
							'type' 		=> 'image',
						),
						array(
							'name' 		=> 'button_text', 
							'type' 		=> 'text', 
							'value' 	=> 'Shop Now'
						),
						array(
							'name' 		=> 'button_link', 
							'type' 		=> 'text', 
							'value' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
						array(
							'name' 		=> 'content_bg_color',
							'label' 	=> esc_html__( 'Content Background Color', 'multipress' ),
							'type' 		=> 'colorpicker',
							'value' 	=> '#f4f9fb'
						),
					),
					array(
						array(
							'name' 		=> 'title', 
							'value' 	=> 'New Smart Watch For Your Life'
						),
						array(
							'name' 		=> 'description', 
							'type' 		=> 'editor', 
							'value' 	=> ''
						),
						array(
							'name' 		=> 'image',
							'label' 	=> esc_html__( 'Image', 'multipress' ),
							'type' 		=> 'image',
						),
						array(
							'name' 		=> 'button_text', 
							'type' 		=> 'text', 
							'value' 	=> 'Shop Now'
						),
						array(
							'name' 		=> 'button_link', 
							'type' 		=> 'text', 
							'value' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
						array(
							'name' 		=> 'content_bg_color',
							'label' 	=> esc_html__( 'Content Background Color', 'multipress' ),
							'type' 		=> 'colorpicker',
							'value' 	=> '#fdf5f2'
						),
					),
				)
			)
	),

	'settings' 	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#f4f9fb'
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'item_title_color',
				'label' 	=> esc_html__( 'Item Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_title_font_size',
				'label' 	=> esc_html__( 'Item Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'item_content_color',
				'label' 	=> esc_html__( 'Item Content Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'item_content_font_size',
				'label' 	=> esc_html__( 'Item Content Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			
	),

);
