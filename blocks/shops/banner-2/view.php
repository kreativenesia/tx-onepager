<!-- Multipress ecommerce 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-ecommerce multipress-ecommerce-2 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php $counter = 1; ?>
	<?php foreach( $contents['items'] as $feature ) : ?>
		
		<?php if ( 3 > $counter ) : ?>
			
			<?php $banner_class = ( 2 == $counter ) ? '2 mp-banner-two-third' : '1 mp-banner-one-third';  ?>
			<?php $image_class 	= ( 1 == $counter ) ? 'mp-banner__image-right' : '';  ?>
			<div class="mp-banner mp-banner-type-<?php echo esc_attr( $banner_class ); ?>" style="background-color:<?php echo ''.$feature['content_bg_color']; ?>">
				<div class="mp-banner__image <?php echo esc_attr( $image_class ); ?>">
					<?php if ( ! empty( $feature['image'] ) ) : ?>
						<img src="<?php echo esc_url( $feature['image'] ); ?>" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
					<?php else : ?>
						<?php if ( 1 == $counter ) : ?>
							<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/product-full-1.jpg" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
						<?php else : ?>
							<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/product-full-2.jpg" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<div class="mp-banner__content">
					<?php if ( ! empty( $feature['title'] ) ) : ?>
						<h2><?php echo esc_attr( $feature['title'] ); ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $feature['description'] ) ) : ?>
						<p><?php echo ''.$feature['description']; ?></p>
					<?php endif; ?>
					<?php if ( ! empty( $feature['button_text'] ) ) : ?>
						<?php $target1 ='';
							if ( ! empty( $feature['button_target'])){
								$target1 = 'target="_blank"';
							}				
						?>
						<a href="<?php echo esc_url( $feature['button_link'] ); ?>" class="button" <?php echo '' . $target1 ?>><?php echo esc_attr( $feature['button_text'] ); ?></a>
					<?php endif; ?>
				</div>
			</div>

		<?php endif; ?>
		
	<?php $counter++; endforeach; ?>
</div>