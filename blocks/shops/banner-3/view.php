
<!-- Multipress ecommerce 3 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-ecommerce multipress-ecommerce-3 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php $counter = 1; ?>
	<?php foreach( $contents['items'] as $feature ) : ?>
		
		<?php if ( 3 > $counter ) : ?>
			
			<?php $banner_class = ( 1 == $counter ) ? '2 mp-banner-two-third' : '1 mp-banner-one-third';  ?>
			<?php $image_class 	= ( 1 == $counter ) ? 'mp-banner__image-right' : '';  ?>
			<?php $title_color 	= ( ! empty( $feature['content_title_color'] ) ) ? "style=color:{$feature['content_title_color']}!important" : '';  ?>
			<?php $text_color 	= ( ! empty( $feature['content_text_color'] ) ) ? "style=color:{$feature['content_text_color']}" : '';  ?>

			<div class="mp-banner mp-banner-type-<?php echo esc_attr( $banner_class ); ?>" style="background-color:<?php echo ''.$feature['content_bg_color']; ?>">
				<div class="mp-banner__image <?php echo esc_attr( $image_class ); ?>">
					<?php if ( ! empty( $feature['image'] ) ) : ?>
						<img src="<?php echo esc_url( $feature['image'] ); ?>" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
					<?php else : ?>
						<?php if ( 1 == $counter ) : ?>
							<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/product-full-3.png" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
						<?php else : ?>
							<img src="<?php echo ONEPAGER_URL; ?>/assets/images/onepager/product-full-4.jpg" alt="<?php esc_html_e( 'Image', 'multipress' ); ?>">
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<div class="mp-banner__content">
					<?php if ( ! empty( $feature['title'] ) ) : ?>
						<h2 <?php echo ''.$title_color; ?>><?php echo esc_attr( $feature['title'] ); ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $feature['description'] ) ) : ?>
						<p <?php echo ''.$text_color; ?>><?php echo ''.$feature['description']; ?></p>
					<?php endif; ?>
					<?php if ( ! empty( $feature['button_text'] ) ) : ?>
						<?php $target1 ='';
							if ( ! empty( $feature['button_target'])){
								$target1 = 'target="_blank"';
							}				
						?>
						<a href="<?php echo esc_url( $feature['button_link'] ); ?>" class="button" <?php echo '' . $target1 ?>><?php echo esc_attr( $feature['button_text'] ); ?></a>
					<?php endif; ?>
				</div>
			</div>

		<?php endif; ?>
		
	<?php $counter++; endforeach; ?>

</div>