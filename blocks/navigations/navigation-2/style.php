<?php
$backgroundColor    = ! empty( $styles["background_color"] ) ? $styles["background_color"] : "";
$contentColor       = ! empty( $styles["content_color"] ) ? $styles["content_color"] : "";
$topBackgroundColor = ! empty( $styles["top_background_color"] ) ? $styles["top_background_color"] : "";
$topContentColor    = ! empty( $styles["top_content_color"] ) ? $styles["top_content_color"] : "";
$separatorColor     = ! empty( $styles["separator_color"] ) ? $styles["separator_color"] : "";
?>

<?php if ( ! empty( $backgroundColor ) ):  ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .bottom_header{
		background-color: <?php echo ''.$backgroundColor; ?>;
	}
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header{
		background-color: <?php echo ''.$backgroundColor; ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $contentColor ) ):  ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .mobile_nav_trigger,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .logo #site-title span,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .logo #site-description span, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .primary_menu .menu > .menu-item > a,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .search_button .search_button_trigger, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .cart_button a, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .secondary_menu a,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .mobile_nav_trigger,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .logo #site-title span,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .logo #site-description span,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .primary_menu .menu > .menu-item > a,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .primary_menu .secondary_menu > .menu-item > a,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .search_button .search_button_trigger, 
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .cart_button a
	{
		color: <?php echo ''.$contentColor; ?>
	}
<?php endif; ?>

<?php if( ! empty( $topBackgroundColor ) ) : ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .top_header{
		background-color: <?php echo ''.$topBackgroundColor; ?>
	}
<?php endif; ?>

<?php if( ! empty( $topContentColor ) ) : ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .top_header,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .top_header a,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .top_header .left-side .fa{
		color: <?php echo ''.$topContentColor; ?>
	}
<?php endif; ?>

<?php if( ! empty( $separatorColor ) ) : ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header.style-2 .bottom-section{
		border-color: <?php echo ''.$separatorColor; ?>
	}
<?php endif; ?>