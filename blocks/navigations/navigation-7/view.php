<!-- Cart Menu -->
<div class="sidemenu_overlay"></div>
<div class="side_menu cart-menu off-pushright">
	<button class="close_side_menu"><i class="ti-arrow-right"></i></button>
	<?php if ( class_exists( 'WooCommerce' ) ) : ?>

		<?php if ( class_exists( 'WC_Widget_Cart' ) ) : ?>
			<?php the_widget( 'WC_Widget_Cart' ); ?>
		<?php else : ?>
			<?php esc_html_e( 'Widget Cart and Widget My Account do not available', 'multipress' ); ?>
		<?php endif; ?>

	<?php endif; ?>
	<?php if ( is_active_sidebar( 'off-canvas' ) ) { ?>
		<?php dynamic_sidebar( 'off-canvas' ); ?>
	<?php } ?>

</div>

<!-- Begin #masthead.site-header-->
<?php $container_size	 		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container'; ?>
<?php $sticky_header_class 		= ( true == $settings['sticky_header']  ) ? 'sticky_header' : ''; ?>
<?php $background_color 		= ! empty( $styles['background_color'] ) ? "style='background-color:{$styles['background_color']}'" : ''; ?>

<?php
	$slider_duration			= ! empty( $settings['slider_duration'] ) ? $settings['slider_duration'] : '5000';
	$slider_bg_color			= ! empty( $styles['background_overlay'] ) ? "background-color:{$styles['background_overlay']};" : '';

	$args = array(
		'post_type'				=> 'multipress-slider',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 1,
	);
	if ( ! empty( $contents['slide_id'] ) ) {
		$args['p'] 	= $contents['slide_id'];
	}
	$slides = new WP_Query( $args );
 ?>

<div class="nav-with-hero">
	<div class="multipress-section multipress-slider multipress-slider-1 multipress-block-<?php echo esc_attr( $id ); ?>">
		<div class="swiper-container" data-duration="<?php echo esc_attr( $slider_duration ); ?>">
			<!-- Additional required wrapper -->
			<div class="swiper-wrapper">

				<?php if ( $slides->have_posts() ) : ?>
					<?php while ( $slides->have_posts() ) : $slides->the_post(); ?>
						
						<?php $slider_data = _multipress_get_meta( '_sliders_details' ); ?>

						<?php if ( ! empty( $slider_data['slides'] ) ) : ?>
					
							<?php foreach ( $slider_data['slides'] as $slide ) : ?>
								
								<?php $slider_align = ! empty( $slide['slider_align'] ) ? $slide['slider_align'] : 'left'; ?>
								<div class="swiper-slide">
									<?php if ( ! empty( $slide['slider_image'] ) ) : ?>
										<?php $bg_image = wp_get_attachment_image_src( $slide['slider_image'], 'full' ); ?>
										<div class="slide-bg" style="<?php echo ''. $slider_bg_color; ?> background-image:url(<?php echo esc_url( $bg_image[0] ); ?>)"></div>
									<?php else : ?>
										<div class="slide-bg" style="<?php echo ''. $slider_bg_color; ?> background-image:url(<?php echo ONEPAGER_URL; ?>/assets/images/onepager/slider-1.jpg)"></div>
									<?php endif; ?>
									<div class="container">
										<div class="slide-content align-<?php echo esc_attr( $slider_align ); ?>">
											<?php if ( ! empty( $slide['slider_title'] ) ) : ?>
												<h2 class="slide-title"><?php echo esc_attr( $slide['slider_title'] ); ?></h2>
											<?php endif; ?>
											<?php if ( ! empty( $slide['slider_content'] ) ) : ?>
												<div class="slide-desc"><?php echo ''.$slide['slider_content']; ?></div>
											<?php endif; ?>
											<?php if ( ! empty( $slide['button_text'] ) ) : ?>
												<?php $slider_link = ! empty( $slide['button_link'] ) ? $slide['button_link'] : ''; ?>
												<a href="<?php echo esc_url( $slider_link ); ?>" class="slide-button"><?php echo esc_attr( $slide['button_text'] ); ?></a>
											<?php endif; ?>
										</div>
									</div>
								</div>

							<?php endforeach; ?>
						<?php endif; ?>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>

				<?php else: ?>
					
					<?php $counter = 1; ?>
					<?php while ( 8 > $counter ) : ?>
					    
						<div class="swiper-slide">
							<div class="slide-bg" style="background-image:url(<?php echo ONEPAGER_URL; ?>/assets/images/onepager/slider-1.jpg)"></div>
							<div class="container">
								<div class="slide-content align-center">
									<h2 class="slide-title">My Adventure</h2>
									<div class="slide-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, consequatur.</div>
									<a href="#" class="slide-button">Buy Now</a>
								</div>
							</div>
						</div>

					<?php $counter++; endwhile; ?>
				<?php endif; ?>

			</div>
			<!-- If we need pagination -->
			<div class="swiper-pagination"></div>
			
			<!-- If we need navigation buttons -->
			<!-- <div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div> -->
			
			<!-- If we need scrollbar -->
			<!-- <div class="swiper-scrollbar"></div> -->
		</div>
	</div>
	
	<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
		<script>
			// .multipress-slider-1
			var mSlider1 = new Swiper ('.multipress-slider-1 .swiper-container', {
			    loop: true,
			    pagination: '.swiper-pagination',
			    autoplay: 5000
		    });
		</script>
	<?php } ?>
	
	<header id="multipress-block-<?php echo esc_attr( $id ); ?>" class="page_header <?php echo esc_attr( $sticky_header_class ); ?>" <?php echo ''.$background_color; ?> data-style="style-7">
		<div class="bottom_header">
			<div class="<?php echo esc_attr( $container_size ); ?>">
				<a href="#" class="mobile_nav_trigger pull-left"><i class="fa fa-bars"></i></a>

				<div class="logo">
					<div class="branding">
						<?php 
							$custom_logo 	= $contents['site_logo'];
							$logotag  		= ( is_home() || is_front_page() ) ? 'h1':'div';
							$title 			= get_bloginfo( 'name' );
						 ?>
						<?php if ( ! empty( $custom_logo ) ) : ?>
							<<?php echo esc_attr( $logotag ); ?> id="site-logo">
								<a href="<?php echo esc_url( get_home_url() ); ?>" title="<?php echo get_bloginfo( 'name' ) ?>" role="home">
									<img src="<?php echo esc_url( $contents['site_logo'] ); ?>" alt="<?php esc_html_e( 'Site Logo', 'multipress' ) ?>">
								</a>
							</<?php echo esc_attr( $logotag ); ?>>
						<?php else : ?>
							<?php printf( '<%1$s id="site-title"><a href="%2$s" title="%3$s" rel="home"><span>%4$s</span></a></%1$s>', tag_escape( $logotag ), home_url(), esc_attr( $title ), $title ); ?>
						 <?php endif; ?>

						 <?php if ( ! empty( $contents['site_description'] ) ) : ?>
							<?php printf( '<%1$s id="site-description"><span>%2$s</span></%1$s>', $logotag, $contents['site_description'] ); ?>
						 <?php endif; ?>
					</div>
				</div>
				
				<?php if ( ! empty( $contents['site_menu'] ) ) :
					$menu_args = array(
						'menu'  			=> $contents['site_menu'],
						'container'       	=> 'nav',
						'container_class' 	=> 'primary_menu pull-left',
						'fallback_cb' 		=> '_multipress_get_dummy_menus',
					);
					if ( class_exists( 'Multipress_Megamenus_Walker' ) ) {
						$menu_args['walker'] = new Multipress_Megamenus_Walker;
					}					
					wp_nav_menu( $menu_args );
				endif; ?>
		
				<div class="right_section">
					<ul> 
						<?php if ( true == $settings['show_search'] ) : ?>
							<li class="search_button">
								<a href="" class="search_button_trigger">
									<i class="fa fa-search"></i>
								</a>
								<?php get_template_part( 'searchform', 'custom' ); ?>
							</li>
						<?php endif; ?>

						<?php if ( true == $settings['show_cart'] ) : ?>
							<li class="cart_button">
								<a href="#">
									<i class="fa fa-shopping-cart"></i>
									<?php if ( class_exists( 'WooCommerce' ) ) : ?>
										<span class="count"><?php echo WC()->cart->cart_contents_count; ?></span> 
									<?php endif; ?>
								</a>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</header>

	<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
		<script>
			if(typeof function_name == 'multipress_init_sticky_nav'){
			  multipress_init_sticky_nav()
			}
			if(typeof function_name == 'multipress_init_navigation'){
			  multipress_init_navigation()
			}
			if(typeof function_name == 'multipress_init_header_ui'){
			  multipress_init_header_ui()
			}
		</script>
	<?php } ?>
</div>


