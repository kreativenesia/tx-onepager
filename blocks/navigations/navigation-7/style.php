.multipress-block-<?php echo esc_attr( $id ); ?> .slide-content h2 {
	<?php _multipress_print_builder_css( $styles['slide_title_color'],		'color:'. $styles['slide_title_color'] . '!important' ); ?>
	<?php _multipress_print_builder_css( $styles['slide_title_font_size'],		'font-size:'. $styles['slide_title_font_size'] . 'px !important' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .slide-content .slide-desc {
	<?php _multipress_print_builder_css( $styles['slide_desc_color'],		'color:'. $styles['slide_desc_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['slide_desc_font_size'],	'font-size:'. $styles['slide_desc_font_size'] . 'px !important' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .slide-content .slide-button {
	<?php _multipress_print_builder_css( $styles['button_bg_color'],		'background-color:'. $styles['button_bg_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['button_text_color'],		'color:'. $styles['button_text_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['slide_button_font_size'],		'font-size:'. $styles['slide_button_font_size'] . 'px !important' ); ?>
}

<?php if ( ! empty ( $styles["background_color"] ) ): ?>
	
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .bottom_header{
		background-color: <?php echo ''.$styles["background_color"]; ?>;
	}
	
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header{
		background-color: <?php echo ''.$styles["background_color"]; ?>;
	}
<?php endif; ?>

<?php if ( ! empty ( $styles["content_color"] ) ): ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .mobile_nav_trigger,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .logo #site-title span,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .logo #site-description span, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .primary_menu .menu > .menu-item > a,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .search_button .search_button_trigger, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header:not(.transparent_menu) .cart_button a, 
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .mobile_nav_trigger,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .logo #site-title span,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .logo #site-description span,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .primary_menu .menu > .menu-item > a,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .search_button .search_button_trigger, 
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .cart_button a
	{
		color: <?php echo ''.$styles["content_color"]; ?>
	}
<?php endif; ?>