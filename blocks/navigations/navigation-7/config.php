<?php

/*
	
	Content
	- Logo : Image Picker
	- Site Description : Text (Kalo gak diisi gak usah ada tagnya)
	- Menu : Menu Picker

	Settings
	- Show Search: checkbox
	- Show Cart : checkbox (Kalo gak diceklis, markup buat cart Menu jangan dirender)
	- Stiky Header: Checkbox (Nambah class sticky-header di .page_header)

	Style
	- Background-color : color picker
	- Transparent Menu : Checkbox
 */

return array(
	'slug'    	=> 'multipress-nav-7',
	'name'    	=> esc_html__( 'Multipress Navigation 7', 'multipress' ),
	'groups'    => array( 'navbars' ),
	'contents' 	=> array(
		array(
			'name' 			=> 'slide_id', 
			'type' 			=> 'select',
			'label' 		=> esc_html__( 'Select a Slider', 'multipress' ),
			'options' 		=> _multipress_get_post_types( 'multipress-slider' ),
		),
		array(
			'name' 			=> 'slider_duration', 
			'type' 			=> 'text',
			'label' 		=> esc_html__( 'Slider Duration', 'multipress' ),
			'value'			=> '5000'
		),
		array(
			'name' 		=> 'site_logo', 
			'type' 		=> 'image',
			'label' 	=> esc_html__( 'Custom Logo Image', 'multipress' ),
		),
		array(
			'name' 		=> 'site_description', 
			'type' 		=> 'text',
			'label' 	=> esc_html__( 'Site Description', 'multipress' ),
			'value' 	=> 'Site Description'
		),
		array(
			'name' 		=> 'site_menu', 
			'type' 		=> 'menu',
			'label' 	=> esc_html__( 'Site Menu', 'multipress' ),
		),
	),
	'settings' 	=> array(
		array(
			'name' 		=> 'show_search', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Show Search Box', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'show_cart', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Show Cart', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'sticky_header', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Make The Header Sticky?', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),
	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Header Style'
		),
		array(
			'name' 		=> 'background_color', 
			'type' 		=> 'colorpicker',
			'label' 	=> esc_html__( 'Background Color', 'multipress' ),
			'value'		=> '#ffffff'
		),
		array(
			'name' => 'content_color',
			'type' => 'colorpicker',
			'label' => esc_html__( 'Content Color', 'multipress' ),
			'value'		=> '#333333'
		),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Slider Styling'
		),
			array(
				'name' 		=> 'background_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_title_color',
				'label' 	=> esc_html__( 'Slide Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_title_font_size',
				'label' 	=> esc_html__( 'Slide Title Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'slide_desc_color',
				'label' 	=> esc_html__( 'Slide Description Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_desc_font_size',
				'label' 	=> esc_html__( 'Slide Desc Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'button_bg_color',
				'label' 	=> esc_html__( 'Button Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'button_text_color',
				'label' 	=> esc_html__( 'Button Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_button_font_size',
				'label' 	=> esc_html__( 'Slide Button Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
		
	),
);
