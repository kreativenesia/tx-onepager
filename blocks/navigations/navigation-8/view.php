<!-- Cart Menu -->
<div class="sidemenu_overlay"></div>
<div class="side_menu cart-menu off-pushright">
	<button class="close_side_menu"><i class="ti-arrow-right"></i></button>
	<?php if ( class_exists( 'WooCommerce' ) ) : ?>

		<?php if ( class_exists( 'WC_Widget_Cart' ) ) : ?>
			<?php the_widget( 'WC_Widget_Cart' ); ?>
		<?php else : ?>
			<?php esc_html_e( 'Widget Cart and Widget My Account do not available', 'multipress' ); ?>
		<?php endif; ?>

	<?php endif; ?>
	<?php if ( is_active_sidebar( 'off-canvas' ) ) { ?>
		<?php dynamic_sidebar( 'off-canvas' ); ?>
	<?php } ?>

</div>

<!-- Begin #masthead.site-header-->
<?php $sticky_header_class 		= ( true == $settings['sticky_header']  ) ? 'sticky_header' : ''; ?>
<?php $transparent_header_class = ( true == $settings['transparent_menu'] ) ? 'transparent_menu' : ''; ?>
<?php $background_color 		= ! empty( $styles['background_color'] ) ? "style='background-color:{$styles['background_color']}'" : ''; ?>
<?php $container_size	 		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container'; ?>

<div class="side_menu primary_side_menu off-pushright mobile_menu">
	<button class="close_side_menu"><i class="ti-arrow-right"></i></button>
	<?php get_template_part( 'searchform', 'custom' ); ?>
	
	<?php if ( ! empty( $contents['site_menu'] ) ) :
		$menu_args = array(
			'menu'  			=> $contents['site_menu'],
			'container'       	=> 'nav',
			'container_class' 	=> 'primary_mobile_menu',
			'fallback_cb' 		=> '_multipress_get_dummy_menus',
		);
		if ( class_exists( 'Multipress_Megamenus_Walker' ) ) {
			$menu_args['walker'] = new Multipress_Megamenus_Walker;
		}
		wp_nav_menu( $menu_args );
	endif; ?>
</div>

<header id="multipress-block-<?php echo esc_attr( $id ); ?>" class="page_header style-8 <?php echo esc_attr( $sticky_header_class ); ?> <?php echo esc_attr( $transparent_header_class ); ?>"  data-style="style-8">
	<div class="bottom_header">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<div class="logo">
				<div class="branding">
					<?php 
						$custom_logo 	= $contents['site_logo'];
						$logotag  		= ( is_home() || is_front_page() ) ? 'h1':'div';
						$title 			= get_bloginfo( 'name' );
					 ?>
					<?php if ( ! empty( $custom_logo ) ) : ?>
						<<?php echo esc_attr( $logotag ); ?> id="site-logo">
							<a href="<?php echo esc_url( get_home_url() ); ?>" title="<?php echo get_bloginfo( 'name' ) ?>" role="home">
								<img src="<?php echo esc_url( $contents['site_logo'] ); ?>" alt="<?php esc_html_e( 'Site Logo', 'multipress' ) ?>">
							</a>
						</<?php echo esc_attr( $logotag ); ?>>
					<?php else : ?>
						<?php printf( '<%1$s id="site-title"><a href="%2$s" title="%3$s" rel="home"><span>%4$s</span></a></%1$s>', tag_escape( $logotag ), home_url(), esc_attr( $title ), $title ); ?>
					 <?php endif; ?>

					 <?php if ( ! empty( $contents['site_description'] ) ) : ?>
						<?php printf( '<%1$s id="site-description"><span>%2$s</span></%1$s>', $logotag, $contents['site_description'] ); ?>
					 <?php endif; ?>
				</div>
			</div>
	
			<div class="right_section">
				<button class="side_menu_trigger"><i class="fa fa-bars"></i></button>
			</div>
		</div>
	</div>
</header>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		if(typeof function_name == 'multipress_init_sticky_nav'){
			  multipress_init_sticky_nav()
			}
			if(typeof function_name == 'multipress_init_navigation'){
			  multipress_init_navigation()
			}
			if(typeof function_name == 'multipress_init_header_ui'){
			  multipress_init_header_ui()
			}
	</script>
<?php } ?>