<?php

/*
	
	Content
	- Logo : Image Picker
	- Site Description : Text (Kalo gak diisi gak usah ada tagnya)
	- Menu : Menu Picker

	Settings
	- Show Search: checkbox
	- Show Cart : checkbox (Kalo gak diceklis, markup buat cart Menu jangan dirender)
	- Stiky Header: Checkbox (Nambah class sticky-header di .page_header)

	Style
	- Background-color : color picker
	- Transparent Menu : Checkbox
 */

return array(
	'slug'    	=> 'multipress-nav-8',
	'name'    	=> esc_html__( 'Multipress Navigation 8', 'multipress' ),
	'groups'    => array( 'navbars' ),
	'contents' 	=> array(
		array(
			'name' 		=> 'site_logo', 
			'type' 		=> 'image',
			'label' 	=> esc_html__( 'Custom Logo Image', 'multipress' ),
		),
		array(
			'name' 		=> 'site_description', 
			'type' 		=> 'text',
			'label' 	=> esc_html__( 'Site Description', 'multipress' ),
			'value' 	=> 'Site Description'
		),
		array(
			'name' 		=> 'site_menu', 
			'type' 		=> 'menu',
			'label' 	=> esc_html__( 'Site Menu', 'multipress' ),
		),
	),
	'settings' 	=> array(
		array(
			'name' 		=> 'sticky_header', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Make The Header Sticky?', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'transparent_menu', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Make The Menu Transparent?', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),
	'styles' 	=> array(
		array(
			'name' 		=> 'background_color', 
			'type' 		=> 'colorpicker',
			'label' 	=> esc_html__( 'Background Color', 'multipress' ),
			'value'     => '#ffffff'
		),
		array(
			'name' => 'content_color',
			'type' => 'colorpicker',
			'label' => esc_html__( 'Content Color', 'multipress' ),
			'value'     => '#333333'
		),
		
	),
);
