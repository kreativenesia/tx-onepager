<?php
$backgroundColor = ! empty( $styles["background_color"] ) ? $styles["background_color"] : '' ;
$contentColor    = ! empty( $styles["content_color"] ) ? $styles["content_color"] : '' ;
?>

<?php if ( ! empty( $backgroundColor ) ): ?>
	<?php if( !$settings['transparent_menu'] ) : ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .bottom_header{
		background-color: <?php echo ''.$backgroundColor; ?>;
	}
	<?php else: ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header div.bottom_header{
		background-color: transparent;
	}
	<?php endif; ?>
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header{
		background-color: <?php echo ''.$backgroundColor; ?>;
	}
<?php endif; ?>

<?php if ( ! empty( $contentColor ) ): ?>
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .mobile_nav_trigger,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .logo #site-title span,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .logo #site-description span, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .primary_menu .menu > .menu-item > a,
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .search_button .search_button_trigger, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header .cart_button a, 
	#multipress-block-<?php echo esc_attr( $id ); ?>.page_header.style-8 .side_menu_trigger,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .mobile_nav_trigger,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .logo #site-title span,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .logo #site-description span,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .primary_menu .menu > .menu-item > a,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .search_button .search_button_trigger, 
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .cart_button a,
	#multipress-block-<?php echo esc_attr( $id ); ?> + .mobile-header .side_menu_trigger
	{
		color: <?php echo ''.$contentColor; ?>
	}
<?php endif; ?>