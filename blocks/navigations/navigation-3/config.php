<?php

/*
	Content
	- Logo : Image Picker
	- Site Description : Text (Kalo gak diisi gak usah ada tagnya)
	- Menu : Menu Picker

	- Phone number
	- Email

	- Social Icons :  Repeater [url, icon-picker]

	Settings
	- Show Search: checkbox
	- Show Cart : checkbox (Kalo gak diceklis, markup buat cart Menu jangan dirender)
	- Stiky Header: Checkbox (Nambah class sticky-header di .page_header)

	Style
	- Background-color : color picker
	- Transparent Menu : Checkbox
 */

return array(
	'slug'    	=> 'multipress-nav-3',
	'name'    	=> esc_html__( 'Navigation Style 3', 'multipress' ),
	'groups'    => array( 'navbars' ),
	'contents' 	=> array(
		array(
			'name' 		=> 'site_logo', 
			'type' 		=> 'image',
			'label' 	=> esc_html__( 'Custom Logo Image', 'multipress' ),
		),
		array(
			'name' 		=> 'site_description', 
			'type' 		=> 'text',
			'label' 	=> esc_html__( 'Site Description', 'multipress' ),
			'value' 	=> 'Site Description'
		),
		array(
			'name' 		=> 'site_menu', 
			'type' 		=> 'menu',
			'label' 	=> esc_html__( 'Site Menu', 'multipress' ),
		),
		array(
			'name' 		=> 'phone_number', 
			'type' 		=> 'text',
			'label' 	=> esc_html__( 'Phone Number', 'multipress' ),
			'value' 	=> '0222503530',
		),
		array(
			'name' 		=> 'email_address', 
			'type' 		=> 'text',
			'label' 	=> esc_html__( 'Email Address', 'multipress' ),
			'value' 	=> 'you@yours.com',
		),
		array(
			'name' 		=> 'social_icons', 
			'type' 		=> 'repeater',
			'label' 	=> esc_html__( 'Social Icons', 'multipress' ),
			'fields' 	=> array(
				array(
					array(
						'name' 		=> 'url', 
						'type' 		=> 'text',
						'label' 	=> esc_html__( 'URL', 'multipress' ),
						'value' 	=> '#',
					),
					array(
						'name' 		=> 'icon', 
						'type' 		=> 'icon',
						'label' 	=> esc_html__( 'Icon', 'multipress' ),
					),
				),
			),
		),
	),
	'settings' 	=> array(
		array(
			'name' 		=> 'show_search', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Show Search Box', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'show_cart', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Show Cart', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'sticky_header', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Make The Header Sticky?', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),
	'styles' 	=> array(
		array(
			'name' 		=> 'background_color', 
			'type' 		=> 'colorpicker',
			'label' 	=> esc_html__( 'Background Color', 'multipress' ), 
			'value' 	=> '#ffffff'
		),
		array(
			'name' => 'content_color',
			'type' => 'colorpicker',
			'label' => esc_html__( 'Content Color', 'multipress' ),
			'value' => '#333333'
		),
		array(
			'name' => 'top_content_color',
			'type' => 'colorpicker',
			'label' => esc_html__( 'Top Content Color', 'multipress' ),
			'value' => '#333333'
		),
	),
);
