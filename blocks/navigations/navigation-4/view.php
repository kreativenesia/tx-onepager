<!-- Begin #masthead.site-header-->
<?php $container_size	 		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container'; ?>
<?php $sticky_header_class 		= ( true == $settings['sticky_header']  ) ? 'sticky_header' : ''; ?>
<?php $transparent_header_class = ( true == $settings['transparent_menu'] ) ? 'transparent_menu' : ''; ?>

<header id="multipress-block-<?php echo esc_attr( $id ); ?>" class="page_header style-4 <?php echo esc_attr( $sticky_header_class ); ?> <?php echo esc_attr( $transparent_header_class ); ?>"  data-style="style-4">
	<div class="bottom_header">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<a href="#" class="mobile_nav_trigger pull-left"><i class="fa fa-bars"></i></a>

			<div class="logo">
				<div class="branding">
					<?php 
						$custom_logo 	= $contents['site_logo'];
						$logotag  		= ( is_home() || is_front_page() ) ? 'h1':'div';
						$title 			= get_bloginfo( 'name' );
					 ?>
					<?php if ( ! empty( $custom_logo ) ) : ?>
						<<?php echo esc_attr( $logotag ); ?> id="site-logo">
							<a href="<?php echo esc_url( get_home_url() ); ?>" title="<?php echo get_bloginfo( 'name' ) ?>" role="home">
								<img src="<?php echo esc_url( $contents['site_logo'] ); ?>" alt="<?php esc_html_e( 'Site Logo', 'multipress' ) ?>">
							</a>
						</<?php echo esc_attr( $logotag ); ?>>
					<?php else : ?>
						<?php printf( '<%1$s id="site-title"><a href="%2$s" title="%3$s" rel="home"><span>%4$s</span></a></%1$s>', tag_escape( $logotag ), home_url(), esc_attr( $title ), $title ); ?>
					 <?php endif; ?>

					 <?php if ( ! empty( $contents['site_description'] ) ) : ?>
						<?php printf( '<%1$s id="site-description"><span>%2$s</span></%1$s>', $logotag, $contents['site_description'] ); ?>
					 <?php endif; ?>
				</div>
			</div>
			
			<div class="bottom-section">
				<?php if ( ! empty( $contents['site_menu'] ) ) :
				
					$menu_args = array(
						'menu'  			=> $contents['site_menu'],
						'container'       	=> 'nav',
						'container_class' 	=> 'primary_menu pull-left',
						'fallback_cb' 		=> '_multipress_get_dummy_menus',
					);
					if ( class_exists( 'Multipress_Megamenus_Walker' ) ) {
						$menu_args['walker'] = new Multipress_Megamenus_Walker;
					}
					wp_nav_menu( $menu_args );
				endif; ?>
			</div>
		</div>
	</div>
</header>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		if(typeof function_name == 'multipress_init_sticky_nav'){
		  multipress_init_sticky_nav()
		}
		if(typeof function_name == 'multipress_init_navigation'){
		  multipress_init_navigation()
		}
		if(typeof function_name == 'multipress_init_header_ui'){
		  multipress_init_header_ui()
		}
	</script>
<?php } ?>