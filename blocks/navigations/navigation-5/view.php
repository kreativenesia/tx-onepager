<!-- Cart Menu -->
<div class="sidemenu_overlay"></div>
<div class="side_menu cart-menu off-pushright">
	<button class="close_side_menu"><i class="ti-arrow-right"></i></button>
	<?php if ( class_exists( 'WooCommerce' ) ) : ?>

		<?php if ( class_exists( 'WC_Widget_Cart' ) ) : ?>
			<?php the_widget( 'WC_Widget_Cart' ); ?>
		<?php else : ?>
			<?php esc_html_e( 'Widget Cart and Widget My Account do not available', 'multipress' ); ?>
		<?php endif; ?>

	<?php endif; ?>

	<?php if ( is_active_sidebar( 'off-canvas' ) ) { ?>
		<?php dynamic_sidebar( 'off-canvas' ); ?>
	<?php } ?>

</div>


<!-- Begin #masthead.site-header-->
<?php $container_size	 		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container'; ?>
<?php $sticky_header_class 		= ( true == $settings['sticky_header']  ) ? 'sticky_header' : ''; ?>

<header id="multipress-block-<?php echo esc_attr( $id ); ?>" class="page_header style-5 <?php echo esc_attr( $sticky_header_class ); ?>"  data-style="style-5">
	<div class="top_header">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<div class="left-side pull-left">
				<p><em><?php esc_html_e( 'Contact us on', 'multipress' ); ?></em> 
					<span class="inline-spacer"></span> 
					<?php if ( ! empty( $contents['phone_number'] ) ) : ?>
						<i class="fa fa-phone"></i> <?php echo esc_attr( $contents['phone_number'] ); ?> 
					<?php endif; ?>
					<span class="inline-spacer"></span> 
					<?php if ( ! empty( $contents['email_address'] ) ) : ?>
						<i class="fa fa-envelope-o"></i> <?php echo esc_attr( $contents['email_address'] ); ?>
					<?php endif; ?>
				</p>
			</div>

			<div class="right-side pull-right">
				<?php if ( ! empty( $contents['social_icons'] ) ) : ?>
					<div class="social-links">
						<?php foreach ( $contents['social_icons'] as $social ) : ?>
							<?php 
								$url 	= ! empty( $social['url'] ) ? $social['url'] : '';
								$icon 	= ! empty( $social['icon'] ) ? $social['icon'] : '';
							 ?>
							<a href="<?php echo esc_url( $url ); ?>"><i class="<?php echo esc_attr( $icon ); ?>"></i></a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="bottom_header">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<a href="#" class="mobile_nav_trigger pull-left"><i class="fa fa-bars"></i></a>

			<div class="logo">
				<div class="branding">
					<?php 
						$custom_logo 	= $contents['site_logo'];
						$logotag  		= ( is_home() || is_front_page() ) ? 'h1':'div';
						$title 			= get_bloginfo( 'name' );
					 ?>
					<?php if ( ! empty( $custom_logo ) ) : ?>
						<<?php echo esc_attr( $logotag ); ?> id="site-logo">
							<a href="<?php echo esc_url( get_home_url() ); ?>" title="<?php echo get_bloginfo( 'name' ) ?>" role="home">
								<img src="<?php echo esc_url( $contents['site_logo'] ); ?>" alt="<?php esc_html_e( 'Site Logo', 'multipress' ) ?>">
							</a>
						</<?php echo esc_attr( $logotag ); ?>>
					<?php else : ?>
						<?php printf( '<%1$s id="site-title"><a href="%2$s" title="%3$s" rel="home"><span>%4$s</span></a></%1$s>', tag_escape( $logotag ), home_url(), esc_attr( $title ), $title ); ?>
					 <?php endif; ?>

					 <?php if ( ! empty( $contents['site_description'] ) ) : ?>
						<?php printf( '<%1$s id="site-description"><span>%2$s</span></%1$s>', $logotag, $contents['site_description'] ); ?>
					 <?php endif; ?>
				</div>
			</div>

			<div class="right_section_banner">
				<?php if ( ! empty( $contents['banner_image'] ) ) : ?>
					<div class="banner">
						<?php $banner_url = ! empty( $contents['banner_url'] ) ? $contents['banner_url'] : ''; ?>
						<a href="<?php echo esc_url( $banner_url ); ?>">
							<img src="<?php echo esc_url( $contents['banner_image'] ); ?>" alt="<?php esc_html_e( 'Banner Image', 'multipress' ); ?>">
						</a>
					</div>
				<?php endif; ?>
			</div>
			
			<div class="bottom-section">
				<?php if ( ! empty( $contents['site_menu'] ) ) :
					$menu_args = array(
						'menu'  			=> $contents['site_menu'],
						'container'       	=> 'nav',
						'container_class' 	=> 'primary_menu pull-left',
						'fallback_cb' 		=> '_multipress_get_dummy_menus',
					);
					if ( class_exists( 'Multipress_Megamenus_Walker' ) ) {
						$menu_args['walker'] = new Multipress_Megamenus_Walker;
					}
					wp_nav_menu( $menu_args );
				endif; ?>

				<div class="right_section">
					<ul class="navigation-addon">
						<?php if ( true == $settings['show_search'] ) : ?>
							<li class="search_button">
								<a href="" class="search_button_trigger">
									<i class="fa fa-search"></i>
								</a>
								<?php get_template_part( 'searchform', 'custom' ); ?>
							</li>
						<?php endif; ?>
						<?php if ( true == $settings['show_cart'] ) : ?>
							<li class="cart_button">
								<a href="#">
									<i class="fa fa-shopping-cart"></i>
									<?php if ( class_exists( 'WooCommerce' ) ) : ?>
										<span class="count"><?php echo WC()->cart->cart_contents_count; ?></span> 
									<?php endif; ?>
								</a>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		if(typeof function_name == 'multipress_init_sticky_nav'){
		  multipress_init_sticky_nav()
		}
		if(typeof function_name == 'multipress_init_navigation'){
		  multipress_init_navigation()
		}
		if(typeof function_name == 'multipress_init_header_ui'){
		  multipress_init_header_ui()
		}
	</script>
<?php } ?>