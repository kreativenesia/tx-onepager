<?php

return array(
	'slug' 		=> 'multipress-slider-1', 
	'name' 		=> esc_html__( 'Multipress Slider 1', 'multipress' ),
	'groups' 	=> array( 'sliders' ),
	'contents' 	=> array(
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name' 			=> 'slide_id', 
				'type' 			=> 'select',
				'label' 		=> esc_html__( 'Select a Slider', 'multipress' ),
				'options' 		=> _multipress_get_post_types( 'multipress-slider' ),
			),
	),
	
	'settings'	=> array(
		array(
			'name'		=> 'slider_size',
			'label' 	=> esc_html__( 'Slider Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'slider_fullscreen' => 'Fullscreen',
				'custom_height' 	=> 'Custom Height'
			)
		),
		array(
			'name' 			=> 'slider_height', 
			'type' 			=> 'text',
			'label' 		=> esc_html__( 'Slider Height', 'multipress' ),
			'value'			=> '768px'
		),
		array(
			'name' 			=> 'slider_duration', 
			'type' 			=> 'text',
			'label' 		=> esc_html__( 'Slider Duration', 'multipress' ),
			'value'			=> '5000'
		),
	),

	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Content Styling'
		),
			array(
				'name' 		=> 'background_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_title_color',
				'label' 	=> esc_html__( 'Slide Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_title_font_size',
				'label' 	=> esc_html__( 'Slide Title Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'slide_desc_color',
				'label' 	=> esc_html__( 'Slide Description Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_desc_font_size',
				'label' 	=> esc_html__( 'Slide Desc Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'button_bg_color',
				'label' 	=> esc_html__( 'Button Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'button_text_color',
				'label' 	=> esc_html__( 'Button Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_button_font_size',
				'label' 	=> esc_html__( 'Slide Button Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
	)
);