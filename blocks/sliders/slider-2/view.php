<?php
	$slider_size	 	= ( 'slider_fullscreen' == $settings['slider_size'] ) ? 'slider-fullscreen' : '';
	$slider_height	 	= ! empty( $settings['slider_height'] ) ? 'style="height:'.$settings['slider_height'].'"' : '';
	$slider_duration	= ! empty( $settings['slider_duration'] ) ? $settings['slider_duration'] : '5000';
	$slider_bg_color	= ! empty( $styles['background_overlay'] ) ? "background-color:{$styles['background_overlay']};" : '';

	$args = array(
		'post_type'				=> 'multipress-slider',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 1,
	);
	if ( ! empty( $contents['slide_id'] ) ) {
		$args['p'] 	= $contents['slide_id']; 
	}
	$slides = new WP_Query( $args );
 ?>

<!-- Multipress Slider 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-slider multipress-slider-2 multipress-block-<?php echo esc_attr( $id ); ?>">
	<div class="swiper-container gallery-top <?php echo ''.$slider_size; ?>" data-duration="<?php echo esc_attr( $settings['slider_duration'] ); ?>" <?php echo ''.$slider_height; ?>>
		<div class="swiper-wrapper">

			<?php if ( $slides->have_posts() ) : ?>
				<?php while ( $slides->have_posts() ) : $slides->the_post(); ?>
					
					<?php $slider_data = _multipress_get_meta( '_sliders_details' ); ?>

					<?php if ( ! empty( $slider_data['slides'] ) ) : ?>
				
						<?php foreach ( $slider_data['slides'] as $slide ) : ?>
							
							<?php $slider_align = ! empty( $slide['slider_align'] ) ? $slide['slider_align'] : 'left'; ?>

							<div class="swiper-slide">
								<?php if ( ! empty( $slide['slider_image'] ) ) : ?>
									<?php $bg_image = wp_get_attachment_image_src( $slide['slider_image'], 'full' ); ?>
									<div class="slide-bg" style="<?php echo ''. $slider_bg_color; ?> background-image:url(<?php echo esc_url( $bg_image[0] ); ?>)"></div>
								<?php else : ?>
									<div class="slide-bg" style="<?php echo ''. $slider_bg_color; ?> background-image:url(<?php echo ONEPAGER_URL; ?>/assets/images/onepager/slider-1.jpg)"></div>
								<?php endif; ?>
								<div class="container">
									<div class="slide-content align-<?php echo esc_attr( $slider_align ); ?>">
										<?php if ( ! empty( $slide['slider_title'] ) ) : ?>
											<h2 class="slide-title"><?php echo esc_attr( $slide['slider_title'] ); ?></h2>
										<?php endif; ?>
										<?php if ( ! empty( $slide['slider_content'] ) ) : ?>
											<div class="slide-desc"><?php echo ''.$slide['slider_content']; ?></div>
										<?php endif; ?>
										<?php if ( ! empty( $slide['button_text'] ) ) : ?>
											<?php $slider_link = ! empty( $slide['button_link'] ) ? $slide['button_link'] : ''; ?>
											<a href="<?php echo esc_url( $slider_link ); ?>" class="slide-button"><?php echo esc_attr( $slide['button_text'] ); ?></a>
										<?php endif; ?>
									</div>
								</div>
							</div>

							<?php ob_start(); ?>
								<div class="swiper-slide">
									<div class="slide-thumb">
										<?php $slider_image = wp_get_attachment_image_src( $slide['slider_image'], 'full' ); ?>
										<?php if ( ! empty( $slider_image ) ) : ?>
											<img src="<?php echo _multipress_resize( $slider_image[0], 144, 112 ); ?>" alt="<?php esc_html_e( 'Slider Image', 'multipress' ); ?>">
										<?php else : ?>
											<img src="http://unsplash.it/144/112" alt="<?php esc_html_e( 'Slider Image', 'multipress' ); ?>">
										<?php endif; ?>
									</div>
									<div class="slide-thumb-detail">
										<?php if ( ! empty( $slide['slider_title'] ) ) : ?>
											<h3><?php echo esc_attr( $slide['slider_title'] ); ?></h3>
										<?php endif; ?>
										<i class="fa fa-angle-right"></i>
									</div>
								</div>
							<?php 
								$slider_ob[] 	= ob_get_contents();
								ob_get_clean();
							 ?>

						<?php endforeach; ?>
					<?php endif; ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php else: ?>
				
				<?php $counter = 1; ?>
				<?php while ( 8 > $counter ) : ?>
				    
					<div class="swiper-slide">
						<div class="slide-bg" style="background-image:url(<?php echo ONEPAGER_URL; ?>/assets/images/onepager/slider-2.jpg)"></div>
						<div class="container">
							<div class="slide-content align-center">
								<h2 class="slide-title">My Adventure</h2>
								<div class="slide-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, consequatur.</div>
								<a href="#" class="slide-button">Buy Now</a>
							</div>
						</div>
					</div>

				<?php $counter++; endwhile; ?>
			<?php endif; ?>
		</div>

		<div class="swiper-pagination"></div>
	</div>

	<div class="swiper-container gallery-thumbs">
		<div class="swiper-wrapper">
			<?php if ( ! empty( $slider_ob ) ) : ?>
				<?php foreach ( $slider_ob as $content ) : ?>
					<?php echo ''.$content; ?>			
				<?php endforeach ?>			
			<?php endif; ?>	
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		// .multipress-slider-2
	    var mSlider2 = new Swiper ('.multipress-slider-2 .gallery-top', {
		    pagination: '.swiper-pagination',
		    autoplay: 5000
	    });
	    var mSlider2Thumb = new Swiper('.multipress-slider-2 .gallery-thumbs', {
	        spaceBetween: 0,
	        centeredSlides: true,
	        slidesPerView: 4,
	        touchRatio: 0.2,
	        slideToClickedSlide: true
	    });
	    mSlider2.params.control = mSlider2Thumb;
	    mSlider2Thumb.params.control = mSlider2;
	</script>
<?php } ?>