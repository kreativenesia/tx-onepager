<?php

return array(
	'slug' 		=> 'multipress-slider-5', 
	'name' 		=> esc_html__( 'Multipress Slider 5', 'multipress' ),
	'groups' 	=> array( 'sliders' ),
	'contents' 	=> array(
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'label' 	=> esc_html__( 'Slider Image Width', 'multipress' ),
				'name' 		=> 'slide_image_width', 
				'value' 	=> '1200'
			),
			array(
				'label' 	=> esc_html__( 'Slider Image Height', 'multipress' ),
				'name' 		=> 'slide_image_height', 
				'value' 	=> '424'
			),
			array(
				'name'		=>'slider_items',
				'type'		=>'repeater',
				'fields' 	=> array(
					array(
						array(
							'label' 	=> esc_html__( 'Slider Image', 'multipress' ),
							'name' 		=> 'slider_image', 
							'type' 		=> 'image', 
							'value' 	=> 'https://unsplash.it/1200/424' 
						),
						array(
							'label' 	=> esc_html__( 'Slider Link', 'multipress' ),
							'name' 		=> 'slider_link', 
							'value' 	=> '#'
						),
					),
					array(
						array(
							'label' 	=> esc_html__( 'Slider Image', 'multipress' ),
							'name' 		=> 'slider_image', 
							'type' 		=> 'image', 
							'value' 	=> 'https://unsplash.it/1200/424' 
						),
						array(
							'label' 	=> esc_html__( 'Slider Link', 'multipress' ),
							'name' 		=> 'slider_link', 
							'value' 	=> '#'
						),
					),
				)
			)
	),
	
	'settings'	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 			=> 'slider_duration', 
			'type' 			=> 'text',
			'label' 		=> esc_html__( 'Slider Duration', 'multipress' ),
			'value'			=> '5000'
		),
		array(
			'name' 		=> 'show_pagination', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Show Pagination', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'show_navigation', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Show Navigation', 'multipress' ), 
			'value' 	=> false, 
		),
	),

	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Content Styling'
		),
			array(
				'name' 		=> 'background_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_title_color',
				'label' 	=> esc_html__( 'Slide Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_title_font_size',
				'label' 	=> esc_html__( 'Slide Title Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'slide_desc_color',
				'label' 	=> esc_html__( 'Slide Description Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_desc_font_size',
				'label' 	=> esc_html__( 'Slide Desc Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
			array(
				'name' 		=> 'button_bg_color',
				'label' 	=> esc_html__( 'Button Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'button_text_color',
				'label' 	=> esc_html__( 'Button Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'slide_button_font_size',
				'label' 	=> esc_html__( 'Slide Button Font Size', 'multipress' ),
				'append'	=> 'px',
				'type' 		=> 'text',
			),
	)
);