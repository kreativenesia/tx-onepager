<?php
	$slider_duration	= ! empty( $settings['slider_duration'] ) ? $settings['slider_duration'] : '5000';
	$slider_bg_color	= ! empty( $styles['background_overlay'] ) ? "background-color:{$styles['background_overlay']};" : '';
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
 	$show_pagination	= ( true == $settings['show_pagination'] ) ? 'true' : 'false';
 	$show_navigation	= ( true == $settings['show_navigation'] ) ? 'true' : 'false';
 ?>

<!-- Multipress SLider 5 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-slider multipress-slider-5 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="mp-image-slider" data-duration="<?php echo esc_attr( $settings['slider_duration'] ); ?>" data-nav=<?php echo esc_attr( $show_navigation ); ?> data-paging=<?php echo esc_attr( $show_pagination ); ?> data-autoplay=true>
			<?php foreach ( $contents['slider_items'] as $slide ) : ?>
				
				<?php 
					$img_width 		= ! empty( $slide['slide_image_width'] ) ? $slide['slide_image_width'] : 1200;
					$img_height 	= ! empty( $slide['slide_image_height'] ) ? $slide['slide_image_height'] : 424;
				?>
				<div class="mp-image-slide">
					<a class="img-slide-wrap" style="padding-bottom:<?php echo ( $img_height / $img_width ) * 100; ?>%" href="<?php echo esc_url( $slide['slider_link'] ); ?>"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-lazy="<?php echo esc_url( $slide['slider_image'] ); ?>"></a>
				</div>

			<?php endforeach; ?>
			
		</div>
	</div>
</div>
<?php if ( isset( $_POST['action'] ) && "onepager_save_sections" == $_POST['action'] ) { ?>
	<script>
		multipress_init_slider_block();
	</script>
<?php } ?>