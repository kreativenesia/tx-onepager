.multipress-block-<?php echo esc_attr( $id ); ?> {
	<?php _multipress_print_builder_css( $styles['padding_top'],	'padding-top:'. $styles['padding_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['padding_bottom'],	'padding-bottom:'. $styles['padding_bottom'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_top'],		'margin-top:'. $styles['margin_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_bottom'],	'margin-bottom:'. $styles['margin_bottom'] . 'px;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .slide-content h2 {
	<?php _multipress_print_builder_css( $styles['slide_title_color'],		'color:'. $styles['slide_title_color'] . '!important' ); ?>
	<?php _multipress_print_builder_css( $styles['slide_title_font_size'],		'font-size:'. $styles['slide_title_font_size'] . 'px !important' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .slide-content .slide-desc {
	<?php _multipress_print_builder_css( $styles['slide_desc_color'],		'color:'. $styles['slide_desc_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['slide_desc_font_size'],	'font-size:'. $styles['slide_desc_font_size'] . 'px !important' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .slide-content .slide-button {
	<?php _multipress_print_builder_css( $styles['button_bg_color'],		'background-color:'. $styles['button_bg_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['button_text_color'],		'color:'. $styles['button_text_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['slide_button_font_size'],		'font-size:'. $styles['slide_button_font_size'] . 'px !important' ); ?>
}