<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Pricing Block style 4 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-pricing multipress-pricing--style-4 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title fadeInUp wow'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle fadeInUp wow'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="row equal-height">
			<?php $delay = 150; ?>
			<?php foreach( $contents['items'] as $item ) : ?>
				<div class="col-md-4">
					
					<?php $is_featured = ! empty( $item['set_as_featured'] ) && true == $item['set_as_featured'] ? 'featured' : ''; ?>
					<?php $featured_color = ( 'featured' == $is_featured ) ? 'style="background-color:' . $item["featured_color"] . '"' : ''; ?>
					<div class="pricing <?php echo esc_attr( $is_featured ); ?> wow fadeInUp pricing<?php echo '' . $count; ?>" data-wow-delay="<?php echo ''.$delay; ?>ms" <?php echo ''.$featured_color; ?>>

						<?php if ( ! empty( $item['plan_icon'] ) ) : ?>
							<div class="pricing__image"> 
								<i class="<?php echo esc_attr( $item['plan_icon'] ); ?>" style="color:<?php echo esc_attr( $item['plan_icon_color'] ); ?>;"></i>
							</div>
						<?php endif; ?>
						
						<?php if ( ! empty( $item['plan_name'] ) ) : ?>
							<h3 class="pricing__title" style="color:<?php echo ''.$item['plan_name_color'].'!important'; ?>"><?php echo esc_attr( $item['plan_name'] ); ?></h3>
						<?php endif; ?>

						<div class="pricing__price">
							<?php if ( ! empty( $item['plan_price_prefix'] ) ) : ?>
								<span class="currency" style="color:<?php echo ''.$item['plan_price_prefix_color'].'!important'; ?>"><?php echo esc_attr( $item['plan_price_prefix'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price'] ) ) : ?>
								<span class="value" style="color:<?php echo ''.$item['plan_price_color'].'!important'; ?>"><?php echo esc_attr( $item['plan_price'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price_suffix'] ) ) : ?>
								<span class="duration" style="color:<?php echo ''.$item['plan_price_suffix_color'].'!important'; ?>"><?php echo esc_attr( $item['plan_price_suffix'] ); ?></span>
							<?php endif; ?>
						</div>

						<div class="pricing__price-line">
							<?php if ( ! empty( $item['plan_price_prefix2'] ) ) : ?>
								<span class="currency" style="color:<?php echo ''.$item['plan_price_prefix_color2'].'!important'; ?>"><?php echo esc_attr( $item['plan_price_prefix2'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price2'] ) ) : ?>
								<span class="value" style="color:<?php echo ''.$item['plan_price_color2'].'!important'; ?>"><?php echo esc_attr( $item['plan_price2'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price_suffix2'] ) ) : ?>
								<span class="duration" style="color:<?php echo ''.$item['plan_price_suffix_color2'].'!important'; ?>"><?php echo esc_attr( $item['plan_price_suffix2'] ); ?></span>
							<?php endif; ?>
						</div>
						
						<?php if ( ! empty( $item['plan_description'] ) ) : ?>
							<div class="pricing__desc" style="color:<?php echo ''.$item['plan_description_color'].'!important'; ?>">
								<p><?php echo ''.$item['plan_description']; ?></p>
							</div>
						<?php endif; ?>
						
						<?php if ( ! empty( $item['plan_button_text'] ) ) : ?>
							<div class="pricing__action">
								<?php $target1 ='';
									if ( ! empty( $item['button_target'])){
										$target1 = 'target="_blank"';
									}				
								?>
								<a href="<?php echo esc_url( $item['plan_button_link'] ); ?>" class="button" style="background:<?php echo esc_attr( $item['plan_button_color'] ); ?>;"  <?php echo '' . $target1; ?>><?php echo esc_attr( $item['plan_button_text'] ); ?></a>
							</div>
						<?php endif; ?>

					</div>
				</div>
				<?php $delay +=175; ?>
			<?php endforeach; ?>
			
		</div>
	</div>
</div>