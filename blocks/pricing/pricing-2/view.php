<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Pricing Block style 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-pricing multipress-pricing--style-2 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle wow fadeInUp'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="row">
			<?php $delay = 150; ?>
			<?php foreach( $contents['items'] as $item ) : ?>

				<div class="col-md-6">
					<div class="pricing wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
						<?php if ( ! empty( $item['plan_icon'] ) ) : ?>
							<figure class="pricing__image" style="color:<?php echo ''.$item['plan_icon_color']; ?>">
								<i class="<?php echo esc_attr( $item['plan_icon'] ); ?>"></i>
							</figure>
						<?php endif; ?>

						<?php if ( ! empty( $item['plan_name'] ) ) : ?>
							<h2 class="pricing__title" style="color:<?php echo ''.$item['plan_name_color'].'!important'; ?>"><?php echo esc_attr( $item['plan_name'] ); ?></h2>
						<?php endif; ?>
						
						<div class="pricing__price">
							<?php if ( ! empty( $item['plan_price_prefix'] ) ) : ?>
								<span class="currency" style="color:<?php echo ''.$item['plan_price_prefix_color']; ?>"><?php echo esc_attr( $item['plan_price_prefix'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price'] ) ) : ?>
								<span class="value" style="color:<?php echo ''.$item['plan_price_color']; ?>"><?php echo esc_attr( $item['plan_price'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price_suffix'] ) ) : ?>
								<span class="duration" style="color:<?php echo ''.$item['plan_price_suffix_color']; ?>"><?php echo esc_attr( $item['plan_price_suffix'] ); ?></span>
							<?php endif; ?>
						</div>
						
						<?php if ( ! empty( $item['plan_description'] ) ) : ?>
							<div class="pricing__desc">
								<p style="color:<?php echo ''.$item['plan_description_color']; ?>"><?php echo ''.$item['plan_description']; ?></p>
							</div>
						<?php endif; ?>

						<ul class="pricing__features" style="color:<?php echo ''.$item['plan_items_color']; ?>">
							<?php if ( ! empty( $item['plan_items'] ) ) : ?>
								<?php foreach ( $item['plan_items'] as $plan_item ) : ?>
									<?php 
										$item_exploded 	= explode( '#', $plan_item ); 
										$item_icon 		= ! empty( $item_exploded [0] ) ? $item_exploded [0] : ''; 
										$item_text 		= ! empty( $item_exploded [1] ) ? $item_exploded [1] : ''; 
									?>
									<li class="<?php echo esc_attr( $item_icon ); ?>"><?php echo esc_attr( $item_text ); ?></li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>

						<?php if ( ! empty( $item['plan_button_text'] ) ) : ?>
							<?php $target1 ='';
								if ( ! empty( $item['button_target'])){
									$target1 = 'target="_blank"';
								}				
							?>
							<div class="pricing__action">
								<a href="<?php echo esc_url( $item['plan_button_link'] ); ?>" class="button" <?php echo '' . $target1; ?>><?php echo esc_attr( $item['plan_button_text'] ); ?></a>
							</div>
						<?php endif; ?>

					</div>
				</div>
				<?php $delay +=175; ?>
			<?php endforeach; ?>

		</div>
	</div>
</div>