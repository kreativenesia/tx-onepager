<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Pricing Block style 1 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-pricing multipress-pricing--style-1 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="row equal-height">
			<div class="col-md-6 col-md-offset-1">
				<div class="boxed-content">
					<?php if ( ! empty( $contents['plan_name_1'] ) ) : ?>
						<h2><?php echo esc_attr( $contents['plan_name_1'] ); ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['plan_description_1'] ) ) : ?>
						<p><?php echo ''.$contents['plan_description_1']; ?></p>
					<?php endif; ?>
					<?php if ( ! empty( $contents['plan_tagline_1'] ) ) : ?>
						<h3><?php echo esc_attr( $contents['plan_tagline_1'] ); ?></h3>
					<?php endif; ?>

					<div class="choose-group">
						<?php if ( ! empty( $contents['plan_item_amount_1'] ) ) : ?>
							<div class="choose">
								<input type="radio" name="type" id="type-1" checked>
								<label for="type-1"><?php echo esc_attr( $contents['plan_item_amount_1'] ); ?> <span><?php echo esc_attr( $contents['plan_item_price_1'] ); ?></span></label>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $contents['plan_item_amount_2'] ) ) : ?>
							<div class="choose">
								<input type="radio" name="type" id="type-2">
								<label for="type-2"><?php echo esc_attr( $contents['plan_item_amount_2'] ); ?> <span><?php echo esc_attr( $contents['plan_item_price_2'] ); ?></span></label>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( $contents['plan_item_amount_3'] ) ) : ?>
							<div class="choose">
								<input type="radio" name="type" id="type-3">
								<label for="type-3"><?php echo esc_attr( $contents['plan_item_amount_3'] ); ?> <span><?php echo esc_attr( $contents['plan_item_price_3'] ); ?></span></label>
							</div>
						<?php endif; ?>
					</div>

					<div class="choose-button">
						<?php if ( ! empty( $contents['plan_item_button_text_1'] ) ) : ?>
							<?php $target1 ='';
								if ( ! empty( $contents['button_target1'])){
									$target1 = 'target="_blank"';
								}				
							?>
							<a href="<?php echo esc_url( $contents['plan_item_button_link_1'] ); ?>" class="button active" data-type="type-1" <?php echo '' . $target1; ?>><?php echo esc_attr( $contents['plan_item_button_text_1'] ); ?></a>
						<?php endif; ?>
						<?php if ( ! empty( $contents['plan_item_button_text_2'] ) ) : ?>
							<a href="<?php echo esc_url( $contents['plan_item_button_link_2'] ); ?>" class="button" data-type="type-2"><?php echo esc_attr( $contents['plan_item_button_text_2'] ); ?></a>
						<?php endif; ?>
						<?php if ( ! empty( $contents['plan_item_button_text_3'] ) ) : ?>
							<a href="<?php echo esc_url( $contents['plan_item_button_link_3'] ); ?>" class="button" data-type="type-3"><?php echo esc_attr( $contents['plan_item_button_text_3'] ); ?></a>
						<?php endif; ?>
					</div>

				</div>
			</div>
			
			<div class="col-md-4">
				<div class="boxed-content">
					<?php if ( ! empty( $contents['plan_name_2'] ) ) : ?>
						<h2><?php echo esc_attr( $contents['plan_name_2'] ); ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['plan_description_2'] ) ) : ?>
						<p><?php echo ''.$contents['plan_description_2']; ?></p>
					<?php endif; ?>
					<?php if ( ! empty( $contents['plan_button_text_2'] ) ) : ?>
						<?php $target2 ='';
							if ( ! empty( $contents['button_target2'])){
								$target2 = 'target="_blank"';
							}				
						?>
						<a href="<?php echo esc_url( $contents['plan_button_link_2'] ); ?>" class="button button--primary" <?php echo '' . $target2 ?>><?php echo esc_attr( $contents['plan_button_text_2'] ); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>