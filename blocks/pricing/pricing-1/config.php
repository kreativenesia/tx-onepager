<?php
 
return array(
	'slug' 		=> 'multipress-pricing-1', 
	'name' 		=> esc_html__( 'Multipress Pricing Table 1', 'multipress' ),
	'groups' 	=> array( 'pricing' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> ''
			),
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Plan 1'
		),
			array(
				'name' 		=> 'plan_name_1',
				'label' 	=> esc_html__( 'Plan Name 1', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Personal'
			),
			array(
				'name' 		=> 'plan_description_1',
				'label' 	=> esc_html__( 'Plan Description 1', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, voluptatem?'
			),
			array(
				'name' 		=> 'plan_tagline_1',
				'label' 	=> esc_html__( 'Plan Tagline 1', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Choose Plan'
			),
			array(
				'name' 		=> 'plan_item_amount_1',
				'label' 	=> esc_html__( 'Plan item amount 1', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '10GB'
			),
			array(
				'name' 		=> 'plan_item_price_1',
				'label' 	=> esc_html__( 'Plan item price 1', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'FREE'
			),
			array(
				'name' 		=> 'plan_item_button_text_1',
				'label' 	=> esc_html__( 'Plan item button text 1', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'BUY PLAN'
			),
			array(
				'name' 		=> 'plan_item_button_link_1',
				'label' 	=> esc_html__( 'Plan item button link 1', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),
			array(
				'name' 		=> 'button_target1',
				'type'		=> 'switch',
				'label'		=> esc_html__( 'Button Link 1 Open New Tabs', 'multipress' ),
				'value' 	=> true
			),	
			array(
				'name' 		=> 'plan_item_amount_2',
				'label' 	=> esc_html__( 'Plan item amount 2', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '100GB'
			),
			array(
				'name' 		=> 'plan_item_price_2',
				'label' 	=> esc_html__( 'Plan item price 2', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '$4/MONTH'
			),
			array(
				'name' 		=> 'plan_item_button_text_2',
				'label' 	=> esc_html__( 'Plan item button text 2', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'BUY PLAN'
			),
			array(
				'name' 		=> 'plan_item_button_link_2',
				'label' 	=> esc_html__( 'Plan item button link 2', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),
			array(
				'name' 		=> 'plan_item_amount_3',
				'label' 	=> esc_html__( 'Plan item amount 3', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '200GB'
			),
			array(
				'name' 		=> 'plan_item_price_3',
				'label' 	=> esc_html__( 'Plan item price 3', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '$10/MONTH'
			),
			array(
				'name' 		=> 'plan_item_button_text_3',
				'label' 	=> esc_html__( 'Plan item button text 3', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'BUY PLAN'
			),
			array(
				'name' 		=> 'plan_item_button_link_3',
				'label' 	=> esc_html__( 'Plan item button link 3', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),

		array(
			'type' 	=> 'divider',
			'label' => 'Content Plan 2'
		),
			array(
				'name' 		=> 'plan_name_2',
				'label' 	=> esc_html__( 'Plan Name 2', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Enterprise'
			),
			array(
				'name' 		=> 'plan_description_2',
				'label' 	=> esc_html__( 'Plan Description 2', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit nihil animi at quod cupiditate tempore consequuntur odit fuga quis consectetur.Veritatis odio velit suscipit reiciendis at. Ratione totam magni, quas voluptatum a, esse possimus earum iure rerum minus recusandae autem.'
			),
			array(
				'name' 		=> 'plan_button_text_2',
				'label' 	=> esc_html__( 'Plan Button Text 2', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Contact Us'
			),
			array(
				'name' 		=> 'plan_button_link_2',
				'label' 	=> esc_html__( 'Plan Button Link 2', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),
			array(
				'name' 		=> 'button_target2',
				'type'		=> 'switch',
				'label'		=> esc_html__( 'Button Link 2 Open New Tabs', 'multipress' ),
				'value' 	=> true
			),	
	),
	
	'settings' 	=> array(
		array(
			'name' 		=> 'section_alignment',
			'label' 	=> esc_html__( 'Section Heading Alignment', 'multipress' ),
			'type' 		=> 'select',
			'options' 	=> array(
				'center' 	=> 'Center',
				'left' 		=> 'Left',
				'right' 	=> 'Right',
			)
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
	),
	
	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#e0e6e9'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
			'type' 		=> 'divider',
			'label' 	=> 'Content Styling'
			),
				array(
					'name' 		=> 'title_color',
					'label' 	=> esc_html__( 'Title Color', 'multipress' ),
					'type' 		=> 'colorpicker',
				),
				array(
					'name' 		=> 'title_font_size',
					'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
					'type' 		=> 'text',
					'append'	=> 'px',
					'value' 	=> ''
				),
				array(
					'name' 		=> 'subtitle_color',
					'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
					'type' 		=> 'colorpicker',
				),
				array(
					'name' 		=> 'subtitle_font_size',
					'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
					'type' 		=> 'text',
					'append'	=> 'px',
					'value' 	=> ''
				),
				array(
					'name' 		=> 'plan_name_color',
					'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
					'type' 		=> 'colorpicker',
				),
				array(
					'name' 		=> 'plan_name_font_size',
					'label' 	=> esc_html__( 'Plan Name Font Size', 'multipress' ),
					'type' 		=> 'text',
					'append'	=> 'px',
					'value' 	=> ''
				),
				array(
					'name' 		=> 'plan_description_color',
					'label' 	=> esc_html__( 'Plan Description Color', 'multipress' ),
					'type' 		=> 'colorpicker',
				),
				array(
					'name' 		=> 'plan_description_font_size',
					'label' 	=> esc_html__( 'Plan Description Font Size', 'multipress' ),
					'type' 		=> 'text',
					'append'	=> 'px',
					'value' 	=> ''
				),
				array(
					'name' 		=> 'plan_tagline_color',
					'label' 	=> esc_html__( 'Plan Tagline Color', 'multipress' ),
					'type' 		=> 'colorpicker',
				),
				array(
					'name' 		=> 'plan_tagline_font_size',
					'label' 	=> esc_html__( 'Plan Tagline Font Size', 'multipress' ),
					'type' 		=> 'text',
					'append'	=> 'px',
					'value' 	=> ''
				),
				array(
					'name' 		=> 'plan_amount_color',
					'label' 	=> esc_html__( 'Plan Amount Color', 'multipress' ),
					'type' 		=> 'colorpicker',
				),
				array(
					'name' 		=> 'plan_amount_font_size',
					'label' 	=> esc_html__( 'Plan Amount Font Size', 'multipress' ),
					'type' 		=> 'text',
					'append'	=> 'px',
					'value' 	=> ''
				),
				array(
					'name' 		=> 'plan_price_color',
					'label' 	=> esc_html__( 'Plan Price Color', 'multipress' ),
					'type' 		=> 'colorpicker',
				),
				array(
					'name' 		=> 'plan_price_font_size',
					'label' 	=> esc_html__( 'Plan Price Font Size', 'multipress' ),
					'type' 		=> 'text',
					'append'	=> 'px',
					'value' 	=> ''
				),
	)
);