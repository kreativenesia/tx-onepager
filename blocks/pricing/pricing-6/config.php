<?php

return array(
	'slug' 		=> 'multipress-pricing-6', 
	'name' 		=> esc_html__( 'Multipress Pricing Table 6', 'multipress' ),
	'groups' 	=> array( 'pricing' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> ''
			),
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=> 'plan_items_label',
				'label'		=> esc_html__( 'Plan Items Label', 'multipress' ),
				'value' 	=> array( 
					'Storage',
					'Bandwidth',
					'User',
					'Backup',
					'Supports',
				),
			),
			array( 
				'name' 		=> 'plan_items_label_color', 
				'type' 		=> 'color', 
				'label' 	=> esc_html__( 'Plan Items Label Color', 'multipress' ),
				'value' 	=> '#fff'
			),
			array(
				'name'		=> 'items',
				'type'		=> 'repeater',
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'plan_name',
							'type'		=> 'text',
							'label'		=> esc_html__( 'Plan Name', 'multipress' ),
							'value' 	=> 'Silver'
						),
						array( 
							'name' 		=> 'plan_name_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'set_as_featured',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Set as Featured', 'multipress' ),
							'value' 	=> false
						),
						array(
							'name' 		=> 'featured_color',
							'label' 	=> 'Featured color', 
							'type' 		=> 'color', 
							'value' 	=> '#3e454c' 
						),
						array(
							'name' 		=> 'plan_price',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price', 'multipress' ),
							'value' 	=> '10'
						),
						array( 
							'name' 		=> 'plan_price_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_prefix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Prefix', 'multipress' ),
							'value' 	=> '$'
						),
						array( 
							'name' 		=> 'plan_price_prefix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_suffix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Suffix', 'multipress' ),
							'value' 	=> '/mo'
						),
						array( 
							'name' 		=> 'plan_price_suffix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_button_text',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Button Text', 'multipress' ),
							'value' 	=> 'Buy Plan'
						),
						array(
							'name' 			=> 'plan_button_link',
							'type'			=> 'text', 
							'label'			=> esc_html__( 'Plan Button Link', 'multipress' ),
							'placeholder' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
						array(
							'name' 		=> 'plan_button_color',
							'label' 	=> 'Button color', 
							'type' 		=> 'color', 
							'value' 	=> '#000' 
						),
						array(
							'name'		=> 'plan_items',
							'label'		=> esc_html__( 'Plan Items', 'multipress' ),
							'value' 	=> array( 
								'10GB',
								'10GB',
								'50',
								'Yes',
								'24 Hours',
							),
						),
						array( 
							'name' 		=> 'plan_items_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						
					),
					array(
						array(
							'name' 		=> 'plan_name',
							'type'		=> 'text',
							'label'		=> esc_html__( 'Plan Name', 'multipress' ),
							'value' 	=> 'Gold'
						),
						array( 
							'name' 		=> 'plan_name_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'set_as_featured',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Set as Featured', 'multipress' ),
							'value' 	=> false
						),
						array(
							'name' 		=> 'featured_color',
							'label' 	=> 'Featured color', 
							'type' 		=> 'color', 
							'value' 	=> '#3e454c' 
						),
						array(
							'name' 		=> 'plan_price',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price', 'multipress' ),
							'value' 	=> '20'
						),
						array( 
							'name' 		=> 'plan_price_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_prefix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Prefix', 'multipress' ),
							'value' 	=> '$'
						),
						array( 
							'name' 		=> 'plan_price_prefix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_suffix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Suffix', 'multipress' ),
							'value' 	=> '/mo'
						),
						array( 
							'name' 		=> 'plan_price_suffix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_button_text',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Button Text', 'multipress' ),
							'value' 	=> 'Buy Plan'
						),
						array(
							'name' 			=> 'plan_button_link',
							'type'			=> 'text', 
							'label'			=> esc_html__( 'Plan Button Link', 'multipress' ),
							'placeholder' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
						array(
							'name' 		=> 'plan_button_color',
							'label' 	=> 'Button color', 
							'type' 		=> 'color', 
							'value' 	=> '#000' 
						),
						array(
							'name'		=> 'plan_items',
							'label'		=> esc_html__( 'Plan Items', 'multipress' ),
							'value' 	=> array( 
								'5GB',
								'20GB',
								'100',
								'Yes',
								'24 Hours',
							),
						),
						array( 
							'name' 		=> 'plan_items_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						
					),
					array(
						array(
							'name' 		=> 'plan_name',
							'type'		=> 'text',
							'label'		=> esc_html__( 'Plan Name', 'multipress' ),
							'value' 	=> 'Platinum'
						),
						array( 
							'name' 		=> 'plan_name_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'set_as_featured',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Set as Featured', 'multipress' ),
							'value' 	=> true
						),
						array(
							'name' 		=> 'featured_color',
							'label' 	=> 'Featured color', 
							'type' 		=> 'color', 
							'value' 	=> '#3e454c' 
						),
						array(
							'name' 		=> 'plan_price',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price', 'multipress' ),
							'value' 	=> '30'
						),
						array( 
							'name' 		=> 'plan_price_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_prefix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Prefix', 'multipress' ),
							'value' 	=> '$'
						),
						array( 
							'name' 		=> 'plan_price_prefix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_suffix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Suffix', 'multipress' ),
							'value' 	=> '/mo'
						),
						array( 
							'name' 		=> 'plan_price_suffix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_button_text',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Button Text', 'multipress' ),
							'value' 	=> 'Buy Plan'
						),
						array(
							'name' 			=> 'plan_button_link',
							'type'			=> 'text', 
							'label'			=> esc_html__( 'Plan Button Link', 'multipress' ),
							'placeholder' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
						array(
							'name' 		=> 'plan_button_color',
							'label' 	=> 'Button color', 
							'type' 		=> 'color', 
							'value' 	=> '#000' 
						),
						array(
							'name'		=> 'plan_items',
							'label'		=> esc_html__( 'Plan Items', 'multipress' ),
							'value' 	=> array( 
								'10GB',
								'30GB',
								'200',
								'Yes',
								'24 Hours',
							),
						),
						array( 
							'name' 		=> 'plan_items_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						
					),
					array(
						array(
							'name' 		=> 'plan_name',
							'type'		=> 'text',
							'label'		=> esc_html__( 'Plan Name', 'multipress' ),
							'value' 	=> 'Ultimate'
						),
						array( 
							'name' 		=> 'plan_name_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'set_as_featured',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Set as Featured', 'multipress' ),
							'value' 	=> false
						),
						array(
							'name' 		=> 'featured_color',
							'label' 	=> 'Featured color', 
							'type' 		=> 'color', 
							'value' 	=> '#3e454c' 
						),
						array(
							'name' 		=> 'plan_price',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price', 'multipress' ),
							'value' 	=> '40'
						),
						array( 
							'name' 		=> 'plan_price_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_prefix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Prefix', 'multipress' ),
							'value' 	=> '$'
						),
						array( 
							'name' 		=> 'plan_price_prefix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_price_suffix',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Price Suffix', 'multipress' ),
							'value' 	=> '/mo'
						),
						array( 
							'name' 		=> 'plan_price_suffix_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						array(
							'name' 		=> 'plan_button_text',
							'type'		=> 'text', 
							'label'		=> esc_html__( 'Plan Button Text', 'multipress' ),
							'value' 	=> 'Buy Plan'
						),
						array(
							'name' 			=> 'plan_button_link',
							'type'			=> 'text', 
							'label'			=> esc_html__( 'Plan Button Link', 'multipress' ),
							'placeholder' 	=> '#'
						),
						array(
							'name' 		=> 'button_target',
							'type'		=> 'switch',
							'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
							'value' 	=> true
						),
						array(
							'name' 		=> 'plan_button_color',
							'label' 	=> 'Button color', 
							'type' 		=> 'color', 
							'value' 	=> '#000' 
						),
						array(
							'name'		=> 'plan_items',
							'label'		=> esc_html__( 'Plan Items', 'multipress' ),
							'value' 	=> array( 
								'20GB',
								'50GB',
								'500',
								'Yes',
								'24 Hours',
							),
						),
						array( 
							'name' 		=> 'plan_items_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Plan Name Color', 'multipress' ),
							'value' 	=> '#333330'
						),
						
					),


				)
			),
			
	),
	

	'settings' 	=> array(
		array(
			'name' 		=> 'section_alignment',
			'label' 	=> esc_html__( 'Section Heading Alignment', 'multipress' ),
			'type' 		=> 'select',
			'options' 	=> array(
				'center' 	=> 'Center',
				'left' 		=> 'Left',
				'right' 	=> 'Right',
			)
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
	),
	
	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#a5a8af'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'cover',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Content Styling'
		),
			array(
				'name' 		=> 'title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
	)
);