<?php
	$container_size		= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg		= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- Pricing Block style 6 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-pricing multipress-pricing--style-6 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</div>
		<?php endif; ?> 

		<div class="pricing-table">
			<ul class="pricing-feature-title first" style="color:<?php echo ''.$contents['plan_items_label_color']; ?>">

				<?php foreach( $contents['plan_items_label'] as $item ) : ?>
					<?php if ( ! empty( $item ) ) : ?>
						<li><?php echo esc_attr( $item ); ?></li>
					<?php endif; ?>
				<?php endforeach; ?>

			</ul>
			
			<?php $item_counter = 1; ?>
			<?php foreach( $contents['items'] as $item ) : ?>
				
				<?php 
					if ( 1 == $item_counter ) : 
						$wrapper_class = 'first';
					elseif ( 4 == $item_counter ) :
						$wrapper_class = 'last';
					else : 
						$wrapper_class = '';
					endif; 
				?>
				<?php $is_featured = ( true == $item['set_as_featured'] ) ? 'featured' : ''; ?>
				<?php $featured_color = ( 'featured' == $is_featured ) ? 'style="background-color:' . $item["featured_color"] . '"' : ''; ?>
				<div class="pricing <?php echo esc_attr( $wrapper_class ); ?> <?php echo esc_attr( $is_featured ); ?>">
					<?php if ( ! empty( $item['plan_name'] ) ) : ?>
						<div class="pricing__title" style="color:<?php echo ''.$item['plan_name_color']; ?>"><?php echo esc_attr( $item['plan_name'] ); ?></div>
					<?php endif; ?>
					<div class="pricing__content" <?php echo ''.$featured_color; ?>>
						<div class="pricing__price" <?php echo ''.$featured_color; ?>>
							<?php if ( ! empty( $item['plan_price_prefix'] ) ) : ?>
								<span class="currency" style="color:<?php echo ''.$item['plan_price_prefix_color']; ?>"><?php echo esc_attr( $item['plan_price_prefix'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price'] ) ) : ?>
								<span class="value" style="color:<?php echo ''.$item['plan_price_color']; ?>"><?php echo esc_attr( $item['plan_price'] ); ?></span>
							<?php endif; ?>

							<?php if ( ! empty( $item['plan_price_suffix'] ) ) : ?>
								<span class="duration" style="color:<?php echo ''.$item['plan_price_suffix_color']; ?>"><?php echo esc_attr( $item['plan_price_suffix'] ); ?></span>
							<?php endif; ?>
						</div>

						<ul class="pricing__features">
							<?php if ( ! empty( $item['plan_items'] ) ) : ?>
								<?php foreach ( $item['plan_items'] as $plan_item ) : ?>
									<?php 
										$item_text 	= ! empty( $plan_item ) ? $plan_item : ''; 
									?>
									<li style="color:<?php echo ''.$item['plan_items_color']; ?>"><span class="label">Label</span><?php echo esc_attr( $item_text ); ?></li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>

						<?php if ( ! empty( $item['plan_button_text'] ) ) : ?>
							<div class="pricing__action">
								<?php $target1 ='';
									if ( ! empty( $item['button_target'])){
										$target1 = 'target="_blank"';
									}				
								?>
								<?php echo esc_attr( $item['plan_button_link']); ?>
								<a href="<?php echo esc_url( $item['plan_button_link'] ); ?>" class="button" style="background:<?php echo esc_attr( $item['plan_button_color'] ); ?>;" <?php echo '' . $target1; ?>><?php echo esc_attr( $item['plan_button_text'] ); ?></a>
							</div>
						<?php endif; ?>

					</div>
				</div>

			<?php $item_counter++; endforeach; ?>
			
			<ul class="pricing-feature-title last" style="color:<?php echo ''.$contents['plan_items_label_color']; ?>">
				<?php foreach( $contents['plan_items_label'] as $item ) : ?>
					<?php if ( ! empty( $item ) ) : ?>
						<li><?php echo esc_attr( $item ); ?></li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>