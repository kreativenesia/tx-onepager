<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-13 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>"> 
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		
		<div class="row">
			<?php $delay = 100; ?>
			<?php foreach( $contents['items'] as $feature ) : ?>
			
				<div class="col-md-4">
					<div class="person-info">
						<figure class="person-info__image">
							<?php if ( ! empty( $feature['image'] ) ) : ?>
								<img src="<?php echo esc_url( $feature['image'] ); ?>">
							<?php endif; ?>
						</figure>
						<div class="person-info__detail">
							<?php if ( ! empty( $feature['name'] ) ) : ?>
								<h3 class="person-info__name"><?php echo esc_attr( $feature['name'] ); ?></h3>
							<?php endif; ?>
							<div class="person-info__contact">
								<ul>
									<?php if ( ! empty( $feature['phone'] ) ) : ?>
										<li><i class="dripicons-phone"></i><a href="tel:<?php echo esc_attr( $feature['phone'] ); ?>"><?php echo esc_attr( $feature['phone'] ); ?></a></li>
									<?php endif; ?>
									<?php if ( ! empty( $feature['email'] ) ) : ?>
										<li><i class="dripicons-mail"></i><a href="mailto:<?php echo esc_attr( $feature['email'] ); ?>"><?php echo esc_attr( $feature['email'] ); ?></a></li>
									<?php endif; ?>
								</ul>
							</div>
							<div class="person-info__links">
								<?php if ( ! empty( $feature['twitter_link'] ) ) : ?>
									<a href="<?php echo esc_url( $feature['twitter_link'] ); ?>"><i class="fa fa-twitter"></i></a>
								<?php endif; ?>
								<?php if ( ! empty( $feature['facebook_link'] ) ) : ?>
									<a href="<?php echo esc_url( $feature['facebook_link'] ); ?>"><i class="fa fa-facebook"></i></a>
								<?php endif; ?>
								<?php if ( ! empty( $feature['linkedin_link'] ) ) : ?>
									<a href="<?php echo esc_url( $feature['linkedin_link'] ); ?>"><i class="fa fa-linkedin"></i></a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<?php $delay +=175; ?>
			<?php endforeach; ?>
			
		</div>
	</div>
</div>