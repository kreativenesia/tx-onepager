<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-21 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<span class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></span>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<ul class="agenda-list">
			<?php $delay = 100; ?>
			<?php foreach( $contents['items'] as $feature ) : ?>
				
				<li class="agenda">
					<figure class="agenda__image">
						<?php if ( ! empty( $feature['image'] ) ) : ?>
							<img src="<?php echo esc_url( $feature['image'] ); ?>" alt="">
						<?php endif; ?>
					</figure>
					<div class="agenda__detail">
						<?php if ( ! empty( $feature['time'] ) ) : ?>
							<small><?php echo esc_attr( $feature['time'] ); ?></small>
						<?php endif; ?>
						<?php if ( ! empty( $feature['title'] ) ) : ?>
							<h3><?php echo esc_attr( $feature['title'] ); ?></h3>
						<?php endif; ?>
						<?php if ( ! empty( $feature['description'] ) ) : ?>
							<p><?php echo ''.$feature['description']; ?></p>
						<?php endif; ?>
					</div>
				</li>
				
				<?php $delay +=175; ?>
			<?php endforeach; ?>
			
		</ul>
		
		<div class="align-center">
			<?php if ( ! empty( $contents['button_text'] ) || ! empty( $contents['button_link'] ) ) : ?>
				<?php $target1 ='';
					if ( ! empty( $contents['button_target'])){
						$target1 = 'target="_blank"';
					}				
				?>
				<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button button--<?php echo esc_attr( $styles['button_style'] ); ?>" <?php echo '' . $target1 ?>><?php echo esc_attr( $contents['button_text'] ); ?></a>
			<?php endif; ?>
		</div>
	</div>
</div>