<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-9 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="image-edge">
		<div class="image-section" style="background-image: url(<?php echo esc_url( $contents['section_image'] ); ?>);"></div>
		<div class="content-section content-section-1">
			<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class="section-title"><span class="num">01</span><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<p><?php echo ''.$contents['section_subtitle']; ?></p>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="image-edge image-right">
		<div class="image-section" style="background-image: url(<?php echo esc_url( $contents['section_image_2'] ); ?>);"></div>
		<div class="content-section content-section-2">
			<?php if ( ! empty( $contents['section_title_2'] ) || ! empty( $contents['section_subtitle_2'] ) ) : ?>
				<?php if ( ! empty( $contents['section_title_2'] ) ) : ?>
					<h2 class="section-title"><span class="num">02</span><?php echo esc_attr( $contents['section_title_2'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle_2'] ) ) : ?>
					<p><?php echo ''.$contents['section_subtitle_2']; ?></p>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>