.multipress-block-<?php echo esc_attr( $id ); ?> {
	<?php if ( ! empty( $styles['bg_image'] ) ) : ?>
		<?php _multipress_print_builder_css( $styles['bg_image'], 		'background-image:url('.$styles['bg_image'].' )' ); ?>
		<?php _multipress_print_builder_css( $styles['bg_size'],  		'background-size:'. esc_attr( $styles['bg_size'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_repeat'],		'background-repeat:'. esc_attr( $styles['bg_repeat'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_position'],		'background-position:'. esc_attr( $styles['bg_position'] ) ); ?>
	<?php endif; ?>
	<?php _multipress_print_builder_css( $styles['bg_color'],		'background-color:'. $styles['bg_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['padding_top'],	'padding-top:'. $styles['padding_top'].'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['padding_bottom'],	'padding-bottom:'. $styles['padding_bottom'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_top'],		'margin-top:'. $styles['margin_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_bottom'],	'margin-bottom:'. $styles['margin_bottom'] . 'px;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .background-overlay {
	<?php _multipress_print_builder_css( $styles['bg_overlay'],		'background-color:'. $styles['bg_overlay'] ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .section-header h2 {
	<?php _multipress_print_builder_css( $styles['section_title_color'],		'color:'. $styles['section_title_color'] . '!important;' ); ?>
	<?php _multipress_print_builder_css( $styles['section_title_font_size'],	'font-size:'. $styles['section_title_font_size'] . 'px'. ' !important;'  ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .section-header .section-subtitle {
	<?php _multipress_print_builder_css( $styles['section_subtitle_color'],		'color:'. $styles['section_subtitle_color']. '!important;' ); ?>
	<?php _multipress_print_builder_css( $styles['section_subtitle_font_size'],	'font-size:'. $styles['section_subtitle_font_size'] . 'px !important' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .image-edge:nth-child(odd) {
	<?php _multipress_print_builder_css( $styles['top_section_bg'],	'background-color:'. $styles['top_section_bg'] ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .content-section-1 h2 {
	<?php _multipress_print_builder_css( $styles['top_title_color'],	'color:'. $styles['top_title_color'] . ' !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .content-section-1 p {
	<?php _multipress_print_builder_css( $styles['top_content_color'],	'color:'. $styles['top_content_color'] ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .image-edge {
	<?php _multipress_print_builder_css( $styles['bottom_section_bg'],	'background-color:'. $styles['bottom_section_bg'] ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .content-section-2 h2 {
	<?php _multipress_print_builder_css( $styles['bottom_title_color'],	'color:'. $styles['bottom_title_color'] . ' !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .content-section-2 p {
	<?php _multipress_print_builder_css( $styles['bottom_content_color'],	'color:'. $styles['bottom_content_color'] ); ?>
}


