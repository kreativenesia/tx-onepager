<?php

return array(
	'slug'    	=> 'multipress-landing-10',
	'name' 		=> esc_html__( 'Landing Squeeze 10', 'multipress' ),
	'groups'    => array( 'landing' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Top Section'
		),
			array(
				'name' 		=> 'top_section_title',
				'label' 	=> esc_html__( 'Top Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Lorem ipsum dolor sit amet'
			),
			array(
				'name' 		=> 'top_section_phone',
				'label' 	=> esc_html__( 'Top Section Phone Number', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '+62222503530'
			),
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Middle Section'
		),
			array(
				'name' 		=> 'middle_section_title',
				'label' 	=> esc_html__( 'Middle Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'This is your primary heading copy'
			),
			array(
				'name' 		=> 'middle_section_subtitle',
				'label' 	=> esc_html__( 'Middle Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, eius!'
			),

		array(
			'type' 		=> 'divider',
			'value' 	=> 'Bottom Section'
		),
			array(
				'name' 		=> 'bottom_section_title',
				'label' 	=> esc_html__( 'Bottom Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Get an Electronic <br>mail when it\'s ready'
			),
			array(
				'name' 		=> 'bottom_section_subtitle',
				'label' 	=> esc_html__( 'Bottom Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'That\'s right - no stamps required'
			),
			array(
				'name' 		=> 'form_id',
				'label' 	=> esc_html__( 'Paste Form Shortcode Here', 'multipress' ),
				'type' 		=> 'textarea',
			),
			
	),

	'settings' 	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Top Section Styling'
		),
			array(
				'name' 		=> 'top_section_background',
				'label' 	=> esc_html__( 'Top Section Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'top_section_title_color',
				'label' 	=> esc_html__( 'Top Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'top_section_title_font_size',
				'label' 	=> esc_html__( 'Top Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
			array(
				'name' 		=> 'top_section_phone_color',
				'label' 	=> esc_html__( 'Top Phone Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'top_section_phone_font_size',
				'label' 	=> esc_html__( 'Top Phone Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Middle Section Styling'
		),
			array(
				'name' 		=> 'middle_section_background',
				'label' 	=> esc_html__( 'Top Section Background', 'multipress' ),
				'type' 		=> 'image',
				'value'		=> 'http://unsplash.it/1980/800?image=1083'
			),
			array(
				'name' 		=> 'middle_section_title_color',
				'label' 	=> esc_html__( 'Middle Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array( 
				'name' 		=> 'middle_section_title_font_size',
				'label' 	=> esc_html__( 'Middle Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
			array(
				'name' 		=> 'middle_section_subtitle_color',
				'label' 	=> esc_html__( 'Middle Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'middle_section_subtitle_font_size',
				'label' 	=> esc_html__( 'Middle Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Bottom Section Styling'
		),
			array(
				'name' 		=> 'bottom_section_background',
				'label' 	=> esc_html__( 'Bottom Section Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'form_background',
				'label' 	=> esc_html__( 'Form Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'bottom_section_title_color',
				'label' 	=> esc_html__( 'Bottom Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'bottom_section_title_font_size',
				'label' 	=> esc_html__( 'Bottom Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
			array(
				'name' 		=> 'bottom_section_subtitle_color',
				'label' 	=> esc_html__( 'Bottom Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'bottom_section_subtitle_font_size',
				'label' 	=> esc_html__( 'Bottom Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),

	),

);
