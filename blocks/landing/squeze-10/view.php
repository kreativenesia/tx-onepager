<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-10 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<div class="top-section">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<?php if ( ! empty( $contents['top_section_title'] ) || ! empty( $contents['top_section_phone'] ) ) : ?>
				<?php if ( ! empty( $contents['top_section_title'] ) ) : ?>
					<div class="left-section pull-left">
						<p><?php echo esc_attr( $contents['top_section_title'] ); ?></p>
					</div>
				<?php endif; ?>
				<?php if ( ! empty( $contents['top_section_phone'] ) ) : ?>
					<div class="right-section pull-right">
						<a href="#"><i class="ti-headphone"></i> <?php echo ''.$contents['top_section_phone']; ?></a>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
 
	<div class="middle-section">
		<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
			<div class="background-overlay"></div>
		<?php endif; ?>

		<div class="<?php echo esc_attr( $container_size ); ?>">
			<div class="row">
				<div class="col-md-6 col-md-offset-6">
					<?php if ( ! empty( $contents['middle_section_title'] ) || ! empty( $contents['middle_section_subtitle'] ) ) : ?>
						<div class="section-header">
							<?php if ( ! empty( $contents['middle_section_title'] ) ) : ?>
								<h2 class='section-title'><?php echo esc_attr( $contents['middle_section_title'] ); ?></h2>
							<?php endif; ?>
							<?php if ( ! empty( $contents['middle_section_subtitle'] ) ) : ?>
								<div class='section-subtitle'><?php echo ''.$contents['middle_section_subtitle']; ?></div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<div class="bottom-section">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<div class="row">
				<?php if ( ! empty( $contents['bottom_section_title'] ) || ! empty( $contents['bottom_section_subtitle'] ) ) : ?>
					<div class="col-md-4">
						<?php if ( ! empty( $contents['top_section_title'] ) ) : ?>
							<h2 class="form-title"><?php echo ''. $contents['bottom_section_title']; ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $contents['bottom_section_subtitle'] ) ) : ?>
							<small class="form-subtitle"><?php echo ''.$contents['bottom_section_subtitle']; ?></small>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				
				<div class="col-md-8">
					<div class="mp-form col">
						<?php if ( ! empty( $contents['form_id'] ) ) : ?>
							<?php echo do_shortcode( $contents['form_id'] ); ?>
						<?php else : ?>
							<div class="field-control">
								<input type="text" placeholder="Your Email">
							</div>
							<div class="field-control">
								<input type="submit" value="Stay informed">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>