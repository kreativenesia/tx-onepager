<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-18 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
				<?php if ( ! empty( $contents['button_text'] ) ) : ?>
					<?php $target1 ='';
						if ( ! empty( $contents['button_target'])){
							$target1 = 'target="_blank"';
						}				
					?>
					<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button button--<?php echo esc_attr( $styles['button_style'] ); ?>" <?php echo '' . $target1 ?>><?php echo esc_attr( $contents['button_text'] ); ?></a>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="row">
			<div class="col-md-2 col-md-offset-2">
				<?php if ( ! empty( $contents['image'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['image'] ); ?>">
				<?php endif; ?>
			</div>
			<div class="col-md-6">
				<?php if ( ! empty( $contents['content'] ) ) : ?>
					<?php echo wpautop( $contents['content'] ); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>