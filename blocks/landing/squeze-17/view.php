<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-17 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row flex">
			<div class="col-md-6 content-section">
				<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
					<?php if ( ! empty( $contents['section_title'] ) ) : ?>
						<h2><?php echo esc_attr( $contents['section_title'] ); ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
						<span><?php echo ''.$contents['section_subtitle']; ?></span>
					<?php endif; ?>
				<?php endif; ?>

				<div class="feature-list style-2 columns-1 align-left icon_left">
					<div class="feature wow fadeInUp" data-wow-delay="100ms">
						<div class="feature__image">
							<i class="<?php echo esc_attr( $contents['media'] ); ?>"></i>
						</div>
						<div class="feature__content">
							<?php if ( ! empty( $contents['title'] ) ) : ?>
								<h2 class="feature__title"><?php echo esc_attr( $contents['title'] ); ?></h2>
							<?php endif; ?>
							<div class="feature__desc">
								<?php if ( ! empty( $contents['content'] ) ) : ?>
									<?php echo ''.$contents['content']; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<?php if ( ! empty( $contents['image_1'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['image_1'] ); ?>">
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?php if ( ! empty( $contents['image_2'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['image_2'] ); ?>">
				<?php endif; ?>
			</div>
			<div class="col-md-6">
				<?php if ( ! empty( $contents['image_3'] ) ) : ?>
					<img src="<?php echo esc_url( $contents['image_3'] ); ?>">
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>