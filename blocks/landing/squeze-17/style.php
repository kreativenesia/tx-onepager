.multipress-block-<?php echo esc_attr( $id ); ?> {
	<?php if ( ! empty( $styles['bg_image'] ) ) : ?>
		<?php _multipress_print_builder_css( $styles['bg_image'], 		'background-image:url('.$styles['bg_image'].' )' ); ?>
		<?php _multipress_print_builder_css( $styles['bg_size'],  		'background-size:'. esc_attr( $styles['bg_size'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_repeat'],		'background-repeat:'. esc_attr( $styles['bg_repeat'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_position'],		'background-position:'. esc_attr( $styles['bg_position'] ) ); ?>
	<?php endif; ?>
	<?php _multipress_print_builder_css( $styles['bg_color'],		'background-color:'. $styles['bg_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['padding_top'],	'padding-top:'. $styles['padding_top'].'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['padding_bottom'],	'padding-bottom:'. $styles['padding_bottom'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_top'],		'margin-top:'. $styles['margin_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_bottom'],	'margin-bottom:'. $styles['margin_bottom'] . 'px;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .background-overlay {
	<?php _multipress_print_builder_css( $styles['bg_overlay'],		'background-color:'. $styles['bg_overlay'] ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .content-section h2 {
	<?php _multipress_print_builder_css( $styles['section_title_color'],		'color:'. $styles['section_title_color'] . '!important;' ); ?>
	<?php _multipress_print_builder_css( $styles['section_title_font_size'],	'font-size:'. $styles['section_title_font_size'] . 'px !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .content-section span {
	<?php _multipress_print_builder_css( $styles['section_subtitle_color'],		'color:'. $styles['section_subtitle_color'] . '!important;' ); ?>
	<?php _multipress_print_builder_css( $styles['section_subtitle_font_size'],	'font-size:'. $styles['section_subtitle_font_size'] . 'px !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .feature__image i {
	<?php _multipress_print_builder_css( $styles['icon_color'],		'color:'. $styles['icon_color'] . '!important;' ); ?>
	<?php _multipress_print_builder_css( $styles['icon_font_size'],	'font-size:'. $styles['icon_font_size'] . 'px !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .feature__content .feature__title {
	<?php _multipress_print_builder_css( $styles['title_color'],		'color:'. $styles['title_color'] . '!important;' ); ?>
	<?php _multipress_print_builder_css( $styles['title_font_size'],	'font-size:'. $styles['title_font_size'] . 'px !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .feature__content .feature__title {
	<?php _multipress_print_builder_css( $styles['title_border_color'],		'border-color:'. $styles['title_border_color'] . '!important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .feature__content .feature__desc, .multipress-block-<?php echo esc_attr( $id ); ?> .feature__content .feature__desc a {
	<?php _multipress_print_builder_css( $styles['content_color'],		'color:'. $styles['content_color'] . '!important;' ); ?>
	<?php _multipress_print_builder_css( $styles['content_font_size'],	'font-size:'. $styles['content_font_size'] . 'px !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .content-section:before {
	<?php _multipress_print_builder_css( $styles['content_area_bg'],	'background-color:'. $styles['content_area_bg'] . '!important;' ); ?>
}