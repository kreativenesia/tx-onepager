<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-1 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="masonry-image-grid">
			<?php foreach( $contents['items'] as $feature ) : ?>
				<figure class="masonry-image">
					<div class="masonry-image__inner">
						<?php if ( ! empty( $feature['image'] ) ) : ?>
							<img src="<?php echo esc_url( $feature['image'] ); ?>" alt="">
						<?php endif; ?>
						<figcaption>
							<?php if ( ! empty( $feature['title'] ) ) : ?>
								<h2 class="masonry-image__title"><?php echo ''.$feature['title']; ?></h2>
							<?php endif; ?>
							<?php if ( ! empty( $feature['description'] ) ) : ?>
								<small class="masonry-image__desc"><?php echo ''.$feature['description']; ?></small>
							<?php endif; ?>
						</figcaption>
					</div>
				</figure>
			<?php endforeach; ?>
		</div>
	</div>
</div>