<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-20 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<span class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></span>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="row">
			<?php $delay = 100; ?>
			<?php foreach( $contents['items'] as $feature ) : ?>
				
				<div class="col-md-3">
					<div class="speaker">
						<figure class="speaker__image">
							<?php if ( ! empty( $feature['image'] ) ) : ?>
								<img src="<?php echo esc_url( $feature['image'] ); ?>" alt="">
							<?php endif; ?>
							<figcaption>
								<?php if ( ! empty( $feature['name'] ) ) : ?>
									<h3 class="speaker__name"><?php echo esc_attr( $feature['name'] ); ?></h3>
								<?php endif; ?>
								<?php if ( ! empty( $feature['title'] ) ) : ?>
									<span class="speaker__title"><?php echo esc_attr( $feature['title'] ); ?></span>
								<?php endif; ?>
							</figcaption>
						</figure>
					</div>
				</div>

				<?php $delay +=175; ?>
			<?php endforeach; ?>
			
		</div>
	</div>
</div>