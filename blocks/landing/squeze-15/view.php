<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-15 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>"> 
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<span class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></span>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="team-list">
					<?php $delay = 100; ?>
					<?php foreach( $contents['items'] as $feature ) : ?>
						
						<li class="team-item">
							<figure class="team-item__image">
								<?php if ( ! empty( $feature['image'] ) ) : ?>
									<img src="<?php echo esc_url( $feature['image'] ); ?>">	
								<?php endif; ?>
							</figure>
							<div class="team-item__detail">
								<h2><?php echo esc_attr( $feature['name'] ); ?> &mdash; 
								<span class="team-position"><?php echo esc_attr( $feature['position'] ); ?></span></h2>
								<p><?php echo ''.$feature['description']; ?>.</p>
							</div>
						</li>
						
						<?php $delay +=175; ?>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>
</div>