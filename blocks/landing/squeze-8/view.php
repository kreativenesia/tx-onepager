<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-8 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="top-section">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
				<div class="section-header">
					<?php if ( ! empty( $contents['section_title'] ) ) : ?>
						<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
						<small class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></small>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="bottom-section">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<div class="row">
				<?php if ( ! empty( $contents['form_title'] ) || ! empty( $contents['form_subtitle'] ) ) : ?>
					<div class="col-md-4">
						<?php if ( ! empty( $contents['form_title'] ) ) : ?>
							<h2 class="form-title"><?php echo ''.$contents['form_title']; ?></h2>
						<?php endif; ?>
						<?php if ( ! empty( $contents['form_subtitle'] ) ) : ?>
							<small class="form-subtitle"><?php echo ''.$contents['form_subtitle']; ?></small>
						<?php endif; ?>
					</div>
				<?php endif; ?>

				<div class="col-md-8">
					<div class="mp-form col">
						<?php if ( ! empty( $contents['form_id'] ) ) : ?>
							<?php echo do_shortcode( $contents['form_id'] ); ?>
						<?php else : ?>
							<div class="field-control">
								<input type="text" placeholder="Your Email">
							</div>
							<div class="field-control">
								<input type="submit" value="Stay informed">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>