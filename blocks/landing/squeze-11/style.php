.multipress-block-<?php echo esc_attr( $id ); ?> {
	<?php _multipress_print_builder_css( $styles['padding_top'],	'padding-top:'. $styles['padding_top'].'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['padding_bottom'],	'padding-bottom:'. $styles['padding_bottom'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_top'],		'margin-top:'. $styles['margin_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_bottom'],	'margin-bottom:'. $styles['margin_bottom'] . 'px;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .middle-section {
	<?php _multipress_print_builder_css( $styles['middle_section_background'],		'background-image:url('. $styles['middle_section_background'] . ')' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .background-overlay {
	<?php _multipress_print_builder_css( $styles['bg_overlay'],		'background-color:'. $styles['bg_overlay'] ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .section-header h2 {
	<?php _multipress_print_builder_css( $styles['middle_section_title_color'],		'color:'. $styles['middle_section_title_color'] . ' !important;' ); ?>
	<?php _multipress_print_builder_css( $styles['middle_section_title_font_size'],	'font-size:'. $styles['middle_section_title_font_size'] . 'px !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .section-header .section-subtitle {
	<?php _multipress_print_builder_css( $styles['middle_section_subtitle_color'],		'color:'. $styles['middle_section_subtitle_color'] . ' !important;' ); ?>
	<?php _multipress_print_builder_css( $styles['middle_section_subtitle_font_size'],	'font-size:'. $styles['middle_section_subtitle_font_size'] . 'px !important;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .top-section { 
	<?php _multipress_print_builder_css( $styles['top_section_background'],		'background-color:'. $styles['top_section_background'] ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .top-section p {
	<?php _multipress_print_builder_css( $styles['top_section_title_color'],		'color:'. $styles['top_section_title_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['top_section_title_font_size'],	'font-size:'. $styles['top_section_title_font_size'] . 'px;' ); ?>
}

.multipress-block-<?php echo esc_attr( $id ); ?> .top-section  a,  .multipress-block-<?php echo esc_attr( $id ); ?> .top-section i {
	<?php _multipress_print_builder_css( $styles['top_section_phone_color'],		'color:'. $styles['top_section_phone_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['top_section_phone_font_size'],	'font-size:'. $styles['top_section_phone_font_size'] . 'px;' ); ?>
}

