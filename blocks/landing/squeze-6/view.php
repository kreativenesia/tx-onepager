<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-6 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<small class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></small>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="align-center">
			<p>
				<?php if ( ! empty( $contents['button_text'] ) || ! empty( $contents['button_link'] ) ) : ?>
					<?php $target1 ='';
						if ( ! empty( $contents['button_target1'])){
							$target1 = 'target="_blank"';
						}				
					?>
					<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button button--primary button--<?php echo esc_attr( $styles['button_style'] ); ?>" <?php echo '' . $target1 ?>><?php echo esc_attr( $contents['button_text'] ); ?></a>
				<?php endif; ?>
				<?php if ( ! empty( $contents['button_text_2'] ) || ! empty( $contents['button_link_2'] ) ) : ?>
					<?php $target2 ='';
						if ( ! empty( $contents['button_target2'])){
							$target2 = 'target="_blank"';
						}				
					?>
					<a href="<?php echo esc_url( $contents['button_link_2'] ); ?>" class="button button--secondary button--<?php echo esc_attr( $styles['button_style_2'] ); ?>" <?php echo '' . $target2 ?>><?php echo esc_attr( $contents['button_text_2'] ); ?></a>
				<?php endif; ?>
			</p>
			<?php if ( ! empty( $contents['content'] ) ) : ?>
				<small><?php echo ''.$contents['content']; ?></small>
			<?php endif; ?>
		</div>
	</div>
</div>