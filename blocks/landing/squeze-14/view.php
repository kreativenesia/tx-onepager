<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-14 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<div class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<span class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></span>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		
		<figure>
			<?php if ( ! empty( $contents['section_image'] ) ) : ?>
				<img src="<?php echo esc_url( $contents['section_image'] ); ?>" alt="">
			<?php endif; ?>
		</figure>

		<div class="feature-list style-2 columns-3 align-left icon_left">
			<?php $delay = 100; ?>
			<?php foreach( $contents['items'] as $feature ) : ?>
				
				<div class="feature wow fadeInUp" data-wow-delay="<?php echo esc_attr( $delay ); ?>ms">
					<div class="feature__image">
						<i class="<?php echo esc_attr( $feature['media'] ); ?>"></i>
					</div>
					<div class="feature__content">
						<?php if ( ! empty( $feature['title'] ) ) : ?>
							<h2 class="feature__title"><?php echo ''.$feature['title']; ?></h2>
						<?php endif; ?>
						<div class="feature__desc">
							<?php if ( ! empty( $feature['content'] ) ) : ?>
								<?php echo ''.$feature['content']; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<?php $delay +=175; ?>
			<?php endforeach; ?>
		</div>
	</div>
</div>