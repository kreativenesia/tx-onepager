<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-comps multipress-comps--type-12 multipress-block-<?php echo esc_attr( $id ); ?>">

	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row">
			<div class="col-md-6 col-md-push-6">
				<div class="section-content">
					<?php if ( ! empty( $contents['content'] ) ) : ?>
						<p><?php echo ''.$contents['content']; ?></p>
					<?php endif; ?>
					<?php if ( ! empty( $contents['sub_content'] ) ) : ?>
						<p>
							<strong><?php echo ''.$contents['sub_content']; ?></strong>
						</p>
					<?php endif; ?>
					<?php if ( ! empty( $contents['button_text'] ) ) : ?>
						<?php $target1 ='';
							if ( ! empty( $contents['button_target'])){
								$target1 = 'target="_blank"';
							}				
						?>
						<a href="<?php echo esc_url( $contents['button_link'] ); ?>" class="button action button--<?php echo esc_attr( $styles['button_style'] ); ?>" <?php echo '' . $target1 ?>><?php echo ''.$contents['button_text']; ?></a>
					<?php endif; ?>

				</div>
				
			</div>
		</div>
	</div>
</div>