<?php

return array(
	'slug'    	=> 'multipress-landing-12',
	'name' 		=> esc_html__( 'Landing Squeeze 12', 'multipress' ),
	'groups'    => array( 'landing' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Content Section'
		),
			array(
				'name' 		=> 'content',
				'label' 	=> esc_html__( 'Content Text', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime illo cum dolores doloribus eaque praesentium quaerat ab quibusdam'
			),
			array(
				'name' 		=> 'sub_content',
				'label' 	=> esc_html__( 'Sub Content Text', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'USD 5000/Month'
			),
			array(
				'name' 		=> 'button_text',
				'label' 	=> esc_html__( 'Button Text', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Request a quote'
			),
			array(
				'name' 		=> 'button_link',
				'label' 	=> esc_html__( 'Button Link', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),
			array(
				'name' 		=> 'button_target',
				'type'		=> 'switch',
				'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
				'value' 	=> true
			),

	),

	'settings' 	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
				'value'		=> 'http://unsplash.it/1980/800?image=1081'
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'cover',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '' 
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Content Styling'
		),
			array(
				'name' 		=> 'content_text_color',
				'label' 	=> esc_html__( 'Content Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'content_text_font_size',
				'label' 	=> esc_html__( 'Content Text Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
			array(
				'name' 		=> 'sub_content_text_color',
				'label' 	=> esc_html__( 'Sub Content Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'sub_content_text_font_size',
				'label' 	=> esc_html__( 'Sub Content Text Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
			),
			array(
				'name' 		=> 'button_background',
				'label' 	=> esc_html__( 'Button Background', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'button_text_color',
				'label' 	=> esc_html__( 'Button Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'button_style',
				'label' 	=> esc_html__( 'Button Style', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'square',
				'options' 	=> array(
					'square' 	=> 'Square',
					'radius' 	=> 'Radius',
					'rounded' 	=> 'Rounded',
				)
			),
	),

);
