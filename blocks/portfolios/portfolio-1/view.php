<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

	$args 				= array(
		'post_type'				=> 'multipress-portfolio',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 8,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);

	if ( ! empty($contents['portfolio_cat'] ) ) {
		if  ( $contents['portfolio_cat'] != 'all' )    {
			$args['tax_query'] 		= array(
									array(
										'taxonomy' => 'portfolio_cat',
										'field'    => 'term_id',
										'terms'    => $contents['portfolio_cat'],
									),
								);
		}
	}

	$portfolios = new WP_Query( $args );
 ?>

<!-- Portfolio Style 1 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-portfolio multipress-portfolio--style-1 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) ) : ?>
			<header class="section-header">
				<a href="#" class="poca-prev"><i class="fa fa-angle-left"></i></a>
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section_title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<a href="#" class="poca-next"><i class="fa fa-angle-right"></i></a>
			</header>
		<?php endif; ?>

		<div class="list_portfolios  <?php _multipress_portfolio_animation_class(); ?>">
			<div class="portfolio-carousel">

				<?php if ( $portfolios->have_posts() ) : ?>
					<?php while ( $portfolios->have_posts() ) : $portfolios->the_post(); ?>
						
						<div class="portfolio card">
							<?php if ( has_post_thumbnail() ) : ?>
								<?php 
									$img_src 	= wp_get_attachment_image_src( get_post_thumbnail_id(), 'portfolio_thumbnail_square' );
									$width 		= ! empty( $img_src[1] ) ? $img_src[1] : '500'; 
									$height 	= ! empty( $img_src[2] ) ? $img_src[2] : '500'; 
									$alt 		= get_post_meta( get_post_thumbnail_id(), '_wp_attachment_image_alt', true );
								echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img class="mp-lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="'._multipress_resize( $img_src[0], 500, 500 ).'" width="'.$width.'" height="'.$height.'" alt="'.$alt.'"></span>';
							else : 
								echo '<span class="intrinsic-ratio" style="padding-bottom:100%"><img class="mp-lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="http://unsplash.it/500/500" width="500" height="500" alt="placeholder"></span>';
							endif; ?>

							<div class="card_details">
								<div class="card_details_content">
									<h3 class="card_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<div class="cats"><?php _multipress_portfolio_get_categories(); ?></div>
									<a href="<?php the_permalink(); ?>" class="button"><?php esc_html_e( 'View Project', 'multipress' ); ?></a>
								</div>
							</div>
						</div>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>

				<?php else: ?>
					
					<?php $counter = 1; ?>
					<?php while ( 4 >= $counter ) : ?>

						<div class="portfolio card">
							<img src="http://unsplash.it/500/500" alt="Portfolio Thumbnail">
							<div class="card_details">
								<div class="card_details_content">
									<h3 class="card_title"><a href="#">Designing Effective Database Systems</a></h3>
									<div class="cats"><a href="#" class="portfolio-category">Graphic Design</a></div>
									<a href="#" class="button">View Project</a>
								</div>
							</div>
						</div>

					<?php $counter ++; endwhile; ?>
					
				<?php endif; ?>

			</div>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".portfolio-carousel").each(function(){
			var carousel = jQuery(this).slick({
				slidesToShow: 4,
				arrows: false,
			});

			jQuery(".poca-prev").on("click", function(e){
				e.preventDefault();
				carousel.slick("slickPrev");
			});

			jQuery(".poca-next").on("click", function(e){
				e.preventDefault();
				carousel.slick("slickNext");
			});
		});
	</script>
<?php } ?>