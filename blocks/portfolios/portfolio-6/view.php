<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

	$args 				= array(
		'post_type'				=> 'multipress-portfolio',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 5,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);

	if ( ! empty($contents['portfolio_cat'] ) ) {
		if  ( $contents['portfolio_cat'] != 'all' )    {
			$args['tax_query'] 		= array(
									array(
										'taxonomy' => 'portfolio_cat',
										'field'    => 'term_id',
										'terms'    => $contents['portfolio_cat'],
									),
								);
		}
	}
	
	$portfolios = new WP_Query( $args );
 ?>

<!-- Portfolio Style 5 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-portfolio multipress-portfolio--style-6 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<?php if ( $portfolios->have_posts() ) : ?>

			<?php $counter = 1; ?>
			
			<?php while ( $portfolios->have_posts() ) : $portfolios->the_post(); ?>
				
				<?php if ( 1 == $counter ) : ?>

					<div class="portfolio featured">
						<figure class="portfolio__image">
							<?php if ( has_post_thumbnail() ) : ?>
								<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 1600, 990 ); ?>" alt="<?php the_title(); ?>">
							<?php else : ?>
								<img src="http://unsplash.it/1600/990" alt="<?php the_title(); ?>">
							<?php endif; ?>
							<div class="portfolio__detail">
								<h2 class="portfolio__title"><?php the_title(); ?></h2>
								<small class="portfolio__desc"><?php _multipress_portfolio_get_categories(); ?></small>
							</div>
						</figure>
					</div>
				
				<?php else : ?>

					<?php ob_start(); ?>
						
						<div class="portfolio">
							<figure class="portfolio__image">
								<?php if ( has_post_thumbnail() ) : ?>
									<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 400, 300 ); ?>" alt="<?php the_title(); ?>">
								<?php else : ?>
									<img src="http://unsplash.it/400/300" alt="<?php the_title(); ?>">
								<?php endif; ?>
								<div class="portfolio__detail">
									<h2 class="portfolio__title"><?php the_title(); ?></h2>
								<small class="portfolio__desc"><?php _multipress_portfolio_get_categories(); ?></small>
								</div>
							</figure>
						</div>

					<?php $portfolio_contents[] = ob_get_contents(); ?>
					<?php ob_end_clean(); ?>

				<?php endif; ?>

			<?php $counter++; endwhile; ?>
			<?php wp_reset_postdata(); ?>

		<?php endif; ?>

		<div class="row">
			<?php if ( ! empty( $portfolio_contents ) ) : ?>
				<?php foreach ( $portfolio_contents as $content ) : ?>
					<?php echo ''.$content; ?>			
				<?php endforeach ?>			
			<?php endif; ?>
		</div>

	</div>
</div>