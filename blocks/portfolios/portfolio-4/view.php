<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	
	$args 				= array(
		'post_type'				=> 'multipress-portfolio',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 10,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);

	if ( ! empty($contents['portfolio_cat'] ) ) {
		if  ( $contents['portfolio_cat'] != 'all' )    {
			$args['tax_query'] 		= array(
									array(
										'taxonomy' => 'portfolio_cat',
										'field'    => 'term_id',
										'terms'    => $contents['portfolio_cat'],
									),
								);
		}
	}
	
	$custom_query 			= get_posts( $args ); 
	$chunked_portfolios 	= array_chunk( $custom_query , 5 );
 ?>

<!-- Portfolio Style 4 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-portfolio multipress-portfolio--style-4 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) ) : ?>
			<header class="section-header"> 
				<a href="#" class="caPrev"><i class="fa fa-angle-left"></i></a>
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section_title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<a href="#" class="caNext"><i class="fa fa-angle-right"></i></a>
			</header>
		<?php endif; ?>
		
		<div class="portfolio-masonry-carousel">
			
			<?php $wrapper_count = 1; ?>
			<?php foreach ( $chunked_portfolios as $custom_portfolios ) : ?>
				
				<div class="portfolio-masonry">
					<div class="grid-sizer"></div>
					
					<?php if ( $wrapper_count % 2 == 0 ) : ?>

						<?php $counter = 1; ?>			
						<?php foreach( $custom_portfolios as $post ) : setup_postdata( $post ); ?>
							
							<?php 
								if ( 3 == $counter ) :
									$width 	= 800;
									$height = 800;
									$class 	= 'grid-l';
								else :
									$width 	= 400;
									$height = 400;
									$class 	= '';
								endif; 
							?>

							<div class="portfolio grid <?php echo esc_attr( $class ); ?>">
								<a href="<?php echo get_permalink( $post->ID ); ?>">
									<?php 
										$thumbnail_id 	= get_post_thumbnail_id( $post->ID ); 
										$img_src 		= wp_get_attachment_image_src( $thumbnail_id, 'full' );
										if ( ! empty( $thumbnail_id ) ) : ?>
											<?php 
												$alt 		= get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true );
												echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-lazy="'._multipress_resize( $img_src[0], $width, $height ).'" width="'.$width.'" height="'.$height.'" alt="'.$alt.'"></span>';
											 ?>
										<?php else :
											
											echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-lazy="http://unsplash.it/'.esc_attr( $width ).'/'.esc_attr( $height ).'?image='.esc_attr( $counter ).'" width="'.$width.'" height="'.$height.'" alt="Placeholder"></span>';
										endif;
									?>
								</a>
							</div>

						<?php $counter++; endforeach; ?>
						
					<?php else : ?>
						
						<?php $counter = 1; ?>
						<?php foreach( $custom_portfolios as $post ) : setup_postdata( $post ); ?>
							
							<?php 
								if ( 2 == $counter ) :
									$width 	= 800;
									$height = 800;
									$class 	= 'grid-l';
								else :
									$width 	= 400;
									$height = 400;
									$class 	= '';
								endif; 
							?>

							<div class="portfolio grid <?php echo esc_attr( $class ); ?>">
								<a href="<?php echo get_permalink( $post->ID ); ?>">
									<?php 
										$thumbnail_id 	= get_post_thumbnail_id( $post->ID ); 
										$img_src 		= wp_get_attachment_image_src( $thumbnail_id, 'full' );
										if ( ! empty( $thumbnail_id ) ) : ?>
											<?php 
												$alt 		= get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true );
												echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-lazy="'._multipress_resize( $img_src[0], $width, $height ).'" width="'.$width.'" height="'.$height.'" alt="'.$alt.'"></span>';
											 ?>
										<?php else :
											
											echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-lazy="http://unsplash.it/'.esc_attr( $width ).'/'.esc_attr( $height ).'?image='.esc_attr( $counter ).'" width="'.$width.'" height="'.$height.'" alt="Placeholder"></span>';
										endif;
									?>
								</a>
							</div>

						<?php $counter++; endforeach; ?>

					<?php endif; ?>

				</div>

			<?php $wrapper_count++; endforeach; ?>

		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(window).load(function(){
			jQuery(".multipress-portfolio .portfolio-masonry").isotope({
				layoutMode:"packery",
				itemSelector: ".grid",
			});

			jQuery(".multipress-portfolio--style-4").each(function(){
				var carousel = jQuery(this).find(".portfolio-masonry-carousel");
				var caNext = jQuery(this).find(".caNext");
				var caPrev = jQuery(this).find(".caPrev");

				carousel.slick({
					slidesToShow: 1,
					arrows: false,
				});

				caPrev.on("click", function(e){
					e.preventDefault();
					carousel.slick("slickPrev");
				});

				caNext.on("click", function(e){
					e.preventDefault();
					carousel.slick("slickNext");
				});

			});
			
		});
	</script>
<?php } ?>

