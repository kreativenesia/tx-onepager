<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	
	$args 				= array(
		'post_type'				=> 'multipress-portfolio',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 10,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);

	if ( ! empty($contents['portfolio_cat'] ) ) {
		if  ( $contents['portfolio_cat'] != 'all' )    {
			$args['tax_query'] 		= array(
									array(
										'taxonomy' => 'portfolio_cat',
										'field'    => 'term_id',
										'terms'    => $contents['portfolio_cat'],
									),
								);
		}
	}
	
	$portfolios = new WP_Query( $args );
 ?>

<!-- Portfolio Style 3 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-portfolio multipress-portfolio--style-3 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
				<?php if ( ! empty( $contents['post_link'] ) ): ?>
					<?php $target1 ='';
						if ( ! empty( $contents['button_target'])){
							$target1 = 'target="_blank"';
						}				
					?>
				<a href="<?php echo esc_url( $contents['post_link']); ?>" class="button" <?php echo '' . $target1 ?>><?php esc_html_e( 'All Portfolios', 'multipress' ); ?></a>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="portfolio-masonry">
			<div class="grid-sizer"></div>
			
			<?php $counter = 1; ?>
			<?php if ( $portfolios->have_posts() ) : ?>
				<?php while ( $portfolios->have_posts() ) : $portfolios->the_post(); ?>
					
					<?php 
						if ( 2 == $counter || 10 == $counter ) :
							$width 		= 800;
							$height 	= 380;
							$grid_class = 'grid-w';
						elseif ( 5 == $counter ) :
							$width 		= 400;
							$height 	= 845;
							$grid_class = 'grid-t';
						elseif ( 4 == $counter ) :
							$width 		= 800;
							$height 	= 800;
							$grid_class = 'grid-l';
						else :
							$width 		= 400;
							$height 	= 400;
							$grid_class = '';
						endif; 
					?>
					<div class="portfolio grid <?php echo esc_attr( $grid_class ); ?>">
						<a href="<?php the_permalink(); ?>">
							<?php if ( has_post_thumbnail() ) : ?>
								<?php 
									$img_src 	= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'portfolio_thumbnail_rectangle' );
									$alt = get_post_meta( get_post_thumbnail_id( get_the_ID() ), '_wp_attachment_image_alt', true );
									echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img class="mp-lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="'._multipress_resize( $img_src[0], $width, $height ).'" width="'.$width.'" height="'.$height.'" alt="'.$alt.'"></span>';
							else :
								echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img class="mp-lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="http://unsplash.it/'.esc_attr( $width ).'/'.esc_attr( $height ).'" width="'.$width.'" height="'.$height.'" alt="placeholder"></span>';
							endif; ?>
						</a>
					</div>

				<?php $counter++; endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php else: ?>
				
				<?php $counter = 1; ?>
				<?php while ( 10 >= $counter ) : ?>
					
					<?php 
						if ( 2 == $counter || 10 == $counter ) :
							$width 		= 800;
							$height 	= 380;
							$grid_class = 'grid-w';
						elseif ( 5 == $counter ) :
							$width 		= 400;
							$height 	= 845;
							$grid_class = 'grid-t';
						elseif ( 4 == $counter ) :
							$width 		= 800;
							$height 	= 800;
							$grid_class = 'grid-l';
						else :
							$width 		= 400;
							$height 	= 400;
							$grid_class = '';
						endif; 
					?>

					<div class="portfolio grid <?php echo esc_attr( $grid_class ); ?>">
						<a href="#"><img src="http://unsplash.it/<?php echo esc_attr( $width ); ?>/<?php echo esc_attr( $height ); ?>" alt=""></a>
					</div>

				<?php $counter ++; endwhile; ?>
				
			<?php endif; ?>

		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(window).load(function(){
			jQuery(".multipress-portfolio .portfolio-masonry").isotope({
				layoutMode:"packery",
				itemSelector: ".grid",
			});
		});
	</script>
<?php } ?>