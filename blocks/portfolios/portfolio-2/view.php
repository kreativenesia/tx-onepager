<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

	$args 				= array(
		'post_type'				=> 'multipress-portfolio',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 6,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);

	if ( ! empty($contents['portfolio_cat'] ) ) {
		if  ( $contents['portfolio_cat'] != 'all' )    {
			$args['tax_query'] 		= array(
									array(
										'taxonomy' => 'portfolio_cat',
										'field'    => 'term_id',
										'terms'    => $contents['portfolio_cat'],
									),
								);
		}
	}
	
	$custom_query 			= get_posts( $args ); 
	$chunked_portfolios 	= array_chunk( $custom_query , 3 );
 ?>

<!-- Portfolio Style 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-portfolio multipress-portfolio--style-2 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle wow fadeInUp'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>
		
		<?php foreach ( $chunked_portfolios as $custom_portfolios ) : ?>
			
			<div class="row">
				
				<?php $delay = 150; $counter = 1; ?>			
				<?php foreach( $custom_portfolios as $post ) : setup_postdata( $post ); ?>
					
					<div class="col-md-4">
						<div class="portfolio wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
							<figure class="portfolio__image">
								<a href="<?php echo get_permalink( $post->ID ); ?>">
									<?php 
										if ( has_post_thumbnail( $post->ID ) ) : 
											$img_src 	= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'portfolio_thumbnail_rectangle' );
											$width 		= ! empty( $img_src[1] ) ? $img_src[1] : '500'; 
											$height 	= ! empty( $img_src[2] ) ? $img_src[2] : '500'; 
											$alt 		= get_post_meta( get_post_thumbnail_id( $post->ID ), '_wp_attachment_image_alt', true );
											echo '<span class="intrinsic-ratio" style="padding-bottom:'.($height/$width)*100 .'%"><img class="mp-lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="'._multipress_resize( $img_src[0], 500, 300 ).'" width="'.$width.'" height="'.$height.'" alt="'.$alt.'"></span>';
										else :
											echo '<span class="intrinsic-ratio" style="padding-bottom:60%"><img class="mp-lazyload" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC" data-original="http://unsplash.it/500/300?image='.esc_attr( $counter ).'" width="500" height="300" alt="placeholder"></span>';
										endif; 
									?>
								</a>
							</figure>
							<div class="portfolio__detail">
								<h3 class="portfolio__title"><a href="<?php echo get_permalink( $post->ID ); ?>"><?php echo get_the_title( $post->ID ); ?></a></h3>
								<div class="portfolio__cat"><?php _multipress_portfolio_get_categories_alt( $post->ID ); ?></div>
							</div>
						</div>
					</div>

				<?php $delay +=175; $counter++; endforeach; ?>

			</div>

		<?php endforeach; ?>

	</div>
</div>