<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	$data_date	 		= ! empty( $contents['countdown_date'] ) ? 'data-countdown-date="'.$contents['countdown_date'].'"': '';
	$date 				= ! empty( $contents['countdown_date'] ) ? $contents['countdown_date'] : '28/12/2017';
	$the_date 			= str_replace( '/', '-', $date );
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-countdown multipress-countdown--type-3 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="row">
			<div class="col-md-4">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<div class="section-header">
						<?php if ( ! empty( $contents['section_title'] ) ) : ?>
							<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
							<small class="section-subtitle"><?php echo date( 'd F Y', strtotime( $the_date ) ); ?></small>
						<?php endif; ?>
					</div>
					<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
						<p><?php echo ''.$contents['section_subtitle']; ?></p>
					<?php endif; ?>
				<?php endif; ?>
			</div>

			<div class="col-md-8">
				<div class="mp-countdown mp-countdown--<?php echo esc_attr( $contents['countdown_style'] ); ?>" <?php echo ''.$data_date; ?>>
					<div class="mp-countdown__timer days">
						<div class="mp-countdown__value">18</div>
						<div class="mp-countdown__label">Days</div>
					</div>
					<div class="mp-countdown__timer hours">
						<div class="mp-countdown__value">23</div>
						<div class="mp-countdown__label">Hours</div>
					</div>
					<div class="mp-countdown__timer minutes">
						<div class="mp-countdown__value">45</div>
						<div class="mp-countdown__label">Minutes</div>
					</div>
					<div class="mp-countdown__timer seconds">
						<div class="mp-countdown__value">13</div>
						<div class="mp-countdown__label">Seconds</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>