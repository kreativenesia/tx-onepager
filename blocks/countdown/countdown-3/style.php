#multipress-block-<?php echo esc_attr( $id ); ?> {
	<?php if ( ! empty( $styles['bg_image'] ) ) : ?>
		<?php _multipress_print_builder_css( $styles['bg_image'], 		'background-image:url('.$styles['bg_image'].' )' ); ?>
		<?php _multipress_print_builder_css( $styles['bg_size'],  		'background-size:'. esc_attr( $styles['bg_size'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_repeat'],		'background-repeat:'. esc_attr( $styles['bg_repeat'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_position'],		'background-position:'. esc_attr( $styles['bg_position'] ) ); ?>
	<?php endif; ?>
	<?php _multipress_print_builder_css( $styles['bg_color'],		'background-color:'. $styles['bg_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['padding_top'],	'padding-top:'. $styles['padding_top'].'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['padding_bottom'],	'padding-bottom:'. $styles['padding_bottom'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_top'],		'margin-top:'. $styles['margin_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_bottom'],	'margin-bottom:'. $styles['margin_bottom'] . 'px;' ); ?>
}

#multipress-block-<?php echo esc_attr( $id ); ?> .background-overlay {
	<?php _multipress_print_builder_css( $styles['bg_overlay'],		'background-color:'. $styles['bg_overlay'] ); ?>
}

#multipress-block-<?php echo esc_attr( $id ); ?> .section-header h2 {
	<?php _multipress_print_builder_css( $styles['section_title_color'],		'color:'. $styles['section_title_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['section_title_font_size'],	'font-size:'. $styles['section_title_font_size'] . 'px;' ); ?>
}

#multipress-block-<?php echo esc_attr( $id ); ?> .section-header .section-subtitle {
	<?php _multipress_print_builder_css( $styles['section_subtitle_color'],		'color:'. $styles['section_subtitle_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['section_subtitle_font_size'],	'font-size:'. $styles['section_subtitle_font_size'] . 'px;' ); ?>
}

#multipress-block-<?php echo esc_attr( $id ); ?> p {
	<?php _multipress_print_builder_css( $styles['section_content_color'],		'color:'. $styles['section_content_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['section_content_font_size'],		'font-size:'. $styles['section_content_font_size'] . 'px;' ); ?>
}

#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown__label{
	<?php _multipress_print_builder_css( $styles['labels_color'],		'color:'. $styles['labels_color'] ); ?>
}

#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--fill .mp-countdown__timer.days .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['days_background_color'],		'background-color:'. $styles['days_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['days_color'],		'color:'. $styles['days_color'] ); ?>
}
#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--fill .mp-countdown__timer.hours .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['hours_background_color'],		'background-color:'. $styles['hours_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['hours_color'],		'color:'. $styles['hours_color'] ); ?>
}
#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--fill .mp-countdown__timer.minutes .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['minutes_background_color'],		'background-color:'. $styles['minutes_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['minutes_color'],		'color:'. $styles['minutes_color'] ); ?>
}
#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--fill .mp-countdown__timer.seconds .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['seconds_background_color'],		'background-color:'. $styles['seconds_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['seconds_color'],		'color:'. $styles['seconds_color'] ); ?>
}

#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--outline .mp-countdown__timer.days .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['days_background_color'],		'border-color:'. $styles['days_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['days_color'],		'color:'. $styles['days_color'] ); ?>
}
#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--outline .mp-countdown__timer.hours .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['hours_background_color'],		'border-color:'. $styles['hours_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['hours_color'],		'color:'. $styles['hours_color'] ); ?>
}
#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--outline .mp-countdown__timer.minutes .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['minutes_background_color'],		'border-color:'. $styles['minutes_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['minutes_color'],		'color:'. $styles['minutes_color'] ); ?>
}
#multipress-block-<?php echo esc_attr( $id ); ?> .mp-countdown--outline .mp-countdown__timer.seconds .mp-countdown__value{
	<?php _multipress_print_builder_css( $styles['seconds_background_color'],		'border-color:'. $styles['seconds_background_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['seconds_color'],		'color:'. $styles['seconds_color'] ); ?>
}