<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	$data_date	 		= ! empty( $contents['countdown_date'] )  ? "data-countdown-date='{$contents['countdown_date']}'": '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-countdown multipress-countdown--type-4 <?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) ) : ?>
			<div class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class="section-subtitle"><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="mp-countdown mp-countdown--<?php echo esc_attr( $contents['countdown_style'] ); ?>  mp-countdown--square" <?php echo ''.$data_date; ?>>
			<div class="mp-countdown__timer days">
				<div class="mp-countdown__value">18</div>
				<div class="mp-countdown__label">Days</div>
			</div>
			<div class="mp-countdown__timer hours">
				<div class="mp-countdown__value">23</div>
				<div class="mp-countdown__label">Hours</div>
			</div>
			<div class="mp-countdown__timer minutes">
				<div class="mp-countdown__value">45</div>
				<div class="mp-countdown__label">Minutes</div>
			</div>
			<div class="mp-countdown__timer seconds">
				<div class="mp-countdown__value">13</div>
				<div class="mp-countdown__label">Seconds</div>
			</div>
		</div>

		<div class="subscribe-form">
			<?php if ( ! empty( $contents['contact_form'] ) ) : ?>
				<?php echo do_shortcode( '[contact-form-7 id="'.$contents['contact_form'].'"]' ); ?>
			<?php else : ?>
				<div class="field-control">
					<label for="">Name</label>
					<div class="control">
						<input type="text" placeholder="Your name">
					</div>
				</div>
				<div class="field-control">
					<label for="">Email</label>
					<div class="control">
						<input type="text" placeholder="Your email">
					</div>
				</div>
				<div class="field-control">
					<div class="control">
						<input type="submit" value="Try Now" class="button big">
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>