<?php

return array(
	'slug'    	=> 'multipress-countdown-1',
	'name' 		=> esc_html__( 'Countdown Style 1', 'multipress' ),
	'groups'    => array( 'countdown' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Countdown <br>to New Era <br>2016'
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'Lorem ipsum dolor sit amet, consectetur <br> adipisicing elit. Laudantium sapiente <br> magni nostrum hic.'
			),
			array(
				'name' 		=> 'button_text',
				'label' 	=> esc_html__( 'Button Text', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'More'
			),
			array(
				'name' 		=> 'button_link',
				'label' 	=> esc_html__( 'Button Link', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),
			array(
				'name' 		=> 'button_target',
				'type'		=> 'switch',
				'label'		=> esc_html__( 'Button Link Open New Tabs', 'multipress' ),
				'value' 	=> true
			),

		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Content'
		),
			array(
				'name' 		=> 'countdown_style',
				'label' 	=> esc_html__( 'Countdown Style', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'fill',
				'options' 	=> array(
					'fill' 		=> 'Fill',
					'outline' 	=> 'Outline',
				)
			),
			array(
				'name' 		=> 'countdown_date',
				'label' 	=> esc_html__( 'Countdown Date (date/month/year)', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '28/12/2017'
			),
	),

	'settings' 	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		), 
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#fff'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
				'value'		=> ONEPAGER_URL . '/assets/images/bg-gradient.jpg'
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'cover',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '' 
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'section_subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'days_background_color',
				'label' 	=> esc_html__( 'Days Background / Border Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#f5b711'
			),
			array(
				'name' 		=> 'days_color',
				'label' 	=> esc_html__( 'Days Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#ffffff'
			),
			array(
				'name' 		=> 'hours_background_color',
				'label' 	=> esc_html__( 'Hours Background / Border Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#11f5c7'
			),
			array(
				'name' 		=> 'hours_color',
				'label' 	=> esc_html__( 'Hours Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#ffffff'
			),
			array(
				'name' 		=> 'minutes_background_color',
				'label' 	=> esc_html__( 'Minutes Background / Border Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#9711f5'
			),
			array(
				'name' 		=> 'minutes_color',
				'label' 	=> esc_html__( 'Minutes Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#ffffff'
			),
			array(
				'name' 		=> 'seconds_background_color',
				'label' 	=> esc_html__( 'Seconds Background / Border Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#f54c11'
			),
			array(
				'name' 		=> 'seconds_color',
				'label' 	=> esc_html__( 'Seconds Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#9711f5'
			),
			array(
				'name' 		=> 'labels_color',
				'label' 	=> esc_html__( 'Labels Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#ffffff'
			),
			
	),

);
