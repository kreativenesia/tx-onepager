<?php
	$container_size	 			= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
 ?>

<!-- Footer Block 3 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-footer multipress-footer--style-3 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php get_template_part( 'social', 'icons' ); ?>
		
		<?php if ( ! empty( $contents['footer_menu'] ) ) :
			$menu_args = array(
				'menu'  			=> $contents['footer_menu'],
				'container'       	=> 'nav',
				'container_class' 	=> 'footer-navigation inline_nav',
				'fallback_cb'		=> '_multipress_get_footer_dummy_menus'
			);
			wp_nav_menu( $menu_args );
		endif; ?>
	</div>
</div>

