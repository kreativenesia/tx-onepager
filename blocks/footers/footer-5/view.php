<?php
	$container_size	 			= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
 ?>

<!-- Footer Block 5-->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-footer multipress-footer--style-5 multipress-block-<?php echo esc_attr( $id ); ?>">
	<div class="top-section">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<div class="row">
				<?php if ( ! empty( $contents['sidebar_one'] ) ) : ?>
					<div class="col-md-4">
						<div class="widget-area">
							<?php dynamic_sidebar( $contents['sidebar_one'] ); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( ! empty( $contents['sidebar_two'] ) ) : ?>
					<div class="col-md-2">
						<div class="widget-area">
							<?php dynamic_sidebar( $contents['sidebar_two'] ); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( ! empty( $contents['sidebar_three'] ) ) : ?>
					<div class="col-md-2">
						<div class="widget-area">
							<?php dynamic_sidebar( $contents['sidebar_three'] ); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( ! empty( $contents['sidebar_four'] ) ) : ?>
					<div class="col-md-2">
						<div class="widget-area">
							<?php dynamic_sidebar( $contents['sidebar_four'] ); ?>
						</div>
					</div>
				<?php endif; ?>
				<?php if ( ! empty( $contents['sidebar_five'] ) ) : ?>
					<div class="col-md-2">
						<div class="widget-area">
							<?php dynamic_sidebar( $contents['sidebar_five'] ); ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	
	<div class="bottom-section">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<?php if ( ! empty( $contents['footer_text'] ) ) : ?>
				<div class="credit">
					<?php echo ''. $contents['footer_text'] ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

