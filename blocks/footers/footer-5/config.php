<?php

return array(
	'slug' 		=> 'multipress-footer-5', 
	'name' 		=> esc_html__( 'Multipress  footer 5', 'multipress' ),
	'groups' 	=> array( 'footers' ),
	'contents' 	=> array(
		array(
			'type' 	=> 'divider',
			'label' => 'Widgets'
		),
			array(
				'name' 		=> 'sidebar_one',
				'type'		=> 'select',
				'label'		=> esc_html__( 'Sidebar One', 'multipress' ),
				'value' 	=> 'footer-one',
				'options'	=> _multipress_get_all_sidebars(),
			),
			array(
				'name' 		=> 'sidebar_two',
				'type'		=> 'select',
				'label'		=> esc_html__( 'Sidebar Two', 'multipress' ),
				'value' 	=> 'footer-two',
				'options'	=> _multipress_get_all_sidebars(),
			),
			array(
				'name' 		=> 'sidebar_three',
				'type'		=> 'select',
				'label'		=> esc_html__( 'Sidebar Three', 'multipress' ),
				'value' 	=> 'footer-three',
				'options'	=> _multipress_get_all_sidebars(),
			),
			array(
				'name' 		=> 'sidebar_four',
				'type'		=> 'select',
				'label'		=> esc_html__( 'Sidebar Four', 'multipress' ),
				'value' 	=> 'footer-four',
				'options'	=> _multipress_get_all_sidebars(),
			),
			array(
				'name' 		=> 'sidebar_five',
				'type'		=> 'select',
				'label'		=> esc_html__( 'Sidebar Five', 'multipress' ),
				'value' 	=> 'footer-five',
				'options'	=> _multipress_get_all_sidebars(),
			),
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name' 			=>'footer_text', 
				'label' 		=> esc_html__( 'Footer Text', 'multipress' ),
				'type' 			=>'textarea', 
				'value' 		=> 'Copyright &copy; 2016 <a class="site-link" href="http://example.com/" title="Multipress" rel="home"><span>Multipress</span></a>.', 
			),
			
	),
	
	'settings'	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
	),

	'styles' 	=> array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#ffffff'
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),

			array(
				'name' 		=> 'footer_color',
				'label' 	=> esc_html__( 'Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'     => '#666666'
			),

			array(
				'name' 		=> 'footer_link_color',
				'label' 	=> esc_html__( 'Link Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'     => '#666666'
			),

			array(
				'name' 		=> 'footer_link_color_hover',
				'label' 	=> esc_html__( 'Link Hover Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'     => '#333333'
			),

			array(
				'name' 		=> 'footer_widget_color',
				'label' 	=> esc_html__( 'Widget Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'     => '#333333'
			),

			array(
				'name' 		=> 'separator_color',
				'label' 	=> esc_html__( 'Separator Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
	)
);