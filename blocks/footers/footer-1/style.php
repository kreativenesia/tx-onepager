.multipress-footer.multipress-block-<?php echo esc_attr( $id ); ?> {
	<?php if ( ! empty( $styles['bg_image'] ) ) : ?>
		<?php _multipress_print_builder_css( $styles['bg_image'], 		'background-image:url('.$styles['bg_image'].' )' ); ?>
		<?php _multipress_print_builder_css( $styles['bg_size'],  		'background-size:'. esc_attr( $styles['bg_size'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_repeat'],		'background-repeat:'. esc_attr( $styles['bg_repeat'] ) ); ?>
		<?php _multipress_print_builder_css( $styles['bg_position'],		'background-position:'. esc_attr( $styles['bg_position'] ) ); ?>
	<?php endif; ?>
	<?php _multipress_print_builder_css( $styles['bg_color'],		'background-color:'. $styles['bg_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['padding_top'],	'padding-top:'. $styles['padding_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['padding_bottom'],	'padding-bottom:'. $styles['padding_bottom'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_top'],		'margin-top:'. $styles['margin_top'] . 'px;' ); ?>
	<?php _multipress_print_builder_css( $styles['margin_bottom'],	'margin-bottom:'. $styles['margin_bottom'] . 'px;' ); ?>
}

.multipress-footer.multipress-block-<?php echo esc_attr($id); ?> .top_footer{
	<?php _multipress_print_builder_css( $styles['footer_color'],	'color:'. $styles['footer_color'] ); ?>
}
.multipress-footer.multipress-block-<?php echo esc_attr($id); ?> .top_footer a{
	<?php _multipress_print_builder_css( $styles['footer_link_color'],	'color:'. $styles['footer_link_color'] ); ?>	
}
.multipress-footer.multipress-block-<?php echo esc_attr($id); ?> .top_footer a:hover{
	<?php _multipress_print_builder_css( $styles['footer_link_color_hover'],	'color:'. $styles['footer_link_color_hover'] ); ?>
}
.multipress-footer.multipress-block-<?php echo esc_attr($id); ?> .top_footer .widget .widget-title{
	<?php _multipress_print_builder_css( $styles['footer_widget_color'],	'color:'. $styles['footer_widget_color'] ); ?>
}
.multipress-footer.multipress-block-<?php echo esc_attr($id); ?> .bottom_footer{
	<?php _multipress_print_builder_css( $styles['secondary_bg_color'],	'background-color:'. $styles['secondary_bg_color'] ); ?>
	<?php _multipress_print_builder_css( $styles['secondary_color'],	'color:'. $styles['secondary_color'] ); ?>
}
.multipress-footer.multipress-block-<?php echo esc_attr($id); ?> .bottom_footer a {
	<?php _multipress_print_builder_css( $styles['secondary_color'],	'color:'. $styles['secondary_color'] ); ?>	
}