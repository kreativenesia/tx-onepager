<?php
	$container_size	 			= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
 ?>

<!-- Footer Block 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-footer multipress-footer--style-2 multipress-block-<?php echo esc_attr( $id ); ?>">
	
	<div class="bottom_footer">
		<div class="<?php echo esc_attr( $container_size ); ?>">
			<div class="row">
				<div class="copy col-md-6">
					<div class="footer-text">
						<?php if ( ! empty( $contents['footer_text'] ) ) : ?>
							<div class="credit">
								<?php echo ''. $contents['footer_text'] ?>
							</div>
						<?php endif; ?>
						<?php get_template_part( 'social', 'icons' ); ?> 
					</div>
				</div>
				
				<div class="footer_menu col-md-6">
					<?php if ( ! empty( $contents['footer_menu'] ) ) :
						$menu_args = array(
							'menu'  			=> $contents['footer_menu'],
							'container'       	=> 'nav',
							'container_class' 	=> 'inline_nav',
							'fallback_cb'		=> '_multipress_get_footer_dummy_menus'
						);
						wp_nav_menu( $menu_args );
					endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

