<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div class="multipress-section multipress-status multipress-status--style-1 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) ) : ?>
			<div class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<small class="section-subtitle"><?php echo ''.$contents['section_subtitle']; ?></small>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<div class="status-grid columns-<?php echo esc_attr( $settings['columns'] ); ?>">
			
			<?php foreach( $contents['items'] as $feature ) : ?>
				
				<div class="status">
					<?php $icon_color = ! empty( $feature['icon_color'] ) ? 'style="color:'.$feature['icon_color'].'"' : '' ; ?>
					<div class="status__icon"><i class="<?php echo esc_attr( $feature['media'] ); ?>" <?php echo ''.$icon_color; ?>></i></div>
					<div class="status__title">
						<h3 class="status__num" data-num=<?php echo esc_attr( $feature['number'] ); ?>>0</h3>
						<small class="status__label"><?php echo esc_attr( $feature['number_label'] ); ?></small>
					</div>
					<div class="status__desc">
						<p><?php echo ''.$feature['description']; ?></p>
					</div>
				</div>

			<?php endforeach; ?>

		</div>
	</div>
</div>