<?php

return array(
	'slug'    	=> 'multipress-status-3',
	'name' 		=> esc_html__( 'Status Style 3', 'multipress' ),
	'groups'    => array( 'status' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Our Glorious Stats'
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'You have the design you have to code'
			),
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=>'items',
				'type'		=>'repeater',
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-comments' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#c993fb'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '6000' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Comments' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-ruler-pencil' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#c993fb'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '9898' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Designs' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-thumb-up' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#c993fb'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '56756' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Likes' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-shopping-cart-full' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#c993fb'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '6000' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Shop' 
						),
					),
					
				)
			)
	),

	'settings' 	=> array(
		array(
			'name' 		=> 'section_alignment',
			'label' 	=> esc_html__( 'Section Heading Alignment', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'center',
			'options' 	=> array(
				'left' 		=> 'Left',
				'center' 	=> 'Center',
				'right' 	=> 'Right',
			)
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name'     => 'columns',
			'label'    => esc_html__( 'Columns', 'multipress' ),
			'type'     => 'select',
			'value'    => '4',
			'options'  => array(
				'2'   => '2',
				'3'   => '3',
				'4'   => '4',
				'5'   => '5'
			),
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		), 
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#25282f'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '' 
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#fafafa'
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'section_subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> 'rgba(255,255,255,.6)'
			),
			array(
				'name' 		=> 'section_subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'number_color',
				'label' 	=> esc_html__( 'Number Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#6f647a'
			),
			array(
				'name' 		=> 'number_font_size',
				'label' 	=> esc_html__( 'Number Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'label_color',
				'label' 	=> esc_html__( 'Label Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> '#b7b7b7'
			),
			array(
				'name' 		=> 'label_font_size',
				'label' 	=> esc_html__( 'Label Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'separator_color',
				'label' 	=> esc_html__( 'Separator Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value'		=> 'rgba(255,255,255,.1)'
			),

	),

);
