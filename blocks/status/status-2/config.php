<?php

return array(
	'slug'    	=> 'multipress-status-2',
	'name' 		=> esc_html__( 'Status Style 2', 'multipress' ),
	'groups'    => array( 'status' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Inception By Numbers'
			),
			array(
				'name' 		=> 'section_subtitle',
				'label' 	=> esc_html__( 'Section Subtitle', 'multipress' ),
				'type' 		=> 'editor',
				'value' 	=> 'You have the design you have to code'
			),
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=>'items',
				'type'		=>'repeater',
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-comments' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#20c5ee'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '8738' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Comments' 
						),
						array(
							'name' 		=> 'description',
							'type' 		=> 'editor', 
							'label' 	=> esc_html__( 'Description', 'multipress' ), 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, inventore.' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-package' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#3320ee'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '9871' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Boxed' 
						),
						array(
							'name' 		=> 'description',
							'type' 		=> 'editor', 
							'label' 	=> esc_html__( 'Description', 'multipress' ), 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, inventore.' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-alarm-clock' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#f164d8'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '8769' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Hours' 
						),
						array(
							'name' 		=> 'description',
							'type' 		=> 'editor', 
							'label' 	=> esc_html__( 'Description', 'multipress' ), 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, inventore.' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-layers'
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#ee203d'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '8651' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Stacks' 
						),
						array(
							'name' 		=> 'description',
							'type' 		=> 'editor', 
							'label' 	=> esc_html__( 'Description', 'multipress' ), 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, inventore.' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-server' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#ee7720'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '344' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Servers' 
						),
						array(
							'name' 		=> 'description',
							'type' 		=> 'editor', 
							'label' 	=> esc_html__( 'Description', 'multipress' ), 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, inventore.' 
						),
					),
					array(
						array(
							'name' 		=> 'media', 
							'type' 		=> 'icon', 
							'size' 		=> '', 
							'value' 	=> 'ti-layout-width-default' 
						),
						array( 
							'name' 		=> 'icon_color', 
							'type' 		=> 'color', 
							'label' 	=> esc_html__( 'Icon Color', 'multipress' ),
							'value' 	=> '#2eee20'
						),
						array(
							'name' 		=> 'number',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number', 'multipress' ), 
							'value' 	=> '53674' 
						),
						array(
							'name' 		=> 'number_label',
							'type' 		=> 'text', 
							'label' 	=> esc_html__( 'Number Label', 'multipress' ), 
							'value' 	=> 'Documents' 
						),
						array(
							'name' 		=> 'description',
							'type' 		=> 'editor', 
							'label' 	=> esc_html__( 'Description', 'multipress' ), 
							'value' 	=> 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat, inventore.' 
						),
					),
				)
			)
	),

	'settings' 	=> array(
		array(
			'name' 		=> 'section_alignment',
			'label' 	=> esc_html__( 'Section Heading Alignment', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'center',
			'options' 	=> array(
				'left' 		=> 'Left',
				'center' 	=> 'Center',
				'right' 	=> 'Right',
			)
		),
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name'     => 'columns',
			'label'    => esc_html__( 'Columns', 'multipress' ),
			'type'     => 'select',
			'value'    => '3',
			'options'  => array(
				'2'   => '2',
				'3'   => '3',
				'4'   => '4',
				'5'   => '5'
			),
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		), 
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#fff'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '' 
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'section_subtitle_color',
				'label' 	=> esc_html__( 'Subtitle Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_subtitle_font_size',
				'label' 	=> esc_html__( 'Subtitle Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'number_color',
				'label' 	=> esc_html__( 'Number Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'number_font_size',
				'label' 	=> esc_html__( 'Number Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'label_color',
				'label' 	=> esc_html__( 'Label Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'label_font_size',
				'label' 	=> esc_html__( 'Label Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
 			array(
				'name' 		=> 'desc_color',
				'label' 	=> esc_html__( 'Description Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'desc_font_size',
				'label' 	=> esc_html__( 'Description Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
 			

	),

);
