<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	
	$args = array(
		'post_type'				=> 'testimonials',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 6,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$testimonials = new WP_Query( $args );
 ?>

<!-- Testimonial Style 3 -->
<div class="multipress-section multipress-testimonial multipress-testimonial--style-3 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<p class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></p>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<div class="mp-testimonial-slider">
			<ul class="slides">

				<?php if ( $testimonials->have_posts() ) : ?>
					<?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
						
						<li class="mp-testimonial" data-thumb="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 50, 50 ); ?>">
							<blockquote>
								<p><?php echo _multipress_testimonials_get_content(); ?></p>
							</blockquote>

							<figure class="mp-testimonial__image">
								<?php if ( has_post_thumbnail() ) : ?>
									<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 100, 100 ); ?>" alt="<?php esc_html_e( 'Testimonial Image', 'multipress' ); ?>">
								<?php else : ?>
									<img src="http://unsplash.it/100" alt="<?php esc_html_e( 'Testimonial Image', 'multipress' ); ?>">
								<?php endif; ?>
								<figcaption>
									<h3 class="mp-testimonial__name"><?php the_title(); ?></h3>
									<small class="mp-testimonial__desc"><?php echo _multipress_testimonials_get_position(); ?></small>
								</figcaption>
							</figure>

						</li>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>

				<?php else: ?>
					
					<?php $counter 	= 1; ?>
					<?php while ( 4 >= $counter ) : ?>

						<li class="mp-testimonial" data-thumb="http://unsplash.it/50">
							<blockquote>
								<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis explicabo error ab tempore. Accusamus exercitationem dolor obcaecati, hic, consectetur pariatur consequatur possimus mollitia voluptate esse error maxime earum perspiciatis aspernatur."</p>
							</blockquote>

							<figure class="mp-testimonial__image">
								<img src="http://unsplash.it/100" alt="">
								<figcaption>
									<h3 class="mp-testimonial__name">Jonathan Doe</h3>
									<small class="mp-testimonial__desc">CEO Awesome Company</small>
								</figcaption>
							</figure>

						</li>
						
					<?php $counter++; endwhile; ?>
				<?php endif; ?>
				
			</ul>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-testimonial--style-3 .mp-testimonial-slider").flexslider({
			controlNav: "thumbnails",
			prevText: '<i class="fa fa-angle-left"></i>',
			nextText: '<i class="fa fa-angle-right"></i>',
		});
	</script>
<?php } ?>