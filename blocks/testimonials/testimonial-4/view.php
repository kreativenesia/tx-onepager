<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';

	$args = array(
		'post_type'				=> 'testimonials',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 6,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$testimonials = new WP_Query( $args );
 ?>

<!-- Testimonial Style 4 -->
<div id="multipress-element-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-testimonial multipress-testimonial--style-4 multipress-element-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>
		
		<div class="mp-testimonial-slider">
			<ul class="slides">

				<?php if ( $testimonials->have_posts() ) : ?>
					<?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
						
						<li class="mp-testimonial" data-thumb="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 100, 100 ); ?>">
							<blockquote>
								<p><?php echo _multipress_testimonials_get_content(); ?></p>
							</blockquote>
							<cite>
								<strong class="mp-testimonial__name h3"><?php the_title(); ?></strong>
								<span class="mp-testimonial__desc"><?php echo _multipress_testimonials_get_position(); ?></span>
							</cite>
						</li>

					<?php endwhile; ?>
					<?php wp_reset_postdata(); ?>

				<?php else: ?>
					
					<?php $testimonials_dummies = _multipress_testimonials_dummies(); ?>
					<?php foreach ( $testimonials_dummies as $testimony ) : ?>
						
						<li class="mp-testimonial" data-thumb="<?php echo esc_url( $testimony['thumbnail'] ); ?>/100/100?image=128">
							<blockquote>
								<?php echo ''.$testimony['content']; ?>
							</blockquote>
							<cite>
								<strong class="mp-testimonial__name h3"><?php echo esc_attr( $testimony['title'] ); ?></strong>
								<span class="mp-testimonial__desc"><?php echo esc_attr( $testimony['position'] ); ?></span>
							</cite>
						</li>
						
					<?php endforeach; ?>
				<?php endif; ?>

				
			</ul>
		</div>
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-testimonial--style-4 .mp-testimonial-slider").flexslider({
			controlNav: "thumbnails",
			prevText: '<i class="fa fa-angle-left"></i>',
			nextText: '<i class="fa fa-angle-right"></i>',
		});
	</script>
<?php } ?>

