<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	
	$args = array(
		'post_type'				=> 'testimonials',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 6,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$testimonials = new WP_Query( $args );
 ?>

<!-- Testimonial Style 6 -->
<div id="multipress-element-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-testimonial multipress-testimonial--style-6 multipress-element-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>

		<?php if ( $testimonials->have_posts() ) : ?>
			
			<div class="mp-testimonial-slider">

				<?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
					
					<div class="mp-testimonial">
						<blockquote>
							<p><?php echo _multipress_testimonials_get_content(); ?></p>
						</blockquote>
						<?php 
							ob_start();
								?>
									<div class="cite">
										<figure class="mp-testimonial__image">
											<?php if ( has_post_thumbnail() ) : ?>
												<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 100, 100, true ); ?>" alt="<?php esc_html_e( 'Testimonial Thumbnail', 'multipress' ); ?>">
											<?php else : ?>
												<img src="http://unsplash.it/48" alt="<?php esc_html_e( 'Testimonial Image', 'multipress' ); ?>">
											<?php endif; ?>
										</figure>
										<cite>
											<strong class="mp-testimonial__name h3"><?php the_title(); ?></strong>
											<span class="mp-testimonial__desc"><?php echo _multipress_testimonials_get_position(); ?></span>
										</cite>
									</div>
								<?php 
								$testimonials_ob[] 	= ob_get_contents();
							ob_get_clean();
						 ?>
					</div>

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
		
			</div>

		<?php if ( ! empty( $testimonials_ob ) ) : ?>
			<div class="mp-testimonial-slider-cite">
				<?php foreach ( $testimonials_ob as $testimoni ) : ?>
					<?php echo ''.$testimoni; ?>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>

		<?php else: ?>
			
			<div class="mp-testimonial-slider">

				<?php $testimonials_dummies = _multipress_testimonials_dummies(); ?>
				<?php foreach ( $testimonials_dummies as $testimony ) : ?>
					
					<div class="mp-testimonial">
						<blockquote>
							<p><?php echo ''.$testimony['content']; ?></p>
						</blockquote>
					</div>

				<?php endforeach; ?>
				
			</div>
			
			<?php if ( ! empty( $testimonials_dummies ) ) : ?>

				<div class="mp-testimonial-slider-cite">

					<?php foreach ( $testimonials_dummies as $testimony ) : ?>

						<div class="cite">
							<figure class="mp-testimonial__image">
								<img src="<?php echo esc_url( $testimony['thumbnail'] ); ?>/48/48?image=128" alt="" data-pin-nopin="true">
							</figure>
							<cite>
								<strong class="mp-testimonial__name h3"><?php echo esc_attr( $testimony['title'] ); ?></strong>
								<span class="mp-testimonial__desc"><?php echo esc_attr( $testimony['position'] ); ?></span>
							</cite>
						</div>

					<?php endforeach; ?>

				</div>
				
			<?php endif; ?>
				
		<?php endif; ?>
		
	</div>
</div>

<?php if ( isset( $_POST['updated'] ) && 0 <> $_POST['updated'] ) { ?>
	<script>
		jQuery(".multipress-testimonial--style-6 .mp-testimonial-slider").slick({
			asNavFor: ".multipress-testimonial--style-6 .mp-testimonial-slider-cite",
			slidesToShow: 1,
			prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-angle-left"></i></button>',
			nextArrow: '<button type="button" class="slick-next"><i class="fa fa-angle-right"></i></button>',
		});

		jQuery(".multipress-testimonial--style-6 .mp-testimonial-slider-cite").slick({
			asNavFor: ".multipress-testimonial--style-6 .mp-testimonial-slider",
			slidesToShow: 3,
			centerMode: true,
			centerPadding: 50,
			arrows: false,
		});
	</script>
<?php } ?>