<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
	
	$args = array(
		'post_type'				=> 'testimonials',
		'post_status'			=> 'publish',
		'ignore_sticky_posts'	=> 1,
		'posts_per_page' 		=> 3,
		'orderby' 				=> $contents['orderby'],
		'order' 				=> $contents['order'],
	);
	$testimonials = new WP_Query( $args );
 ?>

<!-- Testimonial Style 2 -->
<div id="multipress-element-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-testimonial multipress-testimonial--style-2 multipress-element-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<?php if ( ! empty( $contents['section_title'] ) || ! empty( $contents['section_subtitle'] ) ) : ?>
			<header class="section-header align-<?php echo esc_attr( $settings['section_alignment'] ); ?>">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h2 class='section-title wow fadeInUp'><?php echo esc_attr( $contents['section_title'] ); ?></h2>
				<?php endif; ?>
				<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
					<div class='section-subtitle wow fadeInUp'><?php echo ''.$contents['section_subtitle']; ?></div>
				<?php endif; ?>
			</header>
		<?php endif; ?>
		
		<div class="mp-testimonial-grid columns-3">

			<?php if ( $testimonials->have_posts() ) : ?>
				<?php $delay = 150; ?>
				<?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
					
					<div class="mp-testimonial wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
						<figure class="mp-testimonial__image">
							<?php if ( has_post_thumbnail() ) : ?>
								<img src="<?php echo _multipress_resize( _multipress_get_featured_image_url(), 100, 100 ); ?>" alt="<?php esc_html_e( 'Testimonial Image', 'multipress' ); ?>">
							<?php else : ?>
								<img src="http://unsplash.it/100" alt="<?php esc_html_e( 'Testimonial Image', 'multipress' ); ?>">
							<?php endif; ?>
						</figure>
						<cite>
							<strong class="mp-testimonial__name h3"><?php the_title(); ?></strong>
							<span class="mp-testimonial__desc"><?php echo _multipress_testimonials_get_position(); ?></span>
						</cite>
						<blockquote>
							<?php echo _multipress_testimonials_get_content(); ?>
						</blockquote>
					</div>
					<?php $delay +=175; ?>
				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>

			<?php else: ?>
				
				<?php $testimonials_dummies = _multipress_testimonials_dummies(); ?>
				<?php $counter 				= 1; ?>
				<?php $delay = 150; ?>
				<?php foreach ( $testimonials_dummies as $testimony ) : ?>
					
					<?php if ( 4 > $counter ) : ?>
						
						<div class="mp-testimonial wow fadeInUp" data-wow-delay="<?php echo ''.$delay; ?>ms">
							<figure class="mp-testimonial__image">
								<img src="<?php echo esc_url( $testimony['thumbnail'] ); ?>/100/100" alt="<?php esc_html_e( 'thumbanil', 'multipress' ); ?>">
							</figure>
							<cite>
								<strong class="mp-testimonial__name h3"><?php echo esc_attr( $testimony['title'] ); ?></strong>
								<span class="mp-testimonial__desc"><?php echo esc_attr( $testimony['position'] ); ?></span>
							</cite>
							<blockquote>
								<?php echo ''.$testimony['content']; ?>
							</blockquote>
						</div>

						<?php endif; ?>
					<?php $delay +=175; ?>
				<?php $counter++; endforeach; ?>
			<?php endif; ?>

		</div>
	</div>
</div>