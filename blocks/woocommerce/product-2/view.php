<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<!-- ECOMMERCE STYLE 2 -->
<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-ecommerce multipress-ecommerce-type-2 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>
	
	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="mul-tabs">
			<div class="wrap-tab-links wow fadeInUp">
				<?php if ( ! empty( $contents['section_title'] ) ) : ?>
					<h3><?php echo ''.$contents['section_title']; ?></h3>
				<?php endif; ?>
				<?php
					 $cat_args = array( 
					 	'taxonomy' 		=> 'product_cat', 
					 	'hide_empty' 	=> false 
				 	);
					if ( ! empty( $contents['items'] ) ) {
						$categories = array();
						foreach ( $contents['items'] as $items ) {	
							foreach ( $items as $item ) {	
								$categories[] = $item;
							}
						}
						$cat_args['include'] = implode( ",", $categories );
					 } 
					$term_query = new WP_Term_Query( $cat_args );
				 ?>
			    <ul class="tab-links wow fadeInUp">
			        <?php 
						if ( ! empty( $term_query->terms ) ) {
							$counter = 1;
							foreach ( $term_query->terms as $term ) : ?>
								
								<?php $active_class = ( 1 == $counter ) ? 'active' : ''; ?>
								<li class="<?php echo esc_attr( $active_class ); ?>"><a href="#tab-<?php echo esc_attr( $term->term_id ); ?>"><?php echo esc_attr( $term->name ); ?></a></li>
							
							<?php $counter++; endforeach;
						} 
					?>
			    </ul>
			</div>
	 
		    <div class="tab-content wow fadeInUp">
				<?php 
					if ( ! empty( $term_query->terms ) ) {
						$counter = 1;
						foreach ( $term_query->terms as $term ) : ?>
							
							<?php $active_class = ( 1 == $counter ) ? 'active' : ''; ?>
							<!-- Eco Style <?php echo esc_attr( $term->term_id ); ?> tab <?php echo esc_attr( $term->term_id ); ?> -->
							<div id="tab-<?php echo esc_attr( $term->term_id ); ?>" class="tab <?php echo esc_attr( $active_class ); ?>">
							
							<?php if ( ! empty( $contents['product_style'] ) ): ?>
								
								<?php switch ( $contents['product_style'] ) {
									case 'classic': ?>
										<?php $style_class = 'products--style-2'; ?>
										<?php break;

									case 'cover': ?>
										<?php $style_class = 'products--style-3'; ?>
										<?php break;

									default : ?>
										<?php $style_class = 'list_products hoverdir'; ?>
										<?php break;

								} ?>
								
							<?php else : ?>
								<?php $style_class = 'list_products hoverdir'; ?>
							<?php endif; ?>

								<div class="products <?php echo esc_attr( $style_class ); ?> grid-layout columns-<?php echo esc_attr( $contents['product_columns'] ); ?>">
									<?php 
										$args = array(
											'post_type'			=> 'product',
											'posts_per_page'	=> ! empty( $contents['product_per_category'] ) ? $contents['product_per_category'] : 8,
											'tax_query' 		=> array(
												array(
													'taxonomy' 	=> 'product_cat',
													'field' 	=> 'slug',
													'terms' 	=> $term->slug
												),
											),
										);
										$products = new WP_Query( $args );
									 ?>
									 <?php if ( $products->have_posts() ) : while ( $products->have_posts() ) : $products->the_post(); ?>
										
										<?php if ( ! empty( $contents['product_style'] ) ) : ?>
											
											<?php switch ( $contents['product_style'] ) {
												case 'classic':
														get_template_part( 'woocommerce/content-product-style-classic' ); 
													break;

												case 'cover': 
														get_template_part( 'woocommerce/content-product-style-cover' ); 
													break;

												default:
														get_template_part( 'woocommerce/content-product' );
													break;
											} ?>

										<?php else :
											get_template_part( 'woocommerce/content-product' );
										endif; ?>
										
									 <?php endwhile; ?>
									 <?php wp_reset_postdata(); ?>
									 <?php endif; ?>
								</div>
							</div>
						
						<?php $counter++; endforeach;
					} 
				?>
		    </div>
		</div>
		<div class="wrap-eco-banner">
			<div class="eco-banner left-banner">
				<div class="content-banner">
					<?php if ( ! empty( $contents['left_banner_title'] ) ) : ?>
						<h2 class="title-banner"><?php echo ''.$contents['left_banner_title']; ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['left_banner_subtitle'] ) ) : ?>
						<h3 class="subtitle-banner"><?php echo ''.$contents['left_banner_subtitle']; ?></h3>
					<?php endif; ?>
				</div>
				<?php if ( ! empty( $contents['left_banner_image'] ) ) : ?>
					<div class="tb-image"><img src="<?php echo esc_url( $contents['left_banner_image'] ); ?>" class="img-responsive" alt=""></div>
				<?php else : ?>
					<div class="tb-image"><img src="https://fajarekonugroho.files.wordpress.com/2017/02/	fashion_128-1000.jpg" class="img-responsive" alt=""></div>
				<?php endif; ?>
			</div>
			<div class="eco-banner">
				<div class="content-banner right-banner">
					<?php if ( ! empty( $contents['right_banner_title'] ) ) : ?>
						<h2 class="title-banner"><?php echo ''.$contents['right_banner_title']; ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['right_banner_subtitle'] ) ) : ?>
						<h3 class="subtitle-banner"><?php echo ''.$contents['right_banner_subtitle']; ?></h3>
					<?php endif; ?>
				</div>
				<?php if ( ! empty( $contents['right_banner_image'] ) ) : ?>
					<div class="tb-image"><img src="<?php echo esc_url( $contents['right_banner_image'] ); ?>" class="img-responsive" alt=""></div>
				<?php else : ?>
					<div class="tb-image"><img src="https://fajarekonugroho.files.wordpress.com/2017/02/bike_026-1000.jpg" class="img-responsive" alt=""></div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div><!-- ./multipress-ecommerce-type-2 -->