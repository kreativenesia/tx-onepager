<?php

return array(
	'slug'    	=> 'multipress-product-2',
	'name' 		=> esc_html__( 'Multipress Product 2', 'multipress' ),
	'groups'    => array( 'woocommerce' ),
	'contents' 	=> array(
		array(
			'type' 		=> 'divider',
			'value' 	=> 'Section Heading'
		),
			array(
				'name' 		=> 'section_title',
				'label' 	=> esc_html__( 'Section Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Popular Products'
			),
			
		array(
			'type' 	=> 'divider',
			'label' => 'Content Editor'
		),
			array(
				'name'		=>'items',
				'type'		=>'repeater',
				'label'		=> esc_html__( 'Product Categories', 'multipress' ),
				'fields' 	=> array(
					array(
						array(
							'name' 		=> 'product_category',
							'label' 	=> esc_html__( 'Product Category', 'multipress' ),
							'type' 		=> 'select',
							'options' 	=> _multipress_get_terms( 'product_cat', true )
						),
					),
				)
			),
			array(
				'name' 		=> 'product_per_category',
				'label' 	=> esc_html__( 'Product Per Category', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 8
			),
			array(
				'name' 		=> 'product_columns',
				'label' 	=> esc_html__( 'Product Columns', 'multipress' ),
				'type' 		=> 'select',
				'options' 	=> array(
					'2'		=> '2',
					'3'		=> '3',
					'4'		=> '4',
				),
				'value'	=> '3'
			),
			array(
				'name' 		=> 'product_style',
				'label' 	=> esc_html__( 'Product Style', 'multipress' ),
				'type' 		=> 'select',
				'options' 	=> array(
					'grid'			=> esc_html__( 'Grid - Default', 'multipress' ),
					'classic'		=> esc_html__( 'Classic', 'multipress' ),
					'cover'			=> esc_html__( 'Cover', 'multipress' ),
				),
				'value'	=> 'classic'
			),

		array(
			'type' 	=> 'divider',
			'label' => 'Left Banner Editor'
		),
			array(
				'name' 		=> 'left_banner_title',
				'label' 	=> esc_html__( 'Left Banner Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Mid Season On Sale'
			),
			array(
				'name' 		=> 'left_banner_subtitle',
				'label' 	=> esc_html__( 'Left Banner Sub Title', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'hot & trendy clothes'
			),
			array(
				'name' 		=> 'left_banner_image',
				'label' 	=> esc_html__( 'Left Banner Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'left_banner_link',
				'label' 	=> esc_html__( 'Left Banner Link', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),

		array(
			'type' 	=> 'divider',
			'label' => 'Right Banner Editor'
		),
			array(
				'name' 		=> 'right_banner_title',
				'label' 	=> esc_html__( 'Strider Sports', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'Mid Season On Sale'
			),
			array(
				'name' 		=> 'right_banner_subtitle',
				'label' 	=> esc_html__( 'The Would\'s Best Selling BlanceBike', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> 'hot & trendy clothes'
			),
			array(
				'name' 		=> 'right_banner_image',
				'label' 	=> esc_html__( 'Right Banner Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'right_banner_link',
				'label' 	=> esc_html__( 'Right Banner Link', 'multipress' ),
				'type' 		=> 'text',
				'value' 	=> '#'
			),
	),

	'settings' 	=> array(
		array(
			'name'		=> 'container_size',
			'label' 	=> esc_html__( 'Container Size', 'multipress' ),
			'type' 		=> 'select',
			'value' 	=> 'container',
			'options' 	=> array(
				'container' 		=> 'Normal',
				'container-fluid' 	=> 'Fullwidth'
			)
		),
		array(
			'name' 		=> 'parallax_background', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Enable Parallax Background', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fullscreen_height', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fullscreen Height', 'multipress' ), 
			'value' 	=> false, 
		),
		array(
			'name' 		=> 'fixed_bg', 
			'type' 		=> 'switch',
			'label' 	=> esc_html__( 'Fixed Background', 'multipress' ), 
			'value' 	=> false, 
		),
	),

	'styles' => array(
		array(
			'type' 		=> 'divider',
			'label' 	=> 'Section Styling'
		),
			array(
				'name' 		=> 'bg_color',
				'label' 	=> esc_html__( 'Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> '#ffffff'
			),
			array(
				'name' 		=> 'bg_overlay',
				'label' 	=> esc_html__( 'Background Overlay', 'multipress' ),
				'type' 		=> 'colorpicker',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'bg_image',
				'label' 	=> esc_html__( 'Background Image', 'multipress' ),
				'type' 		=> 'image',
			),
			array(
				'name' 		=> 'bg_size',
				'label' 	=> esc_html__( 'Background Size', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'initial',
				'options' 	=> array(
					'initial' 	=> 'Original Size',
					'cover' 	=> 'Cover All Section',
					'Contain' 	=> 'Fit On Section',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_position',
				'label' 	=> esc_html__( 'Background Position', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'center',
				'options' 	=> array(
					'top left' 		=> 'Top Left',
					'top center' 	=> 'Top Center',
					'top right' 	=> 'Top Right',
					'center left' 	=> 'Center Left',
					'center' 		=> 'Center',
					'center right' 	=> 'Center Right',
					'bottom left' 	=> 'Bottom Left',
					'bottom center' => 'Bottom Center',
					'bottom right' 	=> 'Bottom Right',
				)
			),
			array(
				'name' 		=> 'bg_repeat',
				'label' 	=> esc_html__( 'Background Repeat', 'multipress' ),
				'type' 		=> 'select',
				'value' 	=> 'repeat',
				'options' 	=> array(
					'repeat' 	=> 'Repeat',
					'repeat-x' 	=> 'Repeat Horizontally',
					'repeat-y' 	=> 'Repeat Vertically',
					'no-repeat' => 'No Repeat'
				)
			),
			array(
				'name' 		=> 'padding_top',
				'label' 	=> esc_html__( 'Spacing on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'padding_bottom',
				'label' 	=> esc_html__( 'Spacing on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append' 	=> 'px',
				'value' 	=> '100'
			),
			array(
				'name' 		=> 'margin_top',
				'label' 	=> esc_html__( 'Margin on Top', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'margin_bottom',
				'label' 	=> esc_html__( 'Margin on Bottom', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value' 	=> ''
			),
			array(
				'name' 		=> 'section_title_bg_color',
				'label' 	=> esc_html__( 'Title Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_border_color',
				'label' 	=> esc_html__( 'Title Border Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_color',
				'label' 	=> esc_html__( 'Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'section_title_font_size',
				'label' 	=> esc_html__( 'Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Item Styling'
		),
			array(
				'name' 		=> 'tab_active_color',
				'label' 	=> esc_html__( 'Tab Active Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'tab_item_color',
				'label' 	=> esc_html__( 'Tab Text Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'tab_item_font_size',
				'label' 	=> esc_html__( 'Tab Text Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'tab_border_color',
				'label' 	=> esc_html__( 'Tab Border Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'tab_content_background',
				'label' 	=> esc_html__( 'Tab Content Background Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),

		array(
			'type' 		=> 'divider',
			'label' 	=> 'Banner Styling'
		),
			array(
				'name' 		=> 'banner_title_color',
				'label' 	=> esc_html__( 'Banner Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'banner_title_font_size',
				'label' 	=> esc_html__( 'Banner Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			array(
				'name' 		=> 'banner_subtitle_color',
				'label' 	=> esc_html__( 'Banner Sub Title Color', 'multipress' ),
				'type' 		=> 'colorpicker',
			),
			array(
				'name' 		=> 'banner_subtitle_font_size',
				'label' 	=> esc_html__( 'Banner Sub Title Font Size', 'multipress' ),
				'type' 		=> 'text',
				'append'	=> 'px',
				'value'		=> ''
			),
			
	),

);
