<?php
	$container_size	 	= ! empty( $settings['container_size'] ) ? $settings['container_size']: 'container';
	$parallax_bg	 	= ( true == $settings['parallax_background'] ) ? 'parallax-bg': '';
	$fullscreen_height 	= ( true == $settings['fullscreen_height'] ) ? 'fullscreen-height': '';
	$fixed_bg 			= ( true == $settings['fixed_bg'] ) ? 'fixed-bg': '';
?>

<div id="multipress-block-<?php echo esc_attr( $id ); ?>" class="multipress-section multipress-subscribe multipress-subscribe--type-3 multipress-block-<?php echo esc_attr( $id ); ?> <?php echo esc_attr( $parallax_bg ); ?>  <?php echo esc_attr( $fullscreen_height ); ?> <?php echo esc_attr( $fixed_bg ); ?>">
	
	<?php if ( isset( $styles['bg_overlay'] ) && ! empty( $styles['bg_overlay'] ) ) : ?>
		<div class="background-overlay"></div>
	<?php endif; ?>

	<div class="<?php echo esc_attr( $container_size ); ?>">
		<div class="subscribe-box">
			<?php if ( ! empty( $contents['section_title'] ) ) : ?>
				<header class="section-header">
					<?php if ( ! empty( $contents['section_title'] ) ) : ?>
						<h2 class='section-title'><?php echo ''.$contents['section_title']; ?></h2>
					<?php endif; ?>
					<?php if ( ! empty( $contents['section_subtitle'] ) ) : ?>
						<p class="section-subtitle"><?php echo ''.$contents['section_subtitle']; ?></p>
					<?php endif; ?>
				</header>
			<?php endif; ?>
			
			<div class="subscribe-box__content">
				<?php if ( ! empty( $contents['form_id'] ) ) : ?>
					<?php echo do_shortcode( $contents['form_id'] ); ?>
				<?php else : ?>
					<form action="">
						<input type="text" placeholder="Your email address">
						<input type="submit" value="submit">
					</form>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>